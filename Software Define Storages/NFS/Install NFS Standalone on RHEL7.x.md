yum install nfs-utils
systemctl enable rpcbind nfs-server nfs-lock nfs-idmap --now
firewall-cmd --permanent --zone=public --add-service=nfs
firewall-cmd --permanent --zone=public --add-service=mountd
firewall-cmd --permanent --zone=public --add-service=rpc-bind
firewall-cmd --reload

mkdir /srv/nfsdata
chmod -R 755 /srv/nfsdata
chown nfsnobody:nfsnobody /srv/nfsdata/
echo "/srv/nfsdata    192.168.43.0/24(rw,sync,no_root_squash,no_all_squash)" > /etc/exports
exportfs -r
exportfs -a
systemctl restart nfs-server