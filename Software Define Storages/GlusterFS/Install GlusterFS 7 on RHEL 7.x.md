[[_TOC_]]

# Prerequisite
## Virtual Server info
| Hostname | IP | OS | Additional Disk | Purpose |
| ------ | ------ | ----- | ----- | ----- |
| gfs01 | 192.168.43.21 | RHEL 7 | sdb (20GB) | Storage for node 1 |
| gfs02 | 192.168.43.22 | RHEL 7 |sdb (20GB) | Storage for node 2 |
| client | 192.168.43.22 | RHEL 7 | N/A | GlusterFS Client |

## Set host file on each servers
```
cat >> /etc/hosts <<'EOF'
192.168.43.21   gfs01
192.168.43.22   gfs02
EOF
```
## Disk prepartion
```
pvcreate /dev/sdb
vgcreate vg_storage /dev/sdb 
lvcreate -l +100%FREE -n lv_storage vg_storage 
mkfs.xfs -i size=512 /dev/vg_storage/lv_storage
echo "$(blkid | grep lv_storage | awk '{print $2}') /datastore xfs defaults 0 0" >> /etc/fstab
mkdir /datastore
mount -a
```
# Installation
## Online packages installation
* Add GlusterFS repository
```
cat > /etc/yum.repos.d/gluster.repo <<'EOF'
[gluster-7]
name=Gluster 7
baseurl=http://mirror.centos.org/centos/$releasever/storage/$basearch/gluster-7/
gpgcheck=0
enabled=1
EOF
```
* Install GlusterFS package
```
yum install -y \
    glusterfs-server \
    userspace-rcu \
    userspace-rcu-devel
```
* Start and enable GlusterFS service
```
systemctl enable --now glusterd
systemctl status glusterd
```

## Offline packages installation
* Download glusterfs package from: ``https://buildlogs.centos.org/centos/7/storage/x86_64/``
* Upload all glusterfs and userspace-rcu packages in ``glusterfs_rhel7`` to servers
```
[root@gfs01 glusterfs_rhel7]# pwd
/root/glusterfs_rhel7
[root@gfs01 glusterfs_rhel7]# ls -ls
total 3872
 640 -rw-r-----. 1 root root  654836 May  8 09:36 glusterfs-7.5-1.el7.x86_64.rpm
 116 -rw-r-----. 1 root root  115024 May  8 09:36 glusterfs-api-7.5-1.el7.x86_64.rpm
 200 -rw-r-----. 1 root root  202056 May  8 09:36 glusterfs-cli-7.5-1.el7.x86_64.rpm
 852 -rw-r-----. 1 root root  869800 May  8 09:36 glusterfs-client-xlators-7.5-1.el7.x86_64.rpm
 156 -rw-r-----. 1 root root  159152 May  8 09:36 glusterfs-fuse-7.5-1.el7.x86_64.rpm
 428 -rw-r-----. 1 root root  434552 May  8 09:36 glusterfs-libs-7.5-1.el7.x86_64.rpm
1304 -rw-r-----. 1 root root 1334140 May  8 09:36 glusterfs-server-7.5-1.el7.x86_64.rpm
  96 -rw-r-----. 1 root root   94548 May  8 09:36 userspace-rcu-0.10.0-3.el7.x86_64.rpm
  80 -rw-r-----. 1 root root   79408 May  8 09:36 userspace-rcu-devel-0.10.0-3.el7.x86_64.rpm
```
* Install userspace-rcu packages
```
yum localinstall userspace-rcu-*.rpm
```
* Install glusterfs packages
```
yum localinstall glusterfs-*.rpm
```

## Start and enable GlusterFS service
```
systemctl enable --now glusterd
systemctl status glusterd
```
## Local firewall settings for gluserfs servers
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.21 service name=glusterfs accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.22 service name=glusterfs accept'
firewall-cmd --reload
```

# Configuration
## configure trusted pool
* From any single server. i.e: from server1 - ``gfs01``
```
gluster peer probe gfs02
```
* Check the peer status
```
gluster peer status
```

## Setup GlusterFS volume
* On all servers, create sub-directory for GlusterFS volume, i.e: ``data``
```
mkdir /datastore/data
```
* From any signel server, create GlusterFS volume and named ``data``
```
gluster volume create data replica 2 gfs01:/datastore/data gfs02:/datastore/data
gluster volume start data
```
* Confirm that the volume shows "Started"
```
gluster volume info
```
```
[root@gfs01 glusterfs_rhel7]# gluster volume info

Volume Name: datastore
Type: Replicate
Volume ID: 2b4d8bab-c1c6-47e7-8d7e-e405c527dbed
Status: Started
Snapshot Count: 0
Number of Bricks: 1 x 2 = 2
Transport-type: tcp
Bricks:
Brick1: gfs01:/datastore/data
Brick2: gfs02:/datastore/data
Options Reconfigured:
transport.address-family: inet
storage.fips-mode-rchecksum: on
nfs.disable: on
performance.client-io-threads: off
```

## Test the GlusterFS volume
* From any single GlusterFS server, test mount and create some files then verify all servers directory path ``/datastore/data/``, and it should list all created test file
```
mount -t glusterfs gfs01:/data /mnt
cd /mnt/
for i in `seq -w 1 100`; do cp -rp /var/log/messages copy-test-$i; done
ls -l
```

## Local firewall settings for GlusterFS client
* On all GlusterFS servers, allow local firewall rule for client to connect
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.23 service name=glusterfs accept'
firewall-cmd --reload
```

# Setup GlusterFS client
## Set host file or DNS name
```
cat >> /etc/hosts <<'EOF'
192.168.43.21   gfs01
192.168.43.22   gfs02
EOF
```
## Install online GlusterFS client packages
* Add GlusterFS repository
```
cat > /etc/yum.repos.d/gluster.repo <<'EOF'
[gluster-7]
name=Gluster 7
baseurl=http://mirror.centos.org/centos/$releasever/storage/$basearch/gluster-7/
gpgcheck=0
enabled=1
EOF
```
* Install GlusterFS client packages
```
yum install -y \
    glusterfs-client-xlators \
    glusterfs-libs \
    glusterfs \
    glusterfs-fuse
```

## Install offline GlusterFS client package
* Upload offline GlusterFS client packages ``glusterfs_rhel7``
```
[root@client glusterfs_rhel7]# pwd
/root/glusterfs_rhel7
[root@client glusterfs_rhel7]# ls -ls
total 3872
 640 -rw-r-----. 1 root root  654836 May  8 09:36 glusterfs-7.5-1.el7.x86_64.rpm
 116 -rw-r-----. 1 root root  115024 May  8 09:36 glusterfs-api-7.5-1.el7.x86_64.rpm
 200 -rw-r-----. 1 root root  202056 May  8 09:36 glusterfs-cli-7.5-1.el7.x86_64.rpm
 852 -rw-r-----. 1 root root  869800 May  8 09:36 glusterfs-client-xlators-7.5-1.el7.x86_64.rpm
 156 -rw-r-----. 1 root root  159152 May  8 09:36 glusterfs-fuse-7.5-1.el7.x86_64.rpm
 428 -rw-r-----. 1 root root  434552 May  8 09:36 glusterfs-libs-7.5-1.el7.x86_64.rpm
1304 -rw-r-----. 1 root root 1334140 May  8 09:36 glusterfs-server-7.5-1.el7.x86_64.rpm
  96 -rw-r-----. 1 root root   94548 May  8 09:36 userspace-rcu-0.10.0-3.el7.x86_64.rpm
  80 -rw-r-----. 1 root root   79408 May  8 09:36 userspace-rcu-devel-0.10.0-3.el7.x86_64.rpm
```

* Install GlusterFS client packages
```
cd /root/glusterfs_rhel7/
yum localinstall \
    glusterfs-client-xlators-7.5-1.el7.x86_64.rpm \
    glusterfs-libs-7.5-1.el7.x86_64.rpm \
    glusterfs-7.5-1.el7.x86_64.rpm \
    glusterfs-fuse-7.5-1.el7.x86_64.rpm
```
## Test mount from GlusterFS volume named ``data`` to mount point ``/data``
```
mkdir /data
mount -t glusterfs gfs01:/data /data
df -h
```
## Permananetly mount GlusterFS volume
```
echo "gfs01:/data /data glusterfs defaults,_netdev 0 0" >> /etc/fstab
mount -a
```
## Verify and test
```
df -h
for i in `seq -w 1 100`; do cp -rp /var/log/messages /data/copy-test-from-client-$i; done
```

# Increase disk spaces for GlusterFS volume
## Add new additional Virtual Disk on all GlusterFS servers
* All GlusterFS servers (VM) are up and running as normal
* Each servers, add new virtual disk size 10GB to increase disk space for GlusterFS volume. i.e: ``sdc``
* Scan new disk on linux without reboot servers
```
for host in /sys/class/scsi_host/host*; do echo "- - -" > $host/scan; done
lsblk
```

## Increase LVM logical volume for GlusterFS volume
* Create new physical volume for new disk ``sdc``
```
pvcreate /dev/sdc
pvs
```
* Extent existing LVM volume group of GlusterFS data ``vg_storage``
```
vgextend vg_storage /dev/sdc
vgs
```
* Extent all available space to LVM logical volume of GlusterFS data ``lv_storage``
```
lvextend -l +100%FREE /dev/vg_storage/lv_storage
lvs
```
* Expand XFS filesystem
```
xfs_growfs /datastore/
df -h
```

