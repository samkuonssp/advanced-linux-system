[[_TOC_]]

# Prerequisite
## Virtual Server info
| Hostname | IP | OS | Additional Disk | Purpose |
| ------ | ------ | ----- | ----- | ----- |
| gfs01 | 192.168.43.31 | RHEL 8 | sdb (20GB) | Storage for node 1 |
| gfs02 | 192.168.43.32 | RHEL 8 | sdb (20GB) | Storage for node 2 |
| client | 192.168.43.22 | RHEL 8 | N/A | GlusterFS Client |

## Set host file on each servers
```
cat >> /etc/hosts <<'EOF'
192.168.43.31   gfs01
192.168.43.32   gfs02
EOF
```
## Disk prepartion
```
pvcreate /dev/sda
vgcreate vg_storage /dev/sda 
lvcreate -l +100%FREE -n lv_storage vg_storage 
mkfs.xfs -i size=512 /dev/vg_storage/lv_storage
echo "$(blkid | grep lv_storage | awk '{print $2}') /gfsdata xfs defaults 0 0" >> /etc/fstab
mkdir /gfsdata
mount -a
```
# Installation
## Online packages installation
* Add GlusterFS repository
```
cat > /etc/yum.repos.d/gluster.repo <<'EOF'
[gluster-7]
name=Gluster 7
baseurl=http://mirror.centos.org/centos/$releasever/storage/$basearch/gluster-7/
gpgcheck=0
enabled=1

# Dependency
[gluster-dep]
name=Gluster Dependency
baseurl=http://mirror.centos.org/centos/$releasever/BaseOS/$basearch/os/
gpgcheck=0
enabled=1
includepkgs="userspace-rcu"

[gluster-dep-devel]
name=Gluster Dependency Devel
baseurl=http://mirror.centos.org/centos/$releasever/PowerTools/$basearch/os/
gpgcheck=0
enabled=1
includepkgs="userspace-rcu-devel python3-pyxattr"
EOF
```
* Install GlusterFS package
```
yum install -y \
    glusterfs-server \
    userspace-rcu \
    userspace-rcu-devel
```
* Start and enable GlusterFS service
```
systemctl enable --now glusterd
systemctl status glusterd
```

## Local firewall settings for gluserfs servers
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.31 service name=glusterfs accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.32 service name=glusterfs accept'
firewall-cmd --reload
```

# Configuration
## configure trusted pool
* From any single server. i.e: from server1 - ``gfs01``
```
gluster peer probe gfs02
```
* Check the peer status
```
gluster peer status
```

## Setup GlusterFS volume
* On all servers, create sub-directory for GlusterFS volume, i.e: ``brick0``
```
mkdir /gfsdata/brick0
```
* From any single server, create GlusterFS volume and named ``brick0``
```
gluster volume create brick0 replica 2 gfs01:/gfsdata/brick0 gfs02:/gfsdata/brick0
gluster volume start brick0
```
* Confirm that the volume shows "Started"
```
gluster volume info
```

## Test the GlusterFS volume
* From any single GlusterFS server, test mount and create some files then verify all servers directory path ``/gfsdata/brick0/``, and it should list all created test file
```
mount -t glusterfs gfs02:/brick0 /tmp/gfstest
cd /tmp/gfstest/
for i in `seq -w 1 100`; do cp -rp /var/log/messages copy-test-$i; done
ls -l
```

## Local firewall settings for GlusterFS client
* On all GlusterFS servers, allow local firewall rule for client to connect
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.33 service name=glusterfs accept'
firewall-cmd --reload
```

# Setup GlusterFS client
## Set host file or DNS name
```
cat >> /etc/hosts <<'EOF'
192.168.43.31   gfs01
192.168.43.32   gfs02
EOF
```

## Install online GlusterFS client packages
* Add GlusterFS repository
```
cat > /etc/yum.repos.d/gluster.repo <<'EOF'
[gluster-7]
name=Gluster 7
baseurl=http://mirror.centos.org/centos/$releasever/storage/$basearch/gluster-7/
gpgcheck=0
enabled=1
EOF
```
* Install GlusterFS client packages
```
yum install -y \
    glusterfs-client-xlators \
    glusterfs-libs \
    glusterfs \
    glusterfs-fuse
```

## Test mount from GlusterFS volume named ``brick0`` to mount point ``/data``
```
mkdir /data
mount -t glusterfs gfs01:/brick0 /data
df -h
```
## Permananetly mount GlusterFS volume
```
echo "gfs01:/brick0 /data glusterfs defaults,_netdev 0 0" >> /etc/fstab
mount -a
```
## Verify and test
```
df -h
for i in `seq -w 1 100`; do cp -rp /var/log/messages /data/copy-test-from-client-$i; done
```

# Increase disk spaces for GlusterFS volume
## Add new additional Virtual Disk on all GlusterFS servers
* All GlusterFS servers (VM) are up and running as normal
* Each servers, add new virtual disk size 10GB to increase disk space for GlusterFS volume. i.e: ``sdc``
* Scan new disk on linux without reboot servers
```
for host in /sys/class/scsi_host/host*; do echo "- - -" > $host/scan; done
lsblk
```

## Increase LVM logical volume for GlusterFS volume
* Create new physical volume for new disk ``sdc``
```
pvcreate /dev/sdc
pvs
```
* Extent existing LVM volume group of GlusterFS data ``vg_storage``
```
vgextend vg_storage /dev/sdc
vgs
```
* Extent all available space to LVM logical volume of GlusterFS data ``lv_storage``
```
lvextend -l +100%FREE /dev/vg_storage/lv_storage
lvs
```
* Expand XFS filesystem
```
xfs_growfs /datastore/
df -h
```

