gluster volume status
gluster volume stop <volume_name>
gluster volume delete <volume_name>
systemctl stop glusterd

dnf remove glusterfs*

subscription-manager attach --pool=8a1781cc7292e6630172b6cabd290151
subscription-manager repos --disable=ABA_BANK_GlusterFS_os7_gluster-7

yum install -y \
    glusterfs-server \
    userspace-rcu \
    userspace-rcu-devel

