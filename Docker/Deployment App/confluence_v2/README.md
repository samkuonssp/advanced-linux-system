# Prerequisite
* Install PostgreSQL (standalone)
* Install Docker CE
* Create a database for ``Confluence`` named ``confluencedb`` and user named ``confluenceuser``
```
sudo -u postgres psql -p $PGPORT <<'EOF'
CREATE DATABASE confluencedb WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
CREATE USER confluenceuser WITH PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE confluencedb TO confluenceuser;
EOF
```

# Create docker-compose file
[docker-compose.yml](./docker-compose.yml)