***Deploy Sonarqube with PostgreSQL with Container***

[[_TOC_]]

# Apply system configuration required by Elasticsearch
* Disable swapping
```
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a
```
* Virtual memory and open file limit
```
cat <<'EOF' | sudo tee /etc/sysctl.d/elastrics.conf
vm.max_map_count=262144
fs.file-max=65536
EOF
sysctl --system
```
* Change resource limits
```
sudo echo "elasticsearch    -   nofile  65536" > /etc/security/limits.d/elastrics.conf
```

# Create Sonarqube directory to keep docker-compose file
```
mkdir -p /srv/sonarqube
```

# Create docker-compose file for Sonarqube with PostgreSQL
```
cd /srv/sonarqube/
cat <<'EOF' | sudo tee docker-compose.yml
version: "3"

services:
  sonarqube:
    image: sonarqube:enterprise
    depends_on:
      - db
    environment:
      SONAR_JDBC_URL: jdbc:postgresql://db:5432/sonar
      SONAR_JDBC_USERNAME: sonar
      SONAR_JDBC_PASSWORD: sonar
    volumes:
      - sonarqube_data:/opt/sonarqube/data
      - sonarqube_extensions:/opt/sonarqube/extensions
      - sonarqube_logs:/opt/sonarqube/logs
      - sonarqube_temp:/opt/sonarqube/temp
    ports:
      - "9000:9000"
  db:
    image: postgres:12
    environment:
      POSTGRES_USER: sonar
      POSTGRES_PASSWORD: sonar
    volumes:
      - postgresql:/var/lib/postgresql
      - postgresql_data:/var/lib/postgresql/data

volumes:
  sonarqube_data:
  sonarqube_extensions:
  sonarqube_logs:
  sonarqube_temp:
  postgresql:
  postgresql_data:
EOF
```
* Reference: https://docs.sonarqube.org/latest/setup/install-server/

# Spin up Sonarqube with docker-compose
* Run spin up command:
```
docker-compose up -d
```
* Check logs
```
docker-compose logs -ft
```
* Finally, you should be able to access Sonarqube from ``http://<docker host/ip>:9000`` with default credential: ``admin/admin``