# Prerequisite
* Install PostgreSQL (standalone)
* Install Docker CE
* Create a database for ``Jira Software`` named ``jiradb`` and user named ``jirauser``
```
sudo -u postgres psql -p $PGPORT <<'EOF'
CREATE DATABASE jiradb WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
CREATE USER jirauser WITH PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE jiradb TO jirauser;
EOF
```

# Create docker-compose file
[docker-compose.yml](./docker-compose.yml)