[[_TOC_]]
# Overview
# System Architecture
# Server info
| Hostname | IP | OS | Purpose |
| ------ | ------ | ----- | ----- |
| labzabbixdkr01 | 192.168.43.21 | RHEL 7 | Docker Swarm Node 01 |
| labzabbixdkr02 | 192.168.43.22 | RHEL 7 | Docker Swarm Node 02 |
| labzabbixpgdb01 | 192.168.43.23 | RHEL 7 | PostgreSQL 12 DB Standalone |

# Install PostgreSQL 12 DB as standalone
* Install PostgreSQL 12 DB from script
```
yum install -y curl && bash <(curl https://gitlab.com/samkuonssp/advanced-linux-system/-/raw/master/Database%20Servers/PostgreSQL/standalone-installation/pg12_rhel7_installer.sh)
```
* Create database and user for Zabbix
```
source /etc/profile
sudo -u postgres psql -p $PGPORT <<'EOF'
create database zabbix with encoding='UTF-8';
create user zabbix with password 'noPass4you';
grant all on database zabbix to zabbix;
EOF
```
* Allow Docker Nodes in PG restriction network
```
cat >> $PGDATA/pg_hba.conf <<'EOF'
host      zabbix      zabbix      192.168.43.21/32        scram-sha-256
host      zabbix      zabbix      192.168.43.22/32        scram-sha-256
EOF
sudo -u postgres psql -p $PGPORT -c 'select pg_reload_conf();'
```
* Allow local firewall restriction
```
firewall-cmd --permanent --ipset="NET_PGUSER" --add-entry=192.168.43.21
firewall-cmd --permanent --ipset="NET_PGUSER" --add-entry=192.168.43.22
firewall-cmd --reload
```

# Install Docker CE on both Docker Nodes
* Set host file on 3 above servers
```
cat >> /etc/hosts <<'EOF'
192.168.43.21   labzabbixdkr01
192.168.43.22   labzabbixdkr02
192.168.43.23   labzabbixpgdb01
EOF
```
* Install Docker CE from below script
```
sudo yum install -y curl && bash <(curl https://gitlab.com/samkuonssp/advanced-linux-system/-/raw/master/Docker/Docker%20CE/docker_ce_standalone_rhel7.x_installer.sh)
```

# Docker Swarm
* Set local firewall settings for Docker Swarm on each nodes
```
firewall-cmd --permanent --new-service=dockerswarm
for SWARM_PORT in 22/tcp 2377/tcp 7946/tcp 7946/udp 4789/udp; do
    echo "Add port $SWARM_PORT to firewall Docker Swarm service dockerswarm"
    firewall-cmd --permanent --service=dockerswarm --add-port=$SWARM_PORT;
done
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.21 service name=dockerswarm accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.22 service name=dockerswarm accept'
firewall-cmd --reload
```

* Initialize Swarm Mode on ``labzabbixdkr01``
```
docker swarm init
```

* Join Swarm Mode as worker on ``labzabbixdkr02``
```
token=$(ssh -o StrictHostKeyChecking=no labzabbixdkr01 "docker swarm join-token -q worker") && \
docker swarm join labzabbixdkr01:2377 --token $token
```

* Create Docker Network named ``dockernetzabbix``
```
docker network create -d overlay dockernetzabbix
```

* Create Docker secret
```
printf "zabbix"
```

# Create Docker Compose file
* Create Zabbix Directory
```
mkdir -p /srv/zabbix
cd /srv/zabbix/
```

* Create env files
```
cat > .env_db_pgsql <<'EOF'
DB_SERVER_HOST=192.168.43.23
DB_SERVER_PORT=5432
POSTGRES_USER=zabbix
POSTGRES_PASSWORD=noPass4you
POSTGRES_DB=zabbix
EOF
```

```
cat > .env_srv <<'EOF'
# ZBX_LISTENIP=
# ZBX_HISTORYSTORAGEURL=http://elasticsearch:9200/ # Available since 3.4.5
# ZBX_HISTORYSTORAGETYPES=uint,dbl,str,log,text # Available since 3.4.5
# ZBX_DEBUGLEVEL=3
# ZBX_STARTPOLLERS=5
# ZBX_IPMIPOLLERS=0
# ZBX_STARTPREPROCESSORS=3 # Available since 3.4.0
# ZBX_STARTPOLLERSUNREACHABLE=1
# ZBX_STARTTRAPPERS=5
# ZBX_STARTPINGERS=1
# ZBX_STARTDISCOVERERS=1
# ZBX_STARTHTTPPOLLERS=1
# ZBX_STARTTIMERS=1
# ZBX_STARTESCALATORS=1
# ZBX_STARTALERTERS=3 # Available since 3.4.0
ZBX_JAVAGATEWAY_ENABLE=true
# ZBX_JAVAGATEWAY=zabbix-java-gateway
# ZBX_JAVAGATEWAYPORT=10052
ZBX_STARTJAVAPOLLERS=5
# ZBX_STARTVMWARECOLLECTORS=0
# ZBX_VMWAREFREQUENCY=60
# ZBX_VMWAREPERFFREQUENCY=60
# ZBX_VMWARECACHESIZE=8M
# ZBX_VMWARETIMEOUT=10
ZBX_ENABLE_SNMP_TRAPS=true
# ZBX_SOURCEIP=
# ZBX_HOUSEKEEPINGFREQUENCY=1
# ZBX_MAXHOUSEKEEPERDELETE=5000
# ZBX_SENDERFREQUENCY=30
# ZBX_CACHESIZE=8M
# ZBX_CACHEUPDATEFREQUENCY=60
# ZBX_STARTDBSYNCERS=4
# ZBX_HISTORYCACHESIZE=16M
# ZBX_HISTORYINDEXCACHESIZE=4M
# ZBX_TRENDCACHESIZE=4M
# ZBX_VALUECACHESIZE=8M
# ZBX_TIMEOUT=4
# ZBX_TRAPPERIMEOUT=300
# ZBX_UNREACHABLEPERIOD=45
# ZBX_UNAVAILABLEDELAY=60
# ZBX_UNREACHABLEDELAY=15
# ZBX_LOGSLOWQUERIES=3000
# ZBX_EXPORTFILESIZE=
# ZBX_STARTPROXYPOLLERS=1
# ZBX_PROXYCONFIGFREQUENCY=3600
# ZBX_PROXYDATAFREQUENCY=1
# ZBX_LOADMODULE="dummy1.so,dummy2.so,dummy10.so"
# ZBX_TLSCAFILE=
# ZBX_TLSCRLFILE=
# ZBX_TLSCERTFILE=
# ZBX_TLSKEYFILE=
EOF
```

```
cat > .env_web <<'EOF'
# ZBX_SERVER_HOST=zabbix-server
# ZBX_SERVER_PORT=10051
ZBX_SERVER_NAME=Zabbix Lab
# ZBX_HISTORYSTORAGEURL=http://elasticsearch:9200/ # Available since 3.4.5
# ZBX_HISTORYSTORAGETYPES=['uint', 'dbl', 'str', 'text', 'log'] # Available since 3.4.5
# ZBX_MAXEXECUTIONTIME=600
# ZBX_MEMORYLIMIT=128M
# ZBX_POSTMAXSIZE=16M
# ZBX_UPLOADMAXFILESIZE=2M
# ZBX_MAXINPUTTIME=300
# ZBX_SESSION_NAME=zbx_sessionid
# Timezone one of: http://php.net/manual/en/timezones.php
# PHP_TZ=Europe/Riga
EOF
```

```
cat > .env_agent <<'EOF'
# ZBX_SOURCEIP=
# ZBX_DEBUGLEVEL=3
# ZBX_ENABLEREMOTECOMMANDS=0
# ZBX_LOGREMOTECOMMANDS=0
# ZBX_HOSTINTERFACE= # Available since 4.4.0
# ZBX_HOSTINTERFACEITEM= # Available since 4.4.0
# ZBX_SERVER_HOST=zabbix-server
# ZBX_PASSIVE_ALLOW=true
# ZBX_PASSIVESERVERS=
# ZBX_ACTIVE_ALLOW=true
# ZBX_ACTIVESERVERS=
# ZBX_LISTENIP=
# ZBX_STARTAGENTS=3
# ZBX_HOSTNAME=
# ZBX_HOSTNAMEITEM=system.hostname
# ZBX_METADATA=
# ZBX_METADATAITEM=
# ZBX_REFRESHACTIVECHECKS=120
# ZBX_BUFFERSEND=5
# ZBX_BUFFERSIZE=100
# ZBX_MAXLINESPERSECOND=20
# ZBX_ALIAS=""
# ZBX_TIMEOUT=3
# ZBX_UNSAFEUSERPARAMETERS=0
# ZBX_LOADMODULE="dummy1.so,dummy2.so,dummy10.so"
# ZBX_TLSCONNECT=unencrypted
# ZBX_TLSACCEPT=unencrypted
# ZBX_TLSCAFILE=
# ZBX_TLSCRLFILE=
# ZBX_TLSSERVERCERTISSUER=
# ZBX_TLSSERVERCERTSUBJECT=
# ZBX_TLSCERTFILE=
# ZBX_TLSKEYFILE=
# ZBX_TLSPSKIDENTITY=
# ZBX_TLSPSKFILE=
EOF
```

```
cat > .env_java <<'EOF'
# ZBX_START_POLLERS=5
# ZBX_TIMEOUT=3
# Possible values: trace, debug, info, want, error, all, off
# ZBX_DEBUGLEVEL=info
EOF
```

* Create zabbix docker compose file
```
cat > docker-compose.yml <<'EOF'
version: '3.5'
services:
 zabbix-server:
  image: zabbix/zabbix-server-pgsql:latest
  ports:
   - "10051:10051"
  volumes:
   - /etc/localtime:/etc/localtime:ro
   - /etc/timezone:/etc/timezone:ro 
   - ./zbx_env/usr/lib/zabbix/alertscripts:/usr/lib/zabbix/alertscripts:ro
   - ./zbx_env/usr/lib/zabbix/externalscripts:/usr/lib/zabbix/externalscripts:ro
   - ./zbx_env/var/lib/zabbix/export:/var/lib/zabbix/export:rw
   - ./zbx_env/var/lib/zabbix/modules:/var/lib/zabbix/modules:ro
   - ./zbx_env/var/lib/zabbix/enc:/var/lib/zabbix/enc:ro
   - ./zbx_env/var/lib/zabbix/ssh_keys:/var/lib/zabbix/ssh_keys:ro
   - ./zbx_env/var/lib/zabbix/mibs:/var/lib/zabbix/mibs:ro
   - ./zbx_env/var/lib/zabbix/snmptraps:/var/lib/zabbix/snmptraps:ro
  ulimits:
   nproc: 65535
   nofile:
    soft: 20000
    hard: 40000
  deploy:
   mode: replicated
   replicas: 2
   resources:
    limits:
      cpus: '0.70'
      memory: 1G
    reservations:
      cpus: '0.5'
      memory: 512M
  env_file:
   - .env_db_pgsql
   - .env_srv
  depends_on:
   - zabbix-java-gateway
   - zabbix-snmptraps
  networks:
   zbx_net_backend:
     aliases:
      - zabbix-server
      - zabbix-server-pgsql
      - zabbix-server-alpine-pgsql
      - zabbix-server-pgsql-alpine
   zbx_net_frontend:
  stop_grace_period: 30s
  sysctls:
   - net.ipv4.ip_local_port_range=1024 65000
   - net.ipv4.conf.all.accept_redirects=0
   - net.ipv4.conf.all.secure_redirects=0
   - net.ipv4.conf.all.send_redirects=0
  labels:
   com.zabbix.description: "Zabbix server with PostgreSQL database support"
   com.zabbix.company: "LAB"
   com.zabbix.component: "zabbix-server"
   com.zabbix.dbtype: "pgsql"
   com.zabbix.os: "alpine"

 zabbix-web-nginx-pgsql:
  image: zabbix/zabbix-web-nginx-pgsql:latest
  ports:
   - "80:8080"
   - "443:8443"
  links:
   - zabbix-server:zabbix-server
  volumes:
   - /etc/localtime:/etc/localtime:ro
   - /etc/timezone:/etc/timezone:ro
   - ./zbx_env/etc/ssl/nginx:/etc/ssl/nginx:ro
  deploy:
   mode: replicated
   replicas: 2
   resources:
    limits:
      cpus: '0.70'
      memory: 512M
    reservations:
      cpus: '0.5'
      memory: 256M
  env_file:
   - .env_db_pgsql
   - .env_web
  depends_on:
   - zabbix-server
  healthcheck:
   test: ["CMD", "curl", "-f", "http://localhost:8080/"]
   interval: 10s
   timeout: 5s
   retries: 3
   start_period: 30s
  networks:
   zbx_net_backend:
    aliases:
     - zabbix-web-nginx-pgsql
     - zabbix-web-nginx-alpine-pgsql
     - zabbix-web-nginx-pgsql-alpine
   zbx_net_frontend:
  stop_grace_period: 10s
  sysctls:
   - net.core.somaxconn=65535
  labels:
   com.zabbix.description: "Zabbix frontend on Nginx web-server with PostgreSQL database support"
   com.zabbix.company: "LAB"
   com.zabbix.component: "zabbix-frontend"
   com.zabbix.webserver: "nginx"
   com.zabbix.dbtype: "pgsql"
   com.zabbix.os: "alpine"

 zabbix-agent:
  image: zabbix/zabbix-agent:latest
  ports:
   - "10050:10050"
  volumes:
   - /etc/localtime:/etc/localtime:ro
   - /etc/timezone:/etc/timezone:ro
   - ./zbx_env/etc/zabbix/zabbix_agentd.d:/etc/zabbix/zabbix_agentd.d:ro
   - ./zbx_env/var/lib/zabbix/modules:/var/lib/zabbix/modules:ro
   - ./zbx_env/var/lib/zabbix/enc:/var/lib/zabbix/enc:ro
   - ./zbx_env/var/lib/zabbix/ssh_keys:/var/lib/zabbix/ssh_keys:ro
  links:
   - zabbix-server:zabbix-server
  deploy:
   resources:
    limits:
      cpus: '0.2'
      memory: 128M
    reservations:
      cpus: '0.1'
      memory: 64M
   mode: global
  env_file:
   - .env_agent
  privileged: true
  pid: "host"
  networks:
   zbx_net_backend:
    aliases:
     - zabbix-agent
     - zabbix-agent-passive
     - zabbix-agent-alpine
  stop_grace_period: 5s
  labels:
   com.zabbix.description: "Zabbix agent"
   com.zabbix.company: "LAB"
   com.zabbix.component: "zabbix-agentd"
   com.zabbix.os: "alpine"

 zabbix-java-gateway:
  image: zabbix/zabbix-java-gateway:latest
  ports:
   - "10052:10052"
  deploy:
   resources:
    limits:
      cpus: '0.5'
      memory: 512M
    reservations:
      cpus: '0.25'
      memory: 256M
  env_file:
   - .env_java
  networks:
   zbx_net_backend:
    aliases:
     - zabbix-java-gateway
     - zabbix-java-gateway-alpine
  stop_grace_period: 5s
  labels:
   com.zabbix.description: "Zabbix Java Gateway"
   com.zabbix.company: "Zabbix LLC"
   com.zabbix.component: "java-gateway"
   com.zabbix.os: "alpine"

 zabbix-snmptraps:
  image: zabbix/zabbix-snmptraps:latest
  ports:
   - "162:1162/udp"
  volumes:
   - ./zbx_env/var/lib/zabbix/snmptraps:/var/lib/zabbix/snmptraps:rw
  deploy:
   resources:
    limits:
      cpus: '0.5'
      memory: 256M
    reservations:
      cpus: '0.25'
      memory: 128M
  networks:
   zbx_net_frontend:
    aliases:
     - zabbix-snmptraps
   zbx_net_backend:
  stop_grace_period: 5s
  labels:
   com.zabbix.description: "Zabbix snmptraps"
   com.zabbix.company: "Zabbix LLC"
   com.zabbix.component: "snmptraps"
   com.zabbix.os: "ubuntu"

# db_data_pgsql:
#  image: busybox
#  volumes:
#   - ./zbx_env/var/lib/postgresql/data:/var/lib/postgresql/data:rw

networks:
  zbx_net_frontend:
    driver: overlay
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    ipam:
      driver: default
      config:
      - subnet: 172.16.238.0/24
  zbx_net_backend:
    driver: overlay
    driver_opts:
      com.docker.network.enable_ipv6: "false"
    internal: true
    ipam:
      driver: default
      config:
      - subnet: 172.16.239.0/24
EOF
```

* Deploy Zabbix from Docker compose file
```
docker stack deploy -c docker-compose.yml zabbix
```
