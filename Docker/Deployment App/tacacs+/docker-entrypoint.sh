#!/bin/sh

TAC_PLUS_BIN=/tac_plus/sbin/tac_plus
TAC_PLUS_CONF=/tac_plus/etc/tac_plus.cfg

# Check configuration file exists
if [ ! -f ${TAC_PLUS_CONF} ]; then
    echo "No configuration file at ${TAC_PLUS_CONF}"
    exit 1
fi

# Check configuration file for syntax errors
${TAC_PLUS_BIN} -P ${TAC_PLUS_CONF}
if [ $? -ne 0 ]; then
    echo "Invalid configuration file"
    exit 1
fi

# Make the log directories
mkdir -m 760 -p /var/log/tac_plus

echo "Starting server..."

# Start the server
exec ${TAC_PLUS_BIN} -f ${TAC_PLUS_CONF}