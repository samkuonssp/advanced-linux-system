[[_TOC_]]

# Demonstration Info
* All services running with containers
* Database: ``postgresql server latest version``
* Zabbix Server: ``Zabbix server on alpine latest version``
* Zabbix Web Interface: ``Zabbix Web Interface with nginx``
* Zabbix Agent
* Reference: https://github.com/zabbix/zabbix-docker