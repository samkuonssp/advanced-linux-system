# How to deploy phpIPAM with Docker Swarm
* This case I will use MariaDB as external database
* Create env_file as below ``.env``
```
cat > .env <<'EOF'
TZ=Asia/Phnom_Penh
IPAM_DATABASE_HOST=192.168.43.40
IPAM_DATABASE_USER=root
IPAM_DATABASE_PASS=password#1
IPAM_DATABASE_NAME=db_phpipam
SCAN_INTERVAL=1h
EOF
```

* Deploy with compose file
```
docker stack deploy --compose-file docker-compose.yml phpipm
```

* Detials of running containers
```
docker stack ps phpipam
```

* Access to ``http://<docker ip or hostname>`` to complete installation