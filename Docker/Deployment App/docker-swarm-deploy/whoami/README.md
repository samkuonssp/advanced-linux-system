* Create ``docker-compose.yml`` file to deploy ``whoami`` as app1
```
mkdir /srv/app1
cd /srv/app1/

cat <<'EOF' | sudo tee docker-compose.yml
version: '3.7'

networks:
  haproxy-net:
    external: true

services:
  app:
    image: containous/whoami:latest
    networks:
      - haproxy-net
    deploy:
      mode: replicated
      replicas: 4
      endpoint_mode: dnsrr
      placement:
        constraints:
          - node.role != manager
          - node.role == worker
EOF
```
* Start deploy ``whoami`` with stack named ``stack_app1``
```
cd /srv/app1/
docker stack deploy -c docker-compose.yml stack_app1
```