**HAProxy Load Balancer for Docker Swarm**
* Add docker node label for those nodes using as HAProxy/Ingress
```
docker node ls
docker node update --label-add haproxy-node=true <node_id>
```

* Add/Allow firewall ports below on all haproxy-node==true. You can add allow more different ports if your HAProxy bind different ports
```
ufw allow proto tcp from any to any port 80
ufw allow proto tcp from any to any port 443
```

* Create new directory named ``haproxy`` and move into it
```
mkdir /srv/haproxy
cd /srv/haproxy
```

* Create self-signed private certificates
```
mkdir -p /srv/haproxy/certs
cd /srv/haproxy/certs/

openssl dhparam -out dhparam.pem 2048
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout haproxy-private.key -out haproxy-certificate.pem
cat haproxy-private.key >> haproxy-certificate.pem

chmod 400 *.{key,pem}
chmod 600 ../certs
```

* Create docker secret for those self-signed certificate files
```
docker secret create dhparam.pem /srv/haproxy/certs/dhparam.pem
docker secret create haproxy-certificate.pem /srv/haproxy/certs/haproxy-certificate.pem
```

* Prepare/create example ``haproxy.cfg``
```
cat <<'EOF' | sudo tee haproxy.cfg
global
    log         	fd@2 local2
    maxconn     	40000
    master-worker
    nbproc       	1
    nbthread     	4
    cpu-map      	auto:1/1-4 0-3

    ssl-default-bind-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
    ssl-default-bind-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
    ssl-default-bind-options prefer-client-ciphers no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets

    ssl-default-server-ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
    ssl-default-server-ciphersuites TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256
    ssl-default-server-options no-sslv3 no-tlsv10 no-tlsv11 no-tls-tickets

    ssl-dh-param-file /usr/local/etc/haproxy/dhparam.pem

resolvers docker
    nameserver dns1 127.0.0.11:53
    resolve_retries 3
    timeout resolve 1s
    timeout retry   1s
    hold other      10s
    hold refused    10s
    hold nx         10s
    hold timeout    10s
    hold valid      10s
    hold obsolete   10s

defaults
    timeout connect 10s
    log global
    mode http
    option httplog
    option http-server-close
    option forwardfor
    option dontlognull
    option redispatch
    option contstats

frontend default
    mode http
    bind :80
    redirect scheme https code 301 if !{ ssl_fc }

    bind :443 ssl crt /usr/local/etc/haproxy/cgcc.com.kh.pem alpn h2,http/1.1
    http-response set-header Strict-Transport-Security max-age=63072000

    acl url_is_stats req.hdr(Host) -i stats.cgcc.com.kh
    #acl url_is_swpit req.hdr(Host) -i swpit.cgcc.com.kh

    use_backend haproxy if url_is_stats
    #use_backend swarmpit if url_is_swpit

backend haproxy
    stats enable
    stats uri /
    stats refresh 15s
    stats show-legends
    stats show-node
    stats auth sysadm:changeme
    stats admin if TRUE

#backend swarmpit
#    balance roundrobin
#    server-template swpit_app- 1 swarmpit_app:8080 check resolvers docker init-addr libc,none
EOF
```
* Create docker configs to store haproxy configuration file. We are better to have configs version, it will be easy to update configs next time.
```
docker config create haproxy-v1.0.0.cfg haproxy.cfg
docker config ls
```

* Prepare/create ``docker-compose.yml`` file to deploy HAProxy
```
cat <<'EOF' | sudo tee docker-compose-haproxy.yml
version: '3.7'

networks:
  haproxy-net:
    external: true

configs:
  haproxy-v1.0.0.cfg:
    external: true

secrets:
  dhparam.pem:
    external: true
  cgcc.com.kh.pem:
    external: true

services:
  proxy:
    image: haproxy:alpine
    networks:
      - haproxy-net
    ports:
      - target: 80
        published: 80
        protocol: tcp
        mode: host
    ports:
      - target: 443
        published: 443
        protocol: tcp
        mode: host
    configs:
      - source: haproxy-v1.0.0.cfg
        target: /usr/local/etc/haproxy/haproxy.cfg
        mode: 0444
    secrets:
      - source: dhparam.pem
        target: /usr/local/etc/haproxy/dhparam.pem
        mode: 0444
      - source: cgcc.com.kh.pem
        target: /usr/local/etc/haproxy/cgcc.com.kh.pem
        mode: 0444
    dns:
      - 127.0.0.11
    deploy:
      mode: replicated
      replicas: 3
      placement:
        constraints:
          - node.labels.haproxy-node == true
EOF
```
* Create overlay network for HAproxy - named ``haproxy-net``
```
docker network create --attachable --driver overlay haproxy-net
```

* Start deploy HAProxy and set stack name ``haproxy``
```
cd /srv/haproxy/
docker stack deploy -c docker-compose-haproxy.yml haproxy
```
* To verify the deploy status
```
docker stack ls
docker service ls
docker service logs -ft haproxy_haproxy
```
* To add backend App to HAProxy, we need to add ``endpoint_mode: dnsrr`` in deploy section of App compose file.
* After that update ``haproxy.cfg`` as the following:
```
--> Add new ACL, USE_BACKEND and BACKEND name
frontend default
-----
   acl url_is_<app_name> req.hdr(Host) -i <App URL>
   use_backend <app_name> if url_is_<app_name>
-----

backend <app_name>
   balance roundrobin
   server-template <app_name>- <replicas number> <service_name>:<service_port> check resolvers docker init-addr libc,none
```

* Example
```
vim haproxy.cfg
-----
frontend default
  acl url_is_swpit req.hdr(Host) -i swpit.lab.local
  use_backend swarmpit if url_is_swpit

-----
backend swarmpit
    balance roundrobin
    server-template swpit_app- 1 swarmpit_app:8080 check resolvers docker init-addr libc,none
```

* Create a new haproxy configuration file
```
docker config create haproxy-v1.0.1.cfg haproxy.cfg
```

* Change haproxy config version in ``docker-compose-haproxy.yml`` file
```
-----
    configs:
      - source: haproxy-v1.0.1.cfg
-----
```

* Finally re-deploy again to get the update
```
docker stack deploy -c docker-compose-haproxy.yml haproxy
```