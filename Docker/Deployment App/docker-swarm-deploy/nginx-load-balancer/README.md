* Create new directory named ``nginx-ingress`` and move into it
```
mkdir /srv/nginx-ingress
cd /srv/nginx-ingress
```

* Create self-signed private certificates
```
mkdir -p /srv/nginx-ingress/certs
cd /srv/nginx-ingress/certs/

openssl dhparam -out dhparam.pem 2048
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-private.key -out nginx-certificate.pem

chmod 400 *.{key,pem}
chmod 600 ../certs
```

* Create docker secret for those self-signed certificate files
```
docker secret create dhparam-pem /srv/nginx-ingress/certs/dhparam.pem
docker secret create nginx-private-key /srv/nginx-ingress/certs/nginx-private.key
docker secret create nginx-certificate-pem /srv/nginx-ingress/certs/nginx-certificate.pem
```

* Create docker overlay network named ``backend-net``
```
docker netowrk create --attachable --driver overlay backend-net
```


