* Make ``openssl`` installed or if not yet, run command:
```
sudo apt install openssl
```

* Create ``Self-Signed SSL Certificate`` with command:
```
openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 3560 \
            -nodes \
            -out server.crt \
            -keyout server.key
```
* ``-newkey rsa:4096``: Creates a new certificate request and 4096 bit RSA key. The default one is 2048 bits.
* ``-x509``: Creates a X.509 Certificate.
* ``-sha256``: Use 265-bit SHA (Secure Hash Algorithm).
* ``-days 3650``: The number of days to certify the certificate for 3650 is ten years. you can set days as your prefer.
* ``-nodes``: Create a key without a passphrase.
* ``-out server.crt``: Specific the filename to write the newly create certificate.
* ``-keyout server.key``: Specific the filename to write the newly created private key.

* Set permission if needed
```
chmod 400 *.{key,crt}
chmod 600 ../certs
```
