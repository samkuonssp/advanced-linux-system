**Reference** technicial document follow https://dockerswarm.rocks/
# Traefik Proxy with HTTPs
## Preparation
* Connect via SSH to manager node
* Create docker overlay network for ``traefik``
```
docker network create --attachable --driver overlay traefik-net
```
* Generate hash-password for traefik dashboard basic-auth
* if there is no htpasswd command, plz run (ubuntu): sudo apt-get install apache2-utils
```
echo $(htpasswd -nb <user> <secure_password>) | sed -e s/\\$/\\$\\$/g
echo $(htpasswd -nb tfadmin password#1) | sed -e s/\\$/\\$\\$/g
--> Result: tfadmin:$$apr1$$ukcwT7NA$$AuGx7aWU0DUnTmBs9ZsA50`
```

* Deploy traefik load balancer stack:
```
docker stack deploy -c docker-compose-traefik_v2.yml traefik
```

* To access Traefik Dashboard: ``https://tfdash.lab.local``
* For testing, please deply an application named "whoami"
```
docker stack deploy -c docker-compose-whoami.yml
```

* To access Whoami App: ``http://whoami.lab.local``

* Additional
```
docker secret create server-key ./server.key
docker secret create server-crt ./server.crt
```
