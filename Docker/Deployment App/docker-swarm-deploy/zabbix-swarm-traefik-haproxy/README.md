mkdir -p /docker-data/zabbix/zabbix-server/{externalscripts,alertscripts}
cd /docker-data/zabbix/

docker network create --attachable --driver overlay monitoring-network

docker stack deploy -c docker-compose.yml zabbix