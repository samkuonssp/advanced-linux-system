# Prerequisite
* Create nginx configuration directories
  - ``/srv/nginx``: Nginx directory to store docker-compose and main nginx configuration
  - ``/srv/nginx/certs``: Certificate directory for Nginx
  - ``/srv/nginx/conf.d/``: Nginx directory to store all proxy sites
  - ``/srv/nginx/conf.d/global.d``: Nginx directory to store all secure settings configuration files
```
mkdir -p /srv/nginx/{certs,conf.d/global.d}
```

* In case, we use self-signed certificate and private key and follow:
  -  Create certificate
```
cd /srv/nginx/certs/
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-private.key -out nginx-certificate.pem
```

  - Generate strong DHE (Ephemeral Diffie-Hellman) parameters
```
openssl dhparam -out dhparam.pem 2048
```

  - Restrict SSL directory, private key and selfsigned certificate
```
chmod 400 /srv/nginx/certs/*.{key,pem}
chmod 600 /srv/nginx/certs
```
* Create redirection [``conf.d/redirect.conf``](./conf.d/redirect.conf)
* Secure nginx reverse proxy
  - Create [``global.d/common.conf``](./conf.d/global.d/common.conf)
  - Create [``global.d/common_location.conf``](./conf.d/global.d/common_location.conf)
  - Create [``global.d/ssl.conf``](./conf.d/global.d/ssl.conf)

* Example site
```
cat > /srv/nginx/conf.d/jira.lab.local.conf <<'EOF'
upstream jira {
  server        192.168.43.31:8080;
}

server {
  listen        443 ssl;
  server_name   jira.lab.local;

  merge_slashes on;
  msie_padding on;

  include       /etc/nginx/conf.d/global.conf/common.conf;
  include       /etc/nginx/conf.d/global.conf/ssl.conf;

  location / {
    proxy_pass  http://jira;
    include     /etc/nginx/conf.d/global.conf/common_location.conf;
  }
}
EOF
```

# Create docker-compose file
[docker-compose.yml](./docker-compose.yml)

# References:
* [How to set up an easy and secure reverse proxy with Docker, Nginx & Letsencrypt](https://www.freecodecamp.org/news/docker-nginx-letsencrypt-easy-secure-reverse-proxy-40165ba3aee2/#:~:text=In%20order%20to%20get%20the,that%20the%20syntax%20is%20ok.)