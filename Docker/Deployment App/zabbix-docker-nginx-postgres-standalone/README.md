# How to Deploy Zabbix Standalone with Docker
## Features:
* Zabbix Server with alpine base image and running on Docker
* Zabbix Webserver with Nginx
* Zabbix Database with PostgreSQL (timescaldb)
* Grafana for dashboard

## How to deploy:
* Create a directory or folder and let named it ``zabbix-standalone``
```
mkdir /srv/zabbix-standalone
cd /srv/zabbix-standalone/
```

* Clone the source code from gitlab
```
SRC_URL=https://gitlab.com/samkuonssp/advanced-linux-system/-/raw/master/Docker/Deployment%20App/zabbix-docker-nginx-postgres-standalone

curl -Lk ${SRC_URL}/docker-compose.yml -o docker-compose.yml
curl -Lk ${SRC_URL}/zabbix-pgsql.env -o zabbix-pgsql.env
curl -Lk ${SRC_URL}/zabbix-server.env -o zabbix-server.env
curl -Lk ${SRC_URL}/zabbix-web.env -o zabbix-web.env
```

* Spin up ``Zabbix Standalone`` by the following commands:
```
docker-compose -p zabbix up -d
```

* To access Zabbix with default user ``Admin`` and password ``zabbix``: ``http://<docker_ip/hostname/address>``
* To access Grafana with default user ``admin`` and password ``admin``: ``http://<docker_ip/hostname/address>:3000``