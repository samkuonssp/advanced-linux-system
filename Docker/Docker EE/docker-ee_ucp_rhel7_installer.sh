#!/bin/bash
firewall-cmd --permanent --add-service=SVR_UCP_MGR
for m in 179/tcp 443/tcp 2376/tcp 2377/tcp 4789/udp 6443/tcp 6444/tcp 7946/tcp 7946/udp 9099/tcp 10250/tcp 12376/tcp 12378/tcp 12379/tcp ; do
	firewall-cmd --permanent --service=SVR_UCP_MGR --add-port=$m;
done

