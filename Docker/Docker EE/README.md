# Docker EE Implementation
## Scope of Installation


## Docker Enterprise Engine Installation
### Prerequisites
* Use RHEL 64-bit 7.4 and higher on x86_64.
* Use storage driver overlay2 or devicemapper (direct-lvm mode in production). To use direct-lvm, we will requires one or more dedicated block devices, i.e: /dev/sdb.
* Find the URL for your Docker Engine - Enterprise repo at Docker Hub.
* Uninstall old versions of Docker.
* Remove old Docker repos from /etc/yum.repos.d/

### Find your Docker Engine - Enterprise repo URL
To install Docker Enterprise, you will need the URL of the Docker Enterprise repository associated with your trial or subscription:

- Go to https://hub.docker.com/my-content. All of your subscriptions and trials are listed.
- Click the Setup button for Docker Enterprise Edition for Red Hat Enterprise Linux.
- Copy the URL from Copy and paste this URL to download your Edition and save it for later use.
You will use this URL in a later step to create a variable called, DOCKERURL.

### Set your environments setup here:
```
DOCKER_EE_URL=https://storebits.docker.com/ee/rhel/sub-9491de77-73fd-49b7-bfb2-aa47ebb9a3f8
```

### Uninstall old Docker versions
```
sudo yum remove -y docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-selinux \
        docker-engine-selinux \
        docker-engine
```

### Setup Docker EE repository
* Remove existing Docker repositories from ``/etc/yum.repos.d/``:
```
sudo rm /etc/yum.repos.d/docker*.repo
```

* Temporarily store the URL (that you copied above) in an environment variable. Replace ``DOCKER_EE_URL`` with your URL in the following command. This variable assignment does not persist when the session ends:
```
export DOCKERURL="$DOCKER_EE_URL"
```

* Store the value of the variable, ``DOCKERURL`` (from the previous step), in a yum variable in ``/etc/yum/vars/``: Also, store your OS version string in ``/etc/yum/vars/dockerosversion``.
```
sudo -E sh -c 'echo "$DOCKERURL/rhel" > /etc/yum/vars/dockerurl'
sudo sh -c 'echo "7" > /etc/yum/vars/dockerosversion'
```

* Install required packages: ``yum-utils`` provides the yum-config-manager utility, and ``device-mapper-persistent-data`` and ``lvm2`` are required by the devicemapper storage driver:
```
sudo yum install -y yum-utils \
        device-mapper-persistent-data \
        lvm2
```

* Enable the extras RHEL repository. This ensures access to the container-selinux package required by docker-ee. But case, we do not have subscription, so I will install container-selinux from CentOS mirror instead.
```
sudo yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```

* Add the Docker Engine - Enterprise stable repository:
```
sudo yum-config-manager -v \
        --add-repo \
        "$DOCKERURL/rhel/docker-ee.repo"
```

### Install Docker EE Engine pacakges
* Install latest stable ``Docker EE`` packages
```
sudo yum install -y docker-ee \
        docker-ee-cli \
        containerd.io
```

* Configure ``direct-lvm`` mode for production (Recommend)
```
sudo pvcreate /dev/sdb /dev/sdc
sudo vgcreate docker /dev/sdb /dev/sdc
sudo lvcreate --wipesignatures y -n thinpool docker -l 95%VG
sudo lvcreate --wipesignatures y -n thinpoolmeta docker -l 1%VG
sudo lvconvert -y \
    --zero n \
    -c 512K \
    --thinpool docker/thinpool \
    --poolmetadata docker/thinpoolmeta
```
```
cat > /etc/lvm/profile/docker-thinpool.profile <<'EOF'
activation {
    thin_pool_autoextend_threshold=80
    thin_pool_autoextend_percent=20
}
EOF
```
```
sudo lvchange --metadataprofile docker-thinpool docker/thinpool
sudo lvs -o+seg_monitor
sudo lvchange --monitor y docker/thinpool
mkdir -p /etc/docker
cat > /etc/docker/daemon.json <<'EOF'
{
    "storage-driver": "devicemapper",
    "storage-opts": [
    "dm.thinpooldev=/dev/mapper/docker-thinpool",
    "dm.use_deferred_removal=true",
    "dm.use_deferred_deletion=true"
    ]
}
EOF
```
* Enable and start ``Docker Engine`` service
```
systemctl enable docker --now
```

## Universal Control Plane Installation
### UCP Managers
#### FirewallD Settings
* Create FirewallD service for UCP Manager ``SVR_UCP_MGR``
```
firewall-cmd --permanent --new-service=SVR_UCP_MGR
for m in 179/tcp 443/tcp 2376-2377/tcp 4789/udp 6443-6444/tcp 7946/tcp 7946/udp 9099/tcp 10250/tcp 12376/tcp 12378-12386/tcp 12388/tcp; do
    echo "Add UCP manager port $m to SVR_UCP_MGR"
    firewall-cmd --permanent --service=SVR_UCP_MGR --add-port=$m;
done
```

* Create FirewallD source network to access UCP Manager service.
 - Node's IP
 - Docker0's Network 
```
firewall-cmd --permanent --new-ipset=NET_UCP_CON --type=hash:net
for o in 192.168.43.31 172.17.0.0/16; do
    echo "Add requirement network $o to ipset NET_UCP_CON"
    firewall-cmd --permanent --ipset=NET_UCP_CON --add-entry=$o;
done
```

* Apply FirewallD rich rule for UCP Manager service
```
firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source ipset="NET_UCP_CON" service name="SVR_UCP_MGR" accept'
firewall-cmd --reload
```
#### Install UCP (Online - Node required internet access)
* Pull the latest version of UCP
```
docker image pull docker/ucp:3.2.4
```

* Start installing UCP as manager node
```
docker container run --rm -it --name ucp \
  -v /var/run/docker.sock:/var/run/docker.sock \
  docker/ucp:3.2.4 install \
  --interactive \
  --force-minimums \
  --host-address <node-ip-address>
  --pod-cidr <pod-network, i.e: 192.168.44.0/24>
```

#### Install UCP (Offline)
* Download the latest UCP offline image here https://docs.docker.com/ee/ucp/admin/install/install-offline/
* Copy offline image file (*.tar.gz) nodes
* Load the Docker offline image file
```
docker load -i ucp_image_*.tar.gz
```

### UCP Workers
#### FirewallD Settings
* Create FirewallD service for UCP Worker ``SVR_UCP_WKR``
```
firewall-cmd --permanent --new-service=SVR_UCP_WKR
for n in 179/tcp 4789/udp 6444/tcp 7946/tcp 7946/udp 9099/tcp 10250/tcp 12376/tcp 12378/tcp; do
    echo "Add UCP worker port $n to SVR_UCP_WKR"
    firewall-cmd --permanent --service=SVR_UCP_WKR --add-port=$n;
done
```

* Create FirewallD source network to access UCP Worker service.
    * ``Node's IP``
    * ``Docker0's Network``
```
firewall-cmd --permanent --new-ipset=NET_UCP_CON --type=hash:net
for o in 192.168.43.33 172.17.0.0/16; do
    echo "Add requirement network $o to ipset NET_UCP_CON"
    firewall-cmd --permanent --ipset=NET_UCP_CON --add-entry=$o;
done
```

* Apply FirewallD rich rule for UCP Worker service
```
firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source ipset="NET_UCP_CON" service name="SVR_UCP_WKR" accept'
firewall-cmd --reload
```

### Troubleshooting
* Remove node from cluster
```
docker node ls
docker swarm leave --force
docker node rm <node's hostname>
```
* Uninstall UCP automatically
```
docker container run --rm -it \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --name ucp \
    docker/ucp:3.2.4 uninstall-ucp --interactive
```

* Manually uninstall UCP if above command not working
```
#Run the following command on one manager node to remove remaining UCP services
docker service rm $(docker service ls -f name=ucp- -q)

#Run the following command on each manager node to remove remaining UCP containers
docker container rm -f $(docker container ps -a -f name=ucp- -f name=k8s_ -q)

#Run the following command on each manager node to remove remaining UCP volumes
docker volume rm $(docker volume ls -f name=ucp -q)
```


## Docker Trusted Registry Installation
