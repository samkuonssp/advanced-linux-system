# Docker EE Implementation on RHEL 7.x
## Prerequisites
* Use RHEL 64-bit 7.4 and higher on x86_64.
* Use storage driver overlay2 or devicemapper (direct-lvm mode in production). To use direct-lvm, we will requires one or more dedicated block devices, i.e: /dev/sdb.
* Find the URL for your Docker Engine - Enterprise repo at Docker Hub.
* Uninstall old versions of Docker.
* Remove old Docker repos from /etc/yum.repos.d/

## Find your Docker Engine - Enterprise repo URL
To install Docker Enterprise, you will need the URL of the Docker Enterprise repository associated with your trial or subscription:

* Go to https://hub.docker.com/my-content. All of your subscriptions and trials are listed.
* Click the Setup button for Docker Enterprise Edition for Red Hat Enterprise Linux.
* Copy the URL from Copy and paste this URL to download your Edition and save it for later use.
You will use this URL in a later step to create a variable called, DOCKERURL.

## Setting up servers environment

## Installing Docker Engine Enterprise (All nodes)

### Define your Docker EE URL (License or trial)
```
DOCKER_EE_URL=https://storebits.docker.com/ee/rhel/sub-9491de77-73fd-49b7-bfb2-aa47ebb9a3f8
```

### Uninstall old Docker versions
```
sudo yum remove -y docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-selinux \
        docker-engine-selinux \
        docker-engine
```

### Setting up Docker EE repository
* Remove existing Docker repositories from ``/etc/yum.repos.d/``:
```
sudo rm /etc/yum.repos.d/docker*.repo
```

* Temporarily store the URL (that you copied above) in an environment variable. Replace ``DOCKER_EE_URL`` with your URL in the following command. This variable assignment does not persist when the session ends:
```
export DOCKERURL="$DOCKER_EE_URL"
```

* Store the value of the variable, ``DOCKERURL`` (from the previous step), in a yum variable in ``/etc/yum/vars/``: Also, store your OS version string in ``/etc/yum/vars/dockerosversion``. In case we use RedHat Enterprise Linux server 7.x, Docker OS version is ``7``
```
sudo -E sh -c 'echo "$DOCKERURL/rhel" > /etc/yum/vars/dockerurl'
sudo sh -c 'echo "7" > /etc/yum/vars/dockerosversion'
```

* Install required packages: ``yum-utils`` provides the yum-config-manager utility, and ``device-mapper-persistent-data`` and ``lvm2`` are required by the devicemapper storage driver:
```
sudo yum install -y yum-utils \
        device-mapper-persistent-data \
        lvm2
```

* Enable the extras RHEL repository. This ensures access to the container-selinux package required by docker-ee. But case, we do not have subscription, so I will install container-selinux from CentOS mirror instead.
```
sudo yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```

* Add the Docker Engine - Enterprise stable repository:
```
sudo yum-config-manager -v \
        --add-repo \
        "$DOCKERURL/rhel/docker-ee.repo"
```

### Installing Docker Engine Enterprise pacakges
```
sudo yum install -y docker-ee \
        docker-ee-cli \
        containerd.io
```

### Configure ``direct-lvm`` mode for production (Recommend)
* Create LVM Thinpool from devices ``/dev/sdb /dev/sdc`` for docker
```
sudo pvcreate /dev/sdb /dev/sdc
sudo vgcreate docker /dev/sdb /dev/sdc
sudo lvcreate --wipesignatures y -n thinpool docker -l 95%VG
sudo lvcreate --wipesignatures y -n thinpoolmeta docker -l 1%VG
sudo lvconvert -y \
    --zero n \
    -c 512K \
    --thinpool docker/thinpool \
    --poolmetadata docker/thinpoolmeta
```

* Set the thinpool activation profile
```
cat > /etc/lvm/profile/docker-thinpool.profile <<'EOF'
activation {
    thin_pool_autoextend_threshold=80
    thin_pool_autoextend_percent=20
}
EOF
```

* Apply the LVM profile change and monitor
```
sudo lvchange --metadataprofile docker-thinpool docker/thinpool
sudo lvs -o+seg_monitor
sudo lvchange --monitor y docker/thinpool
```
* Configure ``Docker Engine`` to use LVM thinpool
```
mkdir -p /etc/docker
cat > /etc/docker/daemon.json <<'EOF'
{
    "storage-driver": "devicemapper",
    "storage-opts": [
    "dm.thinpooldev=/dev/mapper/docker-thinpool",
    "dm.use_deferred_removal=true",
    "dm.use_deferred_deletion=true"
    ]
}
EOF
```

* Enable and start ``Docker Engine`` service
```
systemctl enable docker --now
```

### Verification Docker software after installation
```
docker info
docker version
```

## Installing ``Docker EE`` Universal Control Plane (UCP)
### UCP Manager Nodes

### UCP Worker Nodes

