[[_TOC_]]
# Getting Started With Swarm Mode
## Step 1 - Initialise Swarm Mode
* Create Swarm Mode Cluster
```
docker swarm --help
docker swarm init
```
* Get Docker Swarm Join-token for Workers
```
docker swarm join-token worker
```
* Get Docker Swarm Join-token for Managers
```
docker swarm join-token manager
```
## Step 2 - Join Cluster
* Obtain the token required to add a worker to the cluster on manager node
```
token=$(ssh -o StrictHostKeyChecking=no 172.17.0.67 "docker swarm join-token -q worker") && echo $token
```
* On the second host, join the cluster by requesting access via the manager. The token is provided as an additional parameter.
```
docker swarm join 172.17.0.67:2377 --token $token
```
* List all nodes with command on manager node
```
docker node ls
```
## Step 3 - Create Overlay Network
* Create a new overlay network called ``dockernet``
```
docker network create -d overlay dockernet
```
## Step 4 - Deploy Service
* In this case, we are deploying the Docker Image katacoda/docker-http-server
```
docker service create \
    --name http \
    --network skynet \
    --replicas 2 \
    -p 80:80 \
    katacoda/docker-http-server
```
* List all Docker Services with command:
```
docker service ls
```
* Test deployment service name http
```
curl -ivv http://<node01 or IP>
curl -ivv http://<node02 or IP>
```
## Step 5 - Inspect State
* list of all the tasks associated with a service across the cluster. In this case, each task is a container
```
docker service ps <service name>
docker service ps http
```
* View the details and configuration of a service
```
docker service inspect --pretty <service name>
docker service inspect --pretty http
```
## Step 6 - Scale Service
* The command below will scale our ``http`` service to be running across five containers
```
docker service scale <service name>=<scale up>
docker service scale http=5
```

# Create Overlay Network
## Step 1 - Initialise Swarm Mode
* Initial Swarm mode
```
docker swarm init
```
* Join Swarm Mode as worker on second node
```
token=$(ssh -o StrictHostKeyChecking=no 172.17.0.41 "docker swarm join-token -q worker") && \
docker swarm join 172.17.0.41:2377 --token $token
```
## Step 2 - Create Network
* Create overlay network called ``app1-network``
```
docker network create -d overlay <overlay network name>
docker network create -d overlay app1-network
```
* List all docker networks
```
docker network ls
```
## Step 3 - Deploy Backend
* The following will deploy a Redis service using the network. The name of the service will be ``redis`` that can be used for discovery via DNS
```
docker service create \
    --name redis \
    --replicas 2 \
    --network app1-network \
    redis:alpine
```
## Step 4 - Deploy Frontend
* Create the new service will the command below
```
docker service create \
    --name app1-web \
    --replicas 2 \
    --network app1-network \
    -p 8080:3000 \
    katacoda/redis-node-docker-example
```
* To verify
```
docker service ls
docker ps
```
* Test
```
curl -ivv http://host01:8080
curl -ivv http://host02:8080
```

# Load Balance and Service Discover in Swarm Mode
## Step 1 - Initialise Cluster
* Initialise Swarm Mode
```
docker swarm init
```

* Add the second host to the cluster
```
docker swarm join 172.17.0.37:2377 --token $(ssh -o StrictHostKeyChecking=no 172.17.0.37 "docker swarm join-token -q worker")
```

## Step 2 - Port Load Balance
* Create a new service named ``lbapp1``
```
docker service create --name lbapp1 --replicas 2 -p 81:80 katacoda/docker-http-server
```

* Test load balancing
```
docker service ps lbapp1
curl host01:81
-> the service will reply from each containers on host01 and host02
```

## Step 3 - Virtual IP and Service Discovery
* Create an attachable overlay network
```
docker network create --attachable -d overlay eg1
```

* Create a service name ``http`` that attached to above network ``eg1``
```
docker service create --name http --replicas 2 --network eg1 katacoda/docker-http-server
```
* Use service inspect command to find the Virtual IP
```
docker service inspect http
```

* Use Dig to fin the internal Virtual IP
```
docker run --name=dig --network eg1 benhall/dig dig http
```

* Ping the name should also discover the IP address
```
docker run --name=ping --network eg1 alpine ping -c 5 http
```

## Step 4 - Multi-Host LB and Service Discovery
* Create an overlay network that app and data can connect to
```
docker network create -d overlay app1-network
```

* Create service name ``redis`` with above overlay network
```
docker service create --name redis --network app1-network redis:alpine
```

* Create service name ``app1-web`` with same overlay network
```
docker service create --name app1-web --network app1-network --replicas 4 -p 80:3000 katacoda/redis-node-docker-example
```

# Apply Rolling Updates Across Swarm Cluster
## Step 1 - Update Limits
* Docker service update help
```
docker service update --help
```

* Docker service add an env 
```
docker service update --add-env KEY=VALUE <service name>
```

* Docker service update CPU and memory limits
```
docker service update --limit-cpu 2 --limit-memory 512mb http
```

* The result will be visible with inspect command
```
docker service inspect --pretty http
```

## Step 2 - Update Replicas
* Update the replicas from 2 to 6
```
docker service update --replicas=6  http
```

* Inspect service to get the result
```
docker service inspect --pretty http | grep Replicas
docker service ls
```

## Step 3 - Update Image
* This command will re-create the instances of our HTTP service with :v2 tag of Docker Image
```
docker service update --image katacoda/docker-http-server:v2 http
```

* Verify
```
docker ps
curl http://docker
```

# Add Healthcheck for Containers
## Step 1 - Creating Service
* Create HTTP service witha Healthcheck
```
cat > Dockerfile <<'EOF'
FROM katacoda/docker-http-server:health
HEALTHCHECK --timeout=1s --interval=1s --retries=3 \
  CMD curl -s --fail http://localhost:80/ || exit 1
EOF
```

* Build and run
```
docker buil -t http .
docker run -d -p 80:80 --name srv http
```

* Verify container healthy with
```
docker ps
```

# Deploy Swarm Services with Compose v3
## Step 1 - Initialise Swarm Mode
## Step 2 - Create Docker Compose file
```
cat > docker-compose.yml <<'EOF'
version: "3"
services:
  redis:
    image: redis:alpine
    volumes:
      - db-data:/data
    networks:
      appnet1:
        aliases:
          - db
    deploy:
      placement:
        constraints: [node.role == manager]

  web:
    image: katacoda/redis-node-docker-example
    networks:
      - appnet1
    depends_on:
      - redis
    deploy:
      mode: replicated
      replicas: 2
      labels: [APP=WEB]
      resources:
        limits:
          cpus: '0.25'
          memory: 512M
        reservations:
          cpus: '0.25'
          memory: 256M
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s
      update_config:
        parallelism: 1
        delay: 10s
        failure_action: continue
        monitor: 60s
        max_failure_ratio: 0.3
      placement:
        constraints: [node.role == worker]

networks:
    appnet1:

volumes:
  db-data:
EOF
```

## Step 3 - Deploy Services
* The docker stack command is used to deploy a Docker Compose Stack via Swarm. In this case, it will prefix the services with myapp.
```
docker stack deploy --compose-file docker-compose.yml myapp
```

* Lists all stacks deployed
```
docker stack ls
```

* Details of internal services
```
docker stack services <stackname>
```

* Details of each service container
```
docker stack ps <stackname>
```

# Deploying Portainer to Docker Swarm Cluster
## Step 1 - Deploy Swarm Cluster
## Step 2 - Deploy Portainer
```
docker service create \
    --name portainer \
    --publish 9000:9000 \
    --constraint 'node.role == manager' \
    --mount type=bind,src=/host/data,dst=/data \
     --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
    portainer/portainer \
    -H unix:///var/run/docker.sock
```

* Access to Portainer via browser ``http://<ip/hostname>:9000``
