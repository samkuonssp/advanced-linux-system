# How to use this script:
# sudo yum install -y curl && bash <(curl https://gitlab.com/samkuonssp/advanced-linux-system/-/raw/master/Docker/Docker%20CE/deploy_docker_swarm_init_manager.sh)

# Updated: 13/02/2020
echo -e "#======== This docker swarm deployment script tried on RHEL 7.x ========#\n"

# =============== Deployment Scripts ================>
install_docker_ce_standalone() {
    if ! [ -x "$(command -v docker)" ]; then
        sudo yum install -y curl && bash <(curl https://gitlab.com/samkuonssp/advanced-linux-system/-/raw/master/Docker/Docker%20CE/docker_ce_standalone_rhel7.x_installer.sh)
    fi
}

local_firewall_settings() {
    firewall-cmd --permanent --new-service=SVR_SWARM
    for SWARM_PORT in 2377/tcp 7946/tcp 7946/udp 4789/udp; do
        echo "Add port $SWARM_PORT to firewall Docker Swarm service SVR_SWARM"
        firewall-cmd --permanent --service=SVR_SWARM --add-port=$SWARM_PORT;
    done
    firewall-cmd --permanent --add-service=SVR_SWARM
    firewall-cmd --reload
}

initialize_docker_swarm() {
    echo -e "\nEnter your docker swarm manager node's IP, i.e: 192.168.43.21\n";
    read -p "Manager node's IP: " DKRMGR_IP
    docker swarm init --advertise-addr $DKRMGR_IP > /dev/null 2>&1
}

docker_swarm_join_token() {
    docker swarm join-token worker
    echo -e "\n";
    docker swarm join-token manager
    echo -e "\n"
}

# ============== Deployment Process ================>
install_docker_ce_standalone
initialize_docker_swarm
local_firewall_settings
docker_swarm_join_token

echo -e "Note: before joining node to swarm, you will need to open local firewall ports: 2377/tcp 7946/tcp 7946/udp 4789/udp on each node as below example:\n";
echo -e 'firewall-cmd --permanent --new-service=SVR_SWARM
for SWARM_PORT in 2377/tcp 7946/tcp 7946/udp 4789/udp; do
    echo "Add port $SWARM_PORT to firewall Docker Swarm service SVR_SWARM"
    firewall-cmd --permanent --service=SVR_SWARM --add-port=$SWARM_PORT;
done
firewall-cmd --permanent --add-service=SVR_SWARM
firewall-cmd --reload'