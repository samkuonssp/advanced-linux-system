cat <<EOF | sudo tee /etc/sysctl.d/docker.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter
sudo sysctl --system

sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a

* Remove existing Docker
```
sudo yum -y remove \
    docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine \
    docker-ce \
    docker-ce-cli \
    && rm -rf /etc/yum.repos.d/docker-ce*.repo
```
* Install requirement packages
```
sudo yum -y install \
    yum-utils \
    device-mapper-persistent-data \
    lvm2 \
    http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```
* Install Docker CE
```
sudo yum-config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum -y install \
    docker-ce \
    docker-ce-cli \
    containerd.io
systemctl enable docker --now
```

# Install Docker Compose
* Check docker-compose version here: https://github.com/docker/compose/releases
```
COMPOSE_VERSION=1.26.2
curl -L -k "https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
```