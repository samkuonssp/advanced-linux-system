# How to use this script:
# sudo yum install -y curl && bash <(curl https://gitlab.com/samkuonssp/advanced-linux-system/-/raw/master/Docker/Docker%20CE/docker_ce_standalone_rhel7.x_installer.sh)

# Updated: 11/02/2020

# =============== Deployment Scripts ================>
uninstall_old_docker() {
    if [ -x "$(command -v docker)" ]; then
        echo -e "\nUninstall old Docker Engine version....\n";
        sudo yum -y remove \
            docker \
            docker-client \
            docker-client-latest \
            docker-common \
            docker-latest \
            docker-latest-logrotate \
            docker-logrotate \
            docker-engine \
            docker-ce \
            docker-ce-cli \
            && rm -rf /etc/yum.repos.d/docker-ce*.repo

        if [ -x "$(command -v docker)" ]; then
            printf "\n\033[0;31mERROR: Cannot uninstall old Docker Engine.\033[0m \nRecheck with system admin again.\n"
            exit
        fi
    fi
}

install_required_packages() {
    echo -e "\nInstall required packages....\n";
    sudo yum -y install \
        yum-utils \
        device-mapper-persistent-data \
        lvm2 \
        http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
}

install_docker_engine() {
    if ! [ -x "$(command -v docker)" ]; then
        echo -e "\nInstall Docker CE latest version....\n";
        sudo yum-config-manager \
            --add-repo https://download.docker.com/linux/centos/docker-ce.repo
        sudo yum -y install \
            docker-ce \
            docker-ce-cli \
            containerd.io
        systemctl enable docker --now

        if ! [ -x "$(command -v docker)" ]; then
            printf "\n\033[0;31mERROR: Cannot install Docker CE.\033[0m \nRead how to install: https://docs.docker.com/install/linux/docker-ce/centos/ and try again.\n"
            exit
        fi
    fi
}

install_docker_compose() {
    if ! [ -x "$(command -v docker-compose)" ]; then
        echo -e "\nFind Docker Compose latest release: https://github.com/docker/compose/releases\n";
        read -p "Docker-compose version: " COMPOSE_VERSION
        sudo curl -L -k "https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
        sudo chmod +x /usr/bin/docker-compose
    
        if ! [ -x "$(command -v docker-compose)" ]; then
            printf "\n\033[0;31mERROR: Cannot install docker-compose version $COMPOSE_VERSION.\033[0m \nRead how to install: https://docs.docker.com/compose/install/ and try again.\n"
            exit
        fi
    fi
}

verify_docker_and_compose_version() {
    echo -e "\nDocker CE & Docker Compose version info....\n";
    sudo docker version && sudo docker-compose version
}

# ============== Deployment Process ================>
echo -e "\n====== Docker CE latest version on RHEL 7.x ======\n";

uninstall_old_docker
install_required_packages
install_docker_engine
install_docker_compose
verify_docker_and_compose_version

echo -e "\nDone! installing latest Docker CE with Docker Compose....\n";