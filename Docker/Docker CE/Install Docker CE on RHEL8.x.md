# Set dependency repository
```
cat > /etc/yum.repos.d/containerdeps.repo <<'EOF'
[containerdeps-baseos]
name=Container BaseOS Deps on RHEL8.x
baseurl=http://mirror.centos.org/centos/$releasever/BaseOS/$basearch/os/
gpgcheck=0
enabled=1
includepkgs=policycoreutils-python-utils python3-policycoreutils checkpolicy audit-libs-python3 policycoreutils python3-libsemanage python3-setools python3-audit libsemanage audit-libs audit libcgroup

[containerdeps-appstream]
name=Container AppStream Deps on RHEL8.x
baseurl=http://mirror.centos.org/centos/$releasever/AppStream/$basearch/os/
gpgcheck=0
enabled=1
includepkgs=container-selinux
EOF
```

# Set local firewall to allow any traffic for Docker0 interface
```
firewall-cmd --permanent --zone=trusted --add-interface=docker0
firewall-cmd --reload
```

# Letting iptables see bridged traffic
```
cat <<EOF | tee /etc/sysctl.d/Docker.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter
sysctl --system
```

# Install Docker CE runtime
* Install requirement packages
```
dnf -y install lvm2 \
    device-mapper-persistent-data \
    container-selinux \
    https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
```

* Install Docker CE
```
dnf config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo

dnf -y install \
    docker-ce \
    docker-ce-cli

systemctl enable docker --now
```

# Install Docker Compose
* Check docker-compose version here: https://github.com/docker/compose/releases
```
COMPOSE_VERSION=1.26.2
curl -L -k "https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
```