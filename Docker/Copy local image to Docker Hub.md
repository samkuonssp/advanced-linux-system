* Create Docker Hub repo
```
1. Login to https://hub.docker.com
2. Create new repository as public
```

* Copy/push local image to Docker HUB
```
docker login
docker tag <local_image>:<tag_version> <docker_hub>/<repo>:<tag>
docker push <docker_hub>/<repo>:<tag>
```

* Example
```
1. Create new docker hub repository - named "samkuonssp/tac_plus"
2. let say our local image - named "tac_plus:1.0.3"
3. Follow the below command:
docker login
docker tag tac_plus:1.0.3 samkuonssp/tac_plus:1.0.3
docker push samkuonssp/tac_plus:1.0.3
```