[[_TOC_]]

# Create VM
* Create a new VM with ``Fedora 64bit`` as the guest operating system for ``Fedora CoreOS`` 
![](./Setup_FCOS/fcos_setup_01.PNG)

* Start live CD boot ``Fedora CoreOS`` as the following:
![](./Setup_FCOS/fcos_setup_02.PNG)

* By default it will automatically login without prompt password
![](./Setup_FCOS/fcos_setup_03.PNG)

# Generate secure password hash
```
sudo openssl password -1 > myignition-file.fcc
--> type password and confirm it
```
![](./Setup_FCOS/fcos_setup_04.PNG)

# Edit FCC file
```
vi myignition-file.fcc
-> re-write as the example:
variant: fcos
version: 1.1.0
passwd:
  users: 
    - name: core
      groups:
        - sudo
        - docker
      password_hash: <your secure encrypted password here>
storage:
  files:
    - path: /etc/hostname
      mode: 0644
      contents:
        inline: <hostname>
    - path: /etc/ssh/sshd_config.d/20-enable-password.conf
      mode: 0644
      contents:
        inline: PasswordAuthentication yes

```
![](./Setup_FCOS/fcos_setup_05.PNG)
![](./Setup_FCOS/fcos_setup_06.PNG)

# Convert FCC to IGN file
```
podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < myignition-file.fcc > myignition-file.ign
```
![](./Setup_FCOS/fcos_setup_08.PNG)

# Get disk list
```
lsblk | grep disk
```
![](./Setup_FCOS/fcos_setup_09.PNG)

# Install CoreOS
```
sudo coreos-installer install -i myignition-file.ign <disk path, i.e: /dev/sda>
reboot
```
![](./Setup_FCOS/fcos_setup_10.PNG)
![](./Setup_FCOS/fcos_setup_11.PNG)

# Change hostname, IP address for CoreOS
* Change hostname
```
hostnamectl set-hostname <new_name>
```

* Set static IP address
```
nmcli connection show
nmcli connection mod '<connection_name>' \
ipv4.addresses 192.168.43.31/24 \
ipv4.gateway 192.168.43.2 \
ipv4.dns 1.1.1.1 \
+ipv4.dns 8.8.8.8 \
connection.autoconnect yes
```

# Reference: 
* https://docs.fedoraproject.org/en-US/fedora-coreos/authentication/
* https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/
* https://docs.fedoraproject.org/en-US/fedora-coreos/authentication/#_enabling_ssh_password_authentication