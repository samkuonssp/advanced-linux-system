```
for i in kubectl kubeadm kubelet;
do
  curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/$i"
done
```

```
kubever=v1.19.0
for i in kubectl kubeadm kubelet;
do
  curl -LO https://storage.googleapis.com/kubernetes-release/release/$kubever/bin/linux/amd64/$i
done
```

```
cat > /etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

sudo rpm-ostree install kubelet kubectl kubeadm -r

```
