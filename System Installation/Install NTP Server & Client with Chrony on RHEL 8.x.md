***Install NTP Server & Client with Chrony on RHEL 8.x***

[[_TOC_]]

# Overview
## LAB info
## LAB diagram

# NTP Server
## Installing ``chrony`` package
```
dnf install chrony
```

## Configuring ``chrony`` as NTP server
```
sudo cp /etc/chrony.conf{,.orig_`date +"%d%m%Y"`}
cat <<'EOF' | sudo tee /etc/chrony.conf
# Publics and References NTP servers
server 0.asia.pool.ntp.org iburst
server 1.asia.pool.ntp.org iburst
server 2.asia.pool.ntp.org iburst
server 3.asia.pool.ntp.org iburst

# Allow NTP client Networks
allow 192.168.100.0/24

# Ignore stratum when selecting the source
stratumweight 0

# Record the rate at which the system clock gains/losses time.
driftfile /var/lib/chrony/drift

# Allow the system clock to be stepped in the first three updates. 
# if its offset is larger than 10 second.
makestep 10 3
maxchange 1000 1 2

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync

# Specify an IP address of an interface on which chronyd will listen 
# for monitoring command
bindcmdaddress 127.0.0.1
bindcmdaddress ::1

# The location of the key file
keyfile /etc/chrony.keys

# The key chronyd will use for authentication of user commands
commandkey 1

# Generate command key if missing.
generatecommandkey

# Specifies that client accesses are not to be logged
noclientlog

# Generate syslog message if a system clock error of over 0.1 seconds
logchange 0.1

# Specify directory for log files.
logdir /var/log/chrony

# Select which information is logged.
log measurements statistics tracking

EOF
```
## Enable and Start ``chrony`` service
```
systemctl enable chronyd --now
```

## Local firewall configuration
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 service name=ntp accept'
firewall-cmd --reload
```

## Set the system hardware clock from the OS time
```
/sbin/hwclock --systohc
```

# NTP Client
## Installing ``chrony`` package
```
dnf install chrony
```

## Configuring ``chrony`` as NTP Client
```
sudo cp /etc/chrony.conf{,.orig_`date +"%d%m%Y"`}
cat <<'EOF' | sudo tee /etc/chrony.conf
# Internal and Centralize NTP server
server 192.168.100.31 iburst

# Ignore stratum when selecting the source
stratumweight 0

# Record the rate at which the system clock gains/losses time.
driftfile /var/lib/chrony/drift

# Allow the system clock to be stepped in the first three updates. if its offset is larger than 10 second.
makestep 10 3
maxchange 1000 1 2

# Enable kernel synchronization of the real-time clock (RTC).
rtcsync

# Specify an IP address of an interface on which chronyd will listen 
# for monitoring command
bindcmdaddress 127.0.0.1
bindcmdaddress ::1

# Generate syslog message if a system clock error of over 0.1 seconds
logchange 0.1

# Specify directory for log files.
logdir /var/log/chrony

# Select which information is logged.
log measurements statistics tracking

EOF
```
## Enable and Start ``chrony`` service
```
systemctl enable chronyd --now
```

## Set the system hardware clock from the OS time
```
/sbin/hwclock --systohc
```

# Verification
**Run the chronyc tracking command to check chrony tracking**
```
chronyc tracking
```
* ``Reference ID``: This is the reference ID and name (or IP address) if available, of the server to which the computer is currently synchronized.
* ``Stratum``: The stratum indicates how many hops away from a computer with an attached reference clock you are.
* ``Ref time``: This is the time (UT C) at which the last measurement from the reference source was processed.

**Run the chronyc sources command to display information about the current time sources that chronyd is accessing**
```
chronyc sources
```
* ``M``: The mode of the source. ^ means a server, = means a peer, and # indicates a locally connected reference clock.
* ``S``: The state of the sources. “*” indicates the source to which chronyd is currently synchronized. “+” indicates acceptable sources that are combined with the selected source. “-” indicates acceptable sources that are excluded by the combining algorithm. “?” indicates sources to which connectivity has been lost or whose packets do not pass all tests. “x” indicates a clock that chronyd thinks is a false ticker, that is, its time is inconsistent with a majority of other sources. “~” indicates a source whose time appears to have too much variability. The “?” condition is also shown at start-up, until at least three samples have been gathered from it.
* ``Name/IP address``: This shows the name or the IP address of the source, or reference ID for reference clocks.

**Run the chronyc sourcestats to displays information about the drift rate and offset estimation**
```
chronyc sourcestats
```
