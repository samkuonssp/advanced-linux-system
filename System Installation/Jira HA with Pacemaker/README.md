firewall-cmd --permanent --add-service=high-availability
firewall-cmd --reload

cat <<'EOF' | sudo tee -a /etc/hosts
192.168.100.31  rh7ha01
192.168.100.32  rh7ha02
EOF

cat <<'EOF' | sudo tee /etc/yum.repos.d/ha.repo
[HA]
name=High Availability repository
baseurl=http://mirror.centos.org/centos/7/os/x86_64/
enabled=1
gpgcheck=0
EOF

yum install pacemaker pcs resource-agents

# Install GlusterFS
cat >> /etc/hosts <<'EOF'
192.168.100.31   rh7ha01
192.168.100.32   rh7ha02
EOF

## Online packages installation
* Add GlusterFS repository
```
cat > /etc/yum.repos.d/gluster.repo <<'EOF'
[gluster-7]
name=Gluster 7
baseurl=http://mirror.centos.org/centos/$releasever/storage/$basearch/gluster-7/
gpgcheck=0
enabled=1
EOF
```
* Install GlusterFS package
```
yum install -y \
    glusterfs-server \
    userspace-rcu \
    userspace-rcu-devel
```
* Start and enable GlusterFS service
```
systemctl enable --now glusterd
systemctl status glusterd
```

firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.100.31 service name=glusterfs accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.100.32 service name=glusterfs accept'
firewall-cmd --reload

* From any single server. i.e: from server1 - ``rh7ha01``
```
gluster peer probe rh7ha02
```
* Check the peer status
```
gluster peer status
```

## Setup GlusterFS volume
* On all servers, create sub-directory for GlusterFS volume, i.e: ``data``
```
mkdir /datastore
```
* From any signel server, create GlusterFS volume and named ``data``
```
gluster volume create jiradata replica 2 rh7ha01:/datastore/jiradata rh7ha02:/datastore/jiradata force
gluster volume start jiradata
```
* Confirm that the volume shows "Started"
```
gluster volume info
```
yum install -y \
    glusterfs-client-xlators \
    glusterfs-libs \
    glusterfs \
    glusterfs-fuse

echo "rh7ha01:/jiradata /jiradata glusterfs defaults 0 0" >> /etc/fstab
mount -a

# Install Jira
* Install java
yum install -y java-11-openjdk java-11-openjdk-devel

* Create systemd
cat <<'EOF' | sudo tee /lib/systemd/system/jira.service
[Unit] 
Description=Atlassian Jira
After=network.target

[Service] 
Type=forking
User=jira
PIDFile=/opt/atlassian/jira/work/catalina.pid
ExecStart=/opt/atlassian/jira/bin/start-jira.sh
ExecStop=/opt/atlassian/jira/bin/stop-jira.sh

[Install] 
WantedBy=multi-user.target
EOF

chmod 664 /lib/systemd/system/jira.service

systemctl daemon-reload

# Install nginx
* Create official Nginx Repository
```
cat > /etc/yum.repos.d/nginx.repo <<'EOF'
[nginx-stable]
name=nginx stable repo for rhel
baseurl=http://nginx.org/packages/rhel/$releasever/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true
EOF
```

* Install nginx package
yum install nginx

* Nginx configuration directories
  - ``/etc/nginx``: Nginx directory to store docker-compose and main nginx configuration
  - ``/etc/nginx/certs``: Certificate directory for Nginx
  - ``/etc/nginx/conf.d/``: Nginx directory to store all proxy sites
  - ``/etc/nginx/conf.d/global.d``: Nginx directory to store all secure settings configuration files
```
mkdir -p /etc/nginx/{certs,conf.d/global.d}
```

* In case, we use self-signed certificate and private key and follow:
  -  Create certificate
```
cd /etc/nginx/certs/
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-private.key -out nginx-certificate.pem
```

  - Generate strong DHE (Ephemeral Diffie-Hellman) parameters
```
openssl dhparam -out dhparam.pem 2048
```

  - Restrict SSL directory, private key and selfsigned certificate
```
chmod 400 /etc/nginx/certs/*.{key,pem}
chmod 600 /etc/nginx/certs
```
* Create redirection ``conf.d/redirect.conf``
```
cat > /etc/nginx/conf.d/redirect.conf <<'EOF'
server {
  listen        80;
  server_name   _;
  return 301 https://$host$request_uri;
}
EOF
```

* Secure nginx reverse proxy
  - Create ``global.d/common.conf``
```
cat > /etc/nginx/conf.d/global.d/common.conf <<'EOF'
add_header Strict-Transport-Security    "max-age=31536000; includeSubDomains" always;
add_header X-Frame-Options              SAMEORIGIN;
add_header X-Content-Type-Options       nosniff;
add_header X-XSS-Protection             "1; mode=block";
EOF
```

  - Create ``global.d/common_location.conf``
```
cat > /etc/nginx/conf.d/global.d/common_location.conf <<'EOF'
proxy_set_header    X-Real-IP           $remote_addr;
proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
proxy_set_header    X-Forwarded-Proto   $scheme;
proxy_set_header    Host                $host;
proxy_set_header    X-Forwarded-Host    $host;
proxy_set_header    X-Forwarded-Port    $server_port;
client_max_body_size 10M;
EOF
```

  - Create ``global.d/ssl.conf``
```
cat > /etc/nginx/conf.d/global.d/ssl.conf <<'EOF'
ssl_protocols               TLSv1.2;
ssl_ecdh_curve              secp384r1;
ssl_ciphers                 "ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384 OLD_TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 OLD_TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256";
ssl_prefer_server_ciphers   on;
ssl_dhparam                 /etc/nginx/certs/dhparam.pem;
ssl_certificate             /etc/nginx/certs/nginx-certificate.pem;
ssl_certificate_key         /etc/nginx/certs/nginx-private.key;
ssl_session_timeout         10m;
ssl_session_cache           shared:SSL:10m;
ssl_session_tickets         off;
ssl_stapling                on;
ssl_stapling_verify         on;
EOF
```

* Copy to node2
scp -r /etc/nginx/{certs,conf.d/global.d} root@192.168.100.32:/etc/nginx/
chmod 400 /etc/nginx/certs/*.{key,pem}
chmod 600 /etc/nginx/certs

cat > /etc/nginx/conf.d/jira.lab.local.conf <<'EOF'
upstream jira {
  server        192.168.100.33:8080;
}

server {
  listen        443 ssl;
  server_name   jira.lab.local;

  merge_slashes on;
  msie_padding on;

  include       /etc/nginx/conf.d/global.d/common.conf;
  include       /etc/nginx/conf.d/global.d/ssl.conf;

  location / {
    proxy_pass  http://jira;
    include     /etc/nginx/conf.d/global.d/common_location.conf;
  }
}
EOF

# Create the Cluster
systemctl enable corosync pacemaker pcsd --now

echo CHANGEME | passwd --stdin hacluster

* Any node
pcs cluster auth rh7ha01 rh7ha02 -u hacluster -p CHANGEME --force

pcs cluster setup --force --name jira rh7ha01 rh7ha02

# Start the cluster
pcs cluster start --all
systemctl restart pcsd pacemaker corosync

# Set Cluster options
pcs property set stonith-enabled=false
pcs property set no-quorum-policy=ignore
pcs resource defaults migration-threshold=1

pcs cluster stop --all && pcs cluster start --all

pcs resource create jira_vip ocf:heartbeat:IPaddr2 ip=192.168.100.33 cidr_netmask=24 op monitor interval=20s --group=cluster_vip

pcs resource create jira_server systemd:jira op monitor interval=10s
pcs resource create nginx_proxy systemd:nginx op monitor interval=10
pcs resource group add group_jira jira_server nginx_proxy
pcs constraint colocation add group_jira cluster_vip INFINITY
pcs constraint order cluster_vip then group_jira

firewall-cmd --permanent --add-port=443/tcp