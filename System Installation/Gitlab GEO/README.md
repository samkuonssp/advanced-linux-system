[[_TOC_]]

# Use Cases
Implementing Geo provides the following benefits:
* Reduce from minutes to seconds the time taken for your distributed developers to clone and fetch large repositories and projects.
* Enable all of your developers to contribute ideas and work in parallel, no matter where they are.
* Balance the read-only load between your primary and secondary nodes.

In addition, it:
* Can be used for cloning and fetching projects, in addition to reading any data available in the GitLab web interface (see current limitations).
* Overcomes slow connections between distant offices, saving time by improving speed for distributed teams.
* Helps reducing the loading time for automated tasks, custom integrations, and internal workflows.
* Can quickly fail over to a secondary node in a disaster recovery scenario.
* Allows planned failover to a secondary node.

Geo provides:
* Read-only secondary nodes: Maintain one primary GitLab node while still enabling read-only secondary nodes for each of your distributed teams.
* Authentication system hooks: Secondary nodes receives all authentication data (like user accounts and logins) from the primary instance.
* An intuitive UI: Secondary nodes utilize the same web interface your team has grown accustomed to. In addition, there are visual notifications that block write operations and make it clear that a user is on a secondary node.

# How it works
Your Geo instance can be used for cloning and fetching projects, in addition to reading any data. This will make working with large repositories over large distances much faster.
![](./geo_overview.png)

When Geo is enabled, the:
* Original instance is known as the primary node.
* Replicated read-only nodes are known as secondary nodes.

Keep in mind that:
* Secondary nodes talk to the primary node to:
  * Get user data for logins (API).
  * Replicate repositories, LFS Objects, and Attachments (HTTPS + JWT).
* Since GitLab Premium 10.0, the primary node no longer talks to secondary nodes to notify for changes (API).
* Pushing directly to a secondary node (for both HTTP and SSH, including Git LFS) was introduced in GitLab Premium 11.3.
* There are limitations in the current implementation

# Gitlab Geo Architecture
![](./geo_architecture.png)

In this diagram:
* There is the primary node and the details of one secondary node.
* Writes to the database can only be performed on the primary node. A secondary node receives database updates via PostgreSQL streaming replication.
* If present, the LDAP server should be configured to replicate for Disaster Recovery scenarios.
* A secondary node performs different type of synchronizations against the primary node, using a special authorization protected by JWT:
  * Repositories are cloned/updated via Git over HTTPS.
  * Attachments, LFS objects, and other files are downloaded via HTTPS using a private API endpoint.

From the perspective of a user performing Git operations:
* The primary node behaves as a full read-write GitLab instance.
* Secondary nodes are read-only but proxy Git push operations to the primary node. This makes secondary nodes appear to support push operations themselves.

To simplify the diagram, some necessary components are omitted. Note that:
* Git over SSH requires gitlab-shell and OpenSSH.
* Git over HTTPS required gitlab-workhorse.

Note that a secondary node needs two different PostgreSQL databases:
* A read-only database instance that streams data from the main GitLab database.
* Another database instance used internally by the secondary node to record what data has been replicated.

In secondary nodes, there is an additional daemon: Geo Log Cursor

# Requirements for running Gitlab Geo
The following are required to run Geo:
* An operating system that supports OpenSSH 6.9+ (needed for fast lookup of authorized SSH keys in the database) The following operating systems are known to ship with a current version of OpenSSH:
  * CentOS 7.4+ (Incase we use RHEL 8.x)
  * Ubuntu 16.04+
* PostgreSQL 11+ with FDW support and Streaming Replication
* Git 2.9+
* All nodes must run the same GitLab version.

Additionally, check GitLab’s minimum requirements, and we recommend you use:
* At least GitLab Enterprise Edition 10.0 for basic Geo features.
* The latest version for a better experience.

## Server Info
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| gitgeo01 | 192.168.43.55 | RHEL 8.x | Gitlab Geo Pimary Node |  |
| gitgeo02 | 192.168.43.56 | RHEL 8.x | Gitlab Geo Secondary Node |  |

```
cat >> /etc/hosts <<'EOF'
192.168.43.55   gitgeo01
192.168.43.56   gitgeo02
EOF
```

## Firewall rules
The following table lists basic ports that must be open between the ``primary`` and ``secondary`` nodes for Geo.
* 80/TCP or HTTP
* 443/TCP or HTTPs
* 22/TCP
```
firewall-cmd --permanent --new-service=gitlab-geo
firewall-cmd --permanent --service=gitlab-geo --set-description="Gitlab Geo Services"
firewall-cmd --permanent --service=gitlab-geo --add-port=80/tcp
firewall-cmd --permanent --service=gitlab-geo --add-port=443/tcp
firewall-cmd --permanent --service=gitlab-geo --add-port=22/tcp
firewall-cmd --permanent --service=gitlab-geo --add-port=5432/tcp
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.55 service name=gitlab-geo accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.56 service name=gitlab-geo accept'
firewall-cmd --reload
```

# Setup Instructions
## Install Gitlab Enterprise Edition on ``primary`` and ``secondary``
* Install the necessary dependencies
```
dnf install -y \
    curl \
    policycoreutils \
    openssh-server \
    postfix
```

* Gitlab require “sshd & postfix” service enable and start
```
systemctl enable sshd postfix --now
```

### Online Gitlab EE Package installation
* Add the GitLab package repository and install the package
```
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash

```

* If you find Curl error message with certificate, then you can just disable SSL verification in Gitlab repo
```
sed -i 's/^sslverify=.*/sslverify=0/g' /etc/yum.repos.d/gitlab_gitlab-ee.repo
```

* Install Gitlab EE package by using ``http`` and wait until finish. Note, we will configure ``https`` later with certificate
  * On ``primary``
```
echo "192.168.43.55   gitlab   gitlab.lab.local" >> /etc/hosts
EXTERNAL_URL="http://gitlab.lab.local" dnf install -y gitlab-ee
```

  * On ``secondary``. ``Note:`` Do not create an account or login to the second node yet.
```
echo "192.168.43.56   gitlab   gitlab.lab.local" >> /etc/hosts
EXTERNAL_URL="http://gitlab.lab.local" dnf install -y gitlab-ee
```

### Offline Gitlab EE Package installation (No internet access)
* Download official Gitlab EE package from this URL: https://packages.gitlab.com/gitlab/gitlab-ee
* Example, here is the latest Gitlab EE RPM package: ``
* Copy this download package to Gitlab server
* Move place where you copy Gitlab EE package to, then install package with below command:
```
echo "192.168.43.55   gitlab   gitlab.lab.local" >> /etc/hosts
EXTERNAL_URL="http://gitlab.lab.local" dnf localinstall -y gitlab-ee-*.rpm
```

## Adding secondary nodes
1. Install GitLab Enterprise Edition on the server that will serve as the secondary node. Do not create an account or log in to the new secondary node.
2. Upload the GitLab License on the primary node to unlock Geo. The license must be for GitLab Premium or higher.
3. Set up the database replication (primary (read-write) <-> secondary (read-only) topology). 
4. Configure fast lookup of authorized SSH keys in the database. This step is required and needs to be done on both the primary and secondary nodes.
5. Configure GitLab to set the primary and secondary nodes.
6. Optional: Configure a secondary LDAP server for the secondary node. See notes on LDAP.
7. Follow the “Using a Geo Server” guide

### Geo database replication
* Link: https://docs.gitlab.com/ee/administration/geo/replication/database.html

#### Step 1. Configure the primary server
* SSH into your GitLab primary server and login as root
* Edit /etc/gitlab/gitlab.rb and add a unique name for your node
```
vim /etc/gitlab/gitlab.rb
 -> gitlab_rails['geo_node_name'] = 'gitprimary'
```

* Reconfigure the primary node for the change to take effect:
```
 gitlab-ctl reconfigure
```

* Execute the command below to define the node as primary node:
```
gitlab-ctl set-geo-primary-node
```

* GitLab 10.4 and up only: Do the following to make sure the gitlab database user has a password defined:
```
gitlab-ctl pg-password-md5 gitlab
# Enter password: <your_password_here>
# Confirm password: <your_password_here>
# fca0b89a972d69f00eb3ec98a5838484

vim /etc/gitlab/gitlab.rb
-> postgresql['sql_user_password'] = 'a000958a14400ad96630bf8187f778a3'
-> gitlab_rails['db_password'] = 'password#2'
```

* Omnibus GitLab already has a replication user called gitlab_replicator. You must set the password for this user manually. You will be prompted to enter a password:
```
gitlab-ctl set-replication-password
```

* Configure PostgreSQL to listen on network interfaces:
```
vim /etc/gitlab/gitlab.rb
-> 
## Geo Primary Role
roles ['geo_primary_role']

-> postgresql['listen_address'] = '192.168.43.55'
-> postgresql['md5_auth_cidr_addresses'] = ['192.168.43.55/32', '192.168.43.56/32']
-> postgresql['max_replication_slots'] = 1
-> gitlab_rails['auto_migrate'] = false
```

* Save the file and reconfigure GitLab for the database listen changes and the replication slot changes to be applied:
```
gitlab-ctl reconfigure
gitlab-ctl restart postgresql
```

* Re-enable migrations now that PostgreSQL is restarted and listening on the private address.
```
vim /etc/gitlab/gitlab.rb
-> gitlab_rails['auto_migrate'] = true

gitlab-ctl reconfigure
```

* Now that the PostgreSQL server is set up to accept remote connections, run netstat -plnt | grep 5432 to make sure that PostgreSQL is listening on port 5432 to the primary server’s private address.

* A certificate was automatically generated when GitLab was reconfigured. This will be used automatically to protect your PostgreSQL traffic from eavesdroppers, but to protect against active (“man-in-the-middle”) attackers, the secondary node needs a copy of the certificate. Make a copy of the PostgreSQL server.crt file on the primary node by running this command:
```
cat ~gitlab-psql/data/server.crt
```

#### Step 2. Configure the secondary server
* SSH into your GitLab secondary server and login as root:
* Stop application server and Sidekiq. ``Note:`` This step is important so we don’t try to execute anything before the node is fully configured.
```
gitlab-ctl stop puma
gitlab-ctl stop sidekiq
```

* Check TCP connectivity to the primary node’s PostgreSQL server:
```
gitlab-rake gitlab:tcp_check[192.168.43.55,5432]
-> TCP connection from 192.168.43.56:40752 to 192.168.43.55:5432 succeeded
```

* Create a file server.crt in the secondary server, with the content you got on the last step of the primary node’s setup:
```
cat > server.crt <<'EOF'
-----BEGIN CERTIFICATE-----
MIIFBzCCAu+gAwIBAgIBADANBgkqhkiG9w0BAQsFADBHMQwwCgYDVQQGEwNVU0Ex
DzANBgNVBAoMBkdpdExhYjERMA8GA1UECwwIRGF0YWJhc2UxEzARBgNVBAMMClBv
c3RncmVTUUwwHhcNMjAwODIwMDUxNzEwWhcNMzAwODE4MDUxNzEwWjBHMQwwCgYD
VQQGEwNVU0ExDzANBgNVBAoMBkdpdExhYjERMA8GA1UECwwIRGF0YWJhc2UxEzAR
BgNVBAMMClBvc3RncmVTUUwwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoIC
AQC4PBHlHZn3dBKD7wAuiSV2L3iGeqz3PIB7pOr6rvu4TG7FdjREeRD2EWHlA0p0
Dmf78YHvISJlz2Kn6XTSaeUvwPsHqAyte/jx36uDDGls4WaHiPAzSiwz4crsEc0N
u2uamHLfqVtgvLsozrr7/ITLC/pNuw/vM1vkY+7eZMBgFNDumiK8tUfQMI/fPErB
pNwmQrge20Mt0rBrej8EavmiZlY4ruzKTnpG7MnCoYCtVDYwyweCjGOo2BLyzchG
bS54itjcA2s+Y5lK8iYsTsr+BRFS9fUqhCzEKlFU0yu336E/Yq0uf2GFrszcm3Ro
/s5iPFaaP0oiIeedltRFPysMEDXmdpGiGMFQyndSxYYGeb8b4uhIQ2oOdL/WFGcL
aE/8OhhMudS1I3vA9yqQZ0FJSgAn2kgGE6mtsEirr+gBqwz1GW69L9ISrcYeZw8r
sXAdSOKUHO+GrIcmsKqoJoPhwuFv6nNmuyGdGVwxuAXfR0BwpuJhiJC8SgqM9cwr
cmcgHipHbgxIye8su4Nwz16vcurjw8WUWf6YHvNWubPQCGRtqPQkvOZHfuJf2hDR
LfjvnrByjH/PByADLtar0ZOYu9ERiBK5mdq/cbq3jOavHy4extimwag2iidaWGJv
GuFkyCsIYtnfOeDtyWCC/qw3grHPmIT+mxEk09xI/oF3/wIDAQABMA0GCSqGSIb3
DQEBCwUAA4ICAQBJQ/Rl/KF7XlKQwlEtGVMQ2KnkvXT3PWuCor2xYAdGqXQtsWn8
rhcWBoEVzQHxAScjD+rcfUZ6/O1uBoLrhZpRqaXX+yjW3D34Yu+MC4JRSUxYyJvI
Z99bsX/GpEDfNbnBB+ImFaKhJap0wcKrzfLjbjpwWkp5/puAAfiB2ayvYeTihtbq
EaNmV6g04Rg3ZcwQFcFc3C1IXiaBk35QqxVM8WVmuq/hpqCzzTi/PxJKf+X8F959
VA0hU22IsgtcEzy5bU02j2jtJohlZ6A/SL07HIEF+c/wy8XUkI1meVLbyHOSt2dW
fV1YeVlRC5vDsVoJEFRruUGvKMVMSvmkZGhQBCS1fGDwjzRDehb31I0JaTOQAuFJ
f9ngHX2LMhPbcBfOl8VFL3m72xhRnvjKztamriiiu5p6LJ9HrZhiQr5XevuttbZM
NjoWL0gWfd8XATL53Tk/sztTi+zmxHyCZbC4PGQquSiNjFP2S0awUB61lE2yfaNY
2VFAG+0zJb/OIwkB7jxlebs6e6it2vH76Nkp912jbxVkyod1HDNhfvAUn4PhEnfN
lNaQhGfzPw3eLd10Wu28jy9zGENR2nndoP69XDFy6l6ENeLqC7WLU3vhkHVs8lCu
FeYsdsXD9ioGcOSPzbry1ge9n0LqHhsZu0EYZ0MmAGopa0+7J+kb+9Ju7g==
-----END CERTIFICATE-----
EOF
```

* Set up PostgreSQL TLS verification on the secondary node:
```
install \
   -D \
   -o gitlab-psql \
   -g gitlab-psql \
   -m 0400 \
   -T server.crt ~gitlab-psql/.postgresql/root.crt
```

* Test that the gitlab-psql user can connect to the primary node’s database (the default Omnibus database name is gitlabhq_production):
```
sudo \
   -u gitlab-psql /opt/gitlab/embedded/bin/psql \
   --list \
   -U gitlab_replicator \
   -d "dbname=gitlabhq_production sslmode=verify-ca" \
   -W \
   -h 192.168.43.55
```

* Configure PostgreSQL:
```
vim /etc/gitlab/gitlab.rb
-> 
## Geo Secondary role
roles ['geo_secondary_role']

-> postgresql['listen_address'] = '192.168.43.56'
-> postgresql['md5_auth_cidr_addresses'] = ['192.168.43.56/32']
-> postgresql['sql_user_password'] = 'a000958a14400ad96630bf8187f778a3'
-> gitlab_rails['db_password'] = 'password#2'
```

* Reconfigure GitLab for the changes to take effect:
```
gitlab-ctl reconfigure
```

* Restart PostgreSQL for the IP change to take effect:
```
gitlab-ctl restart postgresql
```

#### Step 3. Initiate the replication process
* SSH into your GitLab secondary server and login as root:
* Choose a database-friendly name to use for your secondary node to use as the replication slot name. For example, if your domain is secondary.geo.example.com, you may use secondary_example as the slot name as shown in the commands below.
* Execute the command below to start a backup/restore and begin the replication. ``Warning:`` Each Geo secondary node must have its own unique replication slot name. Using the same slot name between two secondaries will break PostgreSQL replication.
```
gitlab-ctl replicate-geo-database \
   --slot-name=gitgeo02_lab \
   --host=192.168.43.55

-> type: replicate
-> type gitlab_replicator password: 
```

### Fast lookup of authorized SSH keys in the database
* Link: https://docs.gitlab.com/ee/administration/operations/fast_ssh_key_lookup.html
* On both nodes, Setting up fast lookup via GitLab Shell
```
cat >> /etc/ssh/sshd_config <<'EOF'
## For Gitlab Geo
Match User git    # Apply the AuthorizedKeysCommands to the git user only
  AuthorizedKeysCommand /opt/gitlab/embedded/service/gitlab-shell/bin/gitlab-shell-authorized-keys-check git %u %k
  AuthorizedKeysCommandUser git
Match all    # End match, settings apply to all users again
EOF

systemctl restart sshd
```

### Configuring a new secondary node
#### Step 1. Manually replicate secret GitLab values
* SSH into the primary node, and execute the command below:
```
cat /etc/gitlab/gitlab-secrets.json
```

* SSH into the secondary node and login as the root user:
* Make a backup of any existing secrets:
```
cp -p /etc/gitlab/gitlab-secrets.json{,_`date +"%d%m%Y"`}
```
* Copy ``/etc/gitlab/gitlab-secrets.json`` from ``primary`` node
```
scp root@192.168.43.55:/etc/gitlab/gitlab-secrets.json /etc/gitlab/
```

* Ensure the file permissions are correct:
```
chown root:root /etc/gitlab/gitlab-secrets.json
chmod 0600 /etc/gitlab/gitlab-secrets.json
```

* Reconfigure the secondary node for the change to take effect:
```
gitlab-ctl reconfigure
gitlab-ctl restart
```

#### Step 2. Manually replicate the primary node’s SSH host keys
#### Step 3. Add the secondary node
* SSH into your GitLab secondary server and login as root:
* Edit /etc/gitlab/gitlab.rb and add a unique name for your node. You will need this in the next steps:
```
vim /etc/gitlab/gitlab.rb
-> gitlab_rails['geo_node_name'] = 'gitsecondary'
```

* Reconfigure the secondary node for the change to take effect:
```
gitlab-ctl reconfigure
```

* Visit the primary node’s Admin Area > Geo (/admin/geo/nodes) in your browser.
* Click the New node button
* Fill in Name with the ``gitlab_rails['geo_node_name']`` in ``/etc/gitlab/gitlab.rb``. These values must always match exactly, character for character.
* Fill in URL with the external_url in /etc/gitlab/gitlab.rb. These values must always match, but it doesn’t matter if one ends with a / and the other doesn’t.
* Optionally, choose which groups or storage shards should be replicated by the secondary node. Leave blank to replicate all. Read more in selective synchronization.
* Click the Add node button to add the secondary node.
* SSH into your GitLab secondary server and restart the services:
```
gitlab-ctl restart
```

* Check if there are any common issue with your Geo setup by running:
```
gitlab-rake gitlab:geo:check
gitlab-rake gitlab:geo:status
```


#### Step 4. Enabling Hashed Storage
#### Step 5. (Optional) Configuring the secondary node to trust the primary node
#### Step 6. Enable Git access over HTTP/HTTPS
#### Step 7. Verify proper functioning of the secondary node

## Promoting a secondary Geo node in single-secondary configurations
* Step 1. Allow replication to finish if possible
* Step 2. Permanently disable the primary node
If you do not have SSH access to the primary node, take the machine offline and prevent it from rebooting by any means at your disposal. Since there are many ways you may prefer to accomplish this, we will avoid a single recommendation. You may need to:
  * Reconfigure the load balancers.
  * Change DNS records (for example, point the primary DNS record to the secondary node in order to stop usage of the primary node).
  * Stop the virtual servers.
  * Block traffic through a firewall.
  * Revoke object storage permissions from the primary node.
  * Physically disconnect a machine
* Step 3. Promoting a secondary node running on a single machine
1. SSH in to your secondary node and login as root:
2. Edit ``/etc/gitlab/gitlab.rb`` to reflect its new status as primary by removing any lines that enabled the ``geo_secondary_role``:
```
vim /etc/gitlab/gitlab.rb
-> # roles ['geo_secondary_role']
```
3. Promote the secondary node to the primary node.
```
gitlab-ctl promote-to-primary-node
```

* Step 4. (Optional) Updating the primary domain DNS record
1. SSH into the secondary node and login as root:
2. Update the primary domain’s DNS record. After updating the primary domain’s DNS records to point to the secondary node, edit /etc/gitlab/gitlab.rb on the secondary node to reflect the new URL:
```
vim /etc/gitlab/gitlab.rb
-> external_url 'https://<new_external_url>'
```
3. Reconfigure the secondary node for the change to take effect:
```
gitlab-ctl reconfigure
```
4. Execute the command below to update the newly promoted primary node URL:
```
gitlab-rake geo:update_primary_node_url
```

* Step 6. (Optional) Removing the secondary’s tracking database
Every secondary has a special tracking database that is used to save the status of the synchronization of all the items from the primary. Because the secondary is already promoted, that data in the tracking database is no longer required.

The data can be removed with the following command:
```
sudo rm -rf /var/opt/gitlab/geo-postgresql
```


# Optional
* check public IP: ``curl --silent ipinfo.io/ip``

# References:
* https://docs.gitlab.com/ee/administration/geo/replication/
* https://about.gitlab.com/install/
