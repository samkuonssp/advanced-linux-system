[[_TOC_]]

# Gitlab Geo for multiple nodes Architecture
![](./geo-ha-diagram.png)

## Server Info
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| gitgeoprapp01 | 192.168.43.55 | RHEL 8.x | Gitlab Geo Pimary App-01 |  |
| gitgeoprapp02 | 192.168.43.56 | RHEL 8.x | Gitlab Geo Pimary App-02 |  |
| gitgeoseapp01 | 192.168.43.57 | RHEL 8.x | Gitlab Geo Secondary App-01 |  |
| gitgeoseapp02 | 192.168.43.58 | RHEL 8.x | Gitlab Geo Secondary App-02 |  |

```
cat >> /etc/hosts <<'EOF'
192.168.43.55   gitgeoprapp01
192.168.43.56   gitgeoprapp02
192.168.43.57   gitgeoseapp01
192.168.43.58   gitgeoseapp02
EOF
```

## Firewall rules
The following table lists basic ports that must be open between the ``primary`` and ``secondary`` nodes for Geo.
* 80/TCP or HTTP
* 443/TCP or HTTPs
* 22/TCP
```
firewall-cmd --permanent --new-service=gitlab-geo
firewall-cmd --permanent --service=gitlab-geo --set-description="Gitlab Geo Services"
firewall-cmd --permanent --service=gitlab-geo --add-port=80/tcp
firewall-cmd --permanent --service=gitlab-geo --add-port=443/tcp
firewall-cmd --permanent --service=gitlab-geo --add-port=22/tcp
firewall-cmd --permanent --service=gitlab-geo --add-port=5432/tcp
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.55 service name=gitlab-geo accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.56 service name=gitlab-geo accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.57 service name=gitlab-geo accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.58 service name=gitlab-geo accept'
firewall-cmd --reload
```

# Setup Instructions
## Install Gitlab Enterprise Edition on all ``primary`` and ``secondary`` nodes
* Install the necessary dependencies
```
dnf install -y \
    curl \
    policycoreutils \
    openssh-server \
    postfix
```

* Gitlab require “sshd & postfix” service enable and start
```
systemctl enable sshd postfix --now
```

### Online Gitlab EE Package installation
* Add the GitLab package repository and install the package
```
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash

```

* If you find Curl error message with certificate, then you can just disable SSL verification in Gitlab repo
```
sed -i 's/^sslverify=.*/sslverify=0/g' /etc/yum.repos.d/gitlab_gitlab-ee.repo
```

* Install Gitlab EE package by using ``http`` and wait until finish. Note, we will configure ``https`` later with certificate
  * On ``primary``
```
echo "192.168.43.55   gitlab   gitlab.lab.local" >> /etc/hosts
EXTERNAL_URL="http://gitlab.lab.local" dnf install -y gitlab-ee
```

  * On ``secondary``. ``Note:`` Do not create an account or login to the second node yet.
```
echo "192.168.43.56   gitlab   gitlab.lab.local" >> /etc/hosts
EXTERNAL_URL="http://gitlab.lab.local" dnf install -y gitlab-ee
```
