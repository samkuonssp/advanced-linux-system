[[_TOC_]]
# Overview
# Prerequisites
## Install all requirement repositories
```
yum install -y \
    https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
    https://rpms.remirepo.net/enterprise/remi-release-7.rpm \
    wget
```
## Install PHP 7.4 (remi)
* Install PHP 7.4 including requirement extentions
```
yum install -y \
    php74 \
    php74-php-pdo \
    php74-php-json \
    php74-php-common \
    php74-php-gd \
    php74-php-pecl-mcrypt \
    php74-php-mysqlnd \
    php74-php-ldap
```
* Set PHP timezone
```
sed -i "s/^;date.timezone =$/date.timezone = \"Asia\/Phnom_Penh\"/" /etc/opt/remi/php74/php.ini
```

## Install MariaDB
* Install MariaDB server package
```
yum install -y mariadb-server && systemctl enable mariadb --now
```

* Proceed mysql secure installation
```
mysql_secure_installation
```

## Install Apache
* Install Apache package
```
yum install -y httpd && systemctl enable httpd --now
```

* Disable CentOS Welcome Page
```
sed -i 's/^\([^#]\)/#\1/g' /etc/httpd/conf.d/welcome.conf
```

* Turn off directory listing, Disable Apache's FollowSymLinks, Turn off server-side includes (SSI) and CGI execution
```
sed -i 's/^\([^#]*\)Options Indexes FollowSymLinks/\1Options -Indexes +SymLinksifOwnerMatch -ExecCGI -Includes/g' /etc/httpd/conf/httpd.conf
```

* Hide the Apache version, secure from clickjacking attacks, disable ETag, secure from XSS attacks and protect cookies with HTTPOnly flag
```
cat >> /etc/httpd/conf/httpd.conf <<'EOF'
ServerSignature Off
ServerTokens Prod
Header append X-FRAME-OPTIONS "SAMEORIGIN"
FileETag None
<IfModule mod_headers.c>
    Header set X-XSS-Protection "1; mode=block"
</IfModule>
EOF
```

* Disable unnecessary modules in /etc/httpd/conf.modules.d/00-base.conf
```
sed -i '/mod_cache.so/ s/^/#/' /etc/httpd/conf.modules.d/00-base.conf && \
sed -i '/mod_cache_disk.so/ s/^/#/' /etc/httpd/conf.modules.d/00-base.conf && \
sed -i '/mod_substitute.so/ s/^/#/' /etc/httpd/conf.modules.d/00-base.conf && \
sed -i '/mod_userdir.so/ s/^/#/' /etc/httpd/conf.modules.d/00-base.conf
```

* Disable everything in /etc/httpd/conf.modules.d/00-dav.conf, 00-lua.conf, 00-proxy.conf and 01-cgi.conf
```
sed -i 's/^/#/g' /etc/httpd/conf.modules.d/00-dav.conf && \
sed -i 's/^/#/g' /etc/httpd/conf.modules.d/00-lua.conf && \
sed -i 's/^/#/g' /etc/httpd/conf.modules.d/00-proxy.conf && \
sed -i 's/^/#/g' /etc/httpd/conf.modules.d/01-cgi.conf
```

* Restart apache service to apply the change
```
systemctl restart httpd
```

# Download EspoCRM latest version from here: https://www.espocrm.com/download/
```
CRM_VER=5.8.5
yum install -y wget unzip && wget https://www.espocrm.com/downloads/EspoCRM-$CRM_VER.zip
```

# Run EspoCRM installation process