[[_TOC_]]


# Virtual Server info
| Hostname | IP | OS | Install | Purpose |
| ------ | ------ | ----- | ----- | ----- |
| LB01 | 192.168.43.31 | RHEL 8.x | Nginx, KeepAlived | Nginx for Load Balancer/Reverse proxy and KeepAlived for floating IP |
| LB02 | 192.168.43.32 | RHEL 8.x | Nginx, KeepAlived | Nginx for Load Balancer/Reverse proxy and KeepAlived for floating IP |
| LB VIP | 192.168.43.30 |  |  | Floating IP for LB01 (Primary) and LB02 (Secondary) |

# Firewall configuration
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.31 destination address=224.0.0.18/32 protocol value=vrrp accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.32 destination address=224.0.0.18/32 protocol value=vrrp accept'
firewall-cmd --reload
```

# Installing Nginx on both nodes
* Create official Nginx Repository
```
cat > /etc/yum.repos.d/nginx.repo <<'EOF'
[nginx-stable]
name=nginx stable repo for rhel
baseurl=http://nginx.org/packages/rhel/$releasever/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true
EOF
```
* Install Nginx package
```
dnf install nginx
systemctl enable nginx --now
```

# Installing KeepAlived on both nodes
```
dnf install keepalived
```

# Confifugre KeepAlived VRRP
* Set keepalived variable for LB01
```
VIP=192.168.43.30
STATE=MASTER
PRIORITY=100
```

* Set keepalived variable for LB02
```
VIP=192.168.43.30
STATE=BACKUP
PRIORITY=99
```

* Set KeepaliveD configuration for each hosts
```
cp /etc/keepalived/keepalived.conf{,_`date +"%d%m%Y"`}
cat <<EOF | sudo tee /etc/keepalived/keepalived.conf
vrrp_script chk_nginx {
    script "pidof nginx"
    interval 2
}

vrrp_instance vip {
    interface eth0
    state ${STATE}
    virtual_router_id ${VIP##*.}
    priority ${PRIORITY}
    virtual_ipaddress {
        ${VIP}
    }
    track_script {
        chk_nginx
    }
}
EOF
```

* Start and enable KeepaliveD service
```
systemctl enable keepalived --now
```

# Configure Nginx Load Balancer
## On LB01
* Nginx configuration directories
  - ``/etc/nginx``: Nginx directory to store main nginx configuration
  - ``/etc/nginx/certs``: Certificate directory for Nginx
  - ``/etc/nginx/conf.d/``: Nginx directory to store all proxy sites
  - ``/etc/nginx/conf.d/global.d``: Nginx directory to store all secure settings configuration files
```
mkdir -p /etc/nginx/{certs,conf.d/global.d}
```

* In case, we use self-signed certificate and private key and follow:
  -  Create certificate
```
cd /etc/nginx/certs/
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-private.key -out nginx-certificate.pem
```

  - Generate strong DHE (Ephemeral Diffie-Hellman) parameters
```
openssl dhparam -out dhparam.pem 2048
```

  - Restrict SSL directory, private key and selfsigned certificate
```
chmod 400 /etc/nginx/certs/*.{key,pem}
chmod 600 /etc/nginx/certs
```
* Create redirection ``conf.d/redirect.conf``
```
cat > /etc/nginx/conf.d/redirect.conf <<'EOF'
server {
  listen        80;
  server_name   _;
  return 301 https://$host$request_uri;
}
EOF
```

* Secure nginx reverse proxy
  - Create ``global.d/common.conf``
```
cat > /etc/nginx/conf.d/global.d/common.conf <<'EOF'
# Remove version
server_tokens off;

# Remove PHP version
proxy_hide_header X-Powered-By;
proxy_hide_header X-CF-Powered-By;

# Limit client max body
client_max_body_size 100m;

# Clickjacking Attack
add_header X-Frame-Options "SAMEORIGIN";

# CSP and X-XSS-Protection
add_header Content-Security-Policy "default-src 'self' http: https: data: blob: 'unsafe-inline'" always;
add_header X-XSS-Protection "1; mode=block";
add_header X-Content-Type-Options nosniff;

# Strict-Transport-Security
add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload";
EOF
```

  - Create ``global.d/common_location.conf``
```
cat > /etc/nginx/conf.d/global.d/common_location.conf <<'EOF'
proxy_set_header    X-Real-IP           $remote_addr;
proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
proxy_set_header    X-Forwarded-Proto   $scheme;
proxy_set_header    Host                $host;
proxy_set_header    X-Forwarded-Host    $host;
proxy_set_header    X-Forwarded-Port    $server_port;
EOF
```

  - Create ``global.d/ssl.conf``
```
cat > /etc/nginx/conf.d/global.d/ssl.conf <<'EOF'
ssl_protocols               TLSv1.2;
ssl_ecdh_curve              secp384r1;
ssl_ciphers                 "ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384 OLD_TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 OLD_TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256";
ssl_prefer_server_ciphers   on;
ssl_dhparam                 /etc/nginx/certs/dhparam.pem;
ssl_certificate             /etc/nginx/certs/nginx-certificate.pem;
ssl_certificate_key         /etc/nginx/certs/nginx-private.key;
ssl_session_timeout         10m;
ssl_session_cache           shared:SSL:10m;
ssl_session_tickets         off;
ssl_stapling                on;
ssl_stapling_verify         on;
EOF
```

* Example site
```
cat > /etc/nginx/conf.d/test1.lab.local.conf <<'EOF'
upstream test1 {
  server        192.168.43.31:80;
  server        192.168.43.32:80;
}

server {
  listen        443 ssl;
  server_name   test1.lab.local;

  merge_slashes on;
  msie_padding on;

  include       /etc/nginx/conf.d/global.d/common.conf;
  include       /etc/nginx/conf.d/global.d/ssl.conf;

  location / {
    proxy_pass  http://test1;
    include     /etc/nginx/conf.d/global.d/common_location.conf;
  }
}
EOF
```