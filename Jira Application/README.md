# Firewall settings
firewall-cmd --permanent --add-port=8080/tcp
firewall-cmd --permanent --add-port=8090/tcp
firewall-cmd --reload

# Installing PostgreSQL
* Login RedHat server as *super user root*
* Make sure you can install some package from your local DVD if no subscription
* Start installing PostgreSQL with below command:
```
yum install -y curl && bash <(curl https://gitlab.com/samkuonssp/database-training/-/raw/master/PostgreSQL/pg12_rhel7_installer.sh)
```

* Create Database and user for Jira
```
source /etc/profile
sudo -u postgres psql -p $PGPORT <<'EOF'
CREATE DATABASE jiradb WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
CREATE USER jirauser WITH PASSWORD 'P@ssw0rd#1';
GRANT ALL PRIVILEGES ON DATABASE jiradb TO jirauser;
EOF
```

* Create Database and user for Confluence
```
source /etc/profile
sudo -u postgres psql -p $PGPORT <<'EOF'
CREATE DATABASE confluencedb WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
CREATE USER confluenceuser WITH PASSWORD 'P@ssw0rd#1';
GRANT ALL PRIVILEGES ON DATABASE confluencedb TO confluenceuser;
EOF
```

# Installing Java
* For RHEL 7.x, you can use command *yum search java* to get your *java openjdk version* in case it is *java-11-openjdk*
* Install *java-11-openjdk* and *java-11-openjdk-devel*
```
yum install -y java-11-openjdk java-11-openjdk-devel
```

# Installing Jira Software
* Download Jira Software here: [https://www.atlassian.com/software/jira/download]
* Copy Jira application "Jira Software" to your installation server
* Set the execution permission for Jira applications
```
chmod a+x atlassian-jira-software-X.X.X-x64.bin
# Where -X.X.X- is your Jira application version
```

## Run Jira Software installer
* Run the Jira Software installer
```
sudo ./atlassian-jira-software-X.X.X-x64.bin
```
* Type *o* and press *Enter* to continue installation
* Type *2* and press *Enter* to use *Custom Install*
* Just press *Enter* to keep using default location of Jira Core and Jira Core Data installation
* Type *1* and press *Enter* to keep using default Jira Core ports
* Type *y* and press *Enter* to instll Jira as service
* After review installation settings, type *i* and press *Enter* to start installation
* When installation is completed, type *y* to start Jira Core service

## Setup the Jira Software
* Once installation is complete head to *http://<Jira server hostname or IP address>:8080* in your browser to begin the setup process.
* Select *Setup myself* and click on *Next* 
* Select *Use my own database* and click on *Next*
* Choose database *PostgreSQL*, then file in the *Jira database, Jira User, Password*. After that click on *Test connection*
* If everything is ok, click on *Submit* and wait to next step
* Setup the application properties like *Application Title*, *Mode: Private* and your *Base URL*
* In *Specify your license key*, you will need to register account with Atlassian (Jira) to generate for trail license

# Installing Confluence
* Download Confluence software: [https://www.atlassian.com/software/confluence/download]
* Copy *Confluence* after download to installation server
* Set the execution permission for Confluence
```
chmod a+x atlassian-confluence-X.X.X-x64.bin
# Where -X.X.X- is your Jira application version
```

## Run Confluence installer
* Run the Confluence installer
```
sudo ./atlassian-confluence-X.X.X-x64.bin
```
* Type *o* and press *Enter* to continue installation
* Type *2* and press *Enter* to use *Custom Install*
* Just press *Enter* to keep using default location of Confluence installation
* Type *1* and press *Enter* to keep using default Confluence ports
* Type *y* and press *Enter* to instll Confluence as service
* After review installation settings, type *i* and press *Enter* to start installation
* When installation is completed, type *y* to start Confluence service


## Setup Confluence
* Once installation is complete head to *http://<Confluence server hostname or IP address>:8090* in your browser to begin the setup process.
* Select *Production Installation* and click on *Next* 
* Select *My own database* and click on *Next*
* Choose database *PostgreSQL*, then file in the *Confluence database, Confluence User, Password*.
* If everything is ok, click on *Submit* and wait to next step
* In *Specify your license key*, you will need to register account with Atlassian (Jira) to generate for trail license
