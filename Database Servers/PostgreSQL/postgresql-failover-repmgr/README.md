**PostgreSQL Failover Replication with REPMGR**

[[_TOC_]]

# Overview
## Components
``repmgr`` is a suite of open-source tools to manage replication and failover within a cluster of PostgreSQL servers. It supports and enhances PostgreSQL's built-in streaming replication, which provides a single read/write primary server and one or more read-only standbys containing near-real time copies of the primary server's database. It provides two main tools:

* ``repmgr``: A command-line tool used to perform administrative tasks such as:
    - setting up standby servers
    - promoting a standby server to primary
    - switching over primary and standby servers
    - displaying the status of servers in the replication cluster
* ``repmgrd``: A daemon which actively monitors servers in a replication cluster and performs the following tasks:
    - monitoring and recording replication performance
    - performing failover by detecting failure of the primary and promoting the most suitable standby server
    - provide notifications about events in the cluster to a user-defined script which can perform tasks such as sending alerts by email

*Credit*: ``https://repmgr.org``

## LAB info
| Hostname | IP | OS | Purpose | Installed | Additional Disk |
| ------ | ------ | ----- | ----- | ----- | ----- |
| pgdb01 | 192.168.100.31 | RHEL 8.x | Primary PostgreSQL Database | PostgreSQL13, repmgr | nvme0n2 20G: for PGDATA |
| pgdb02 | 192.168.100.32 | RHEL 8.x | Secondary PostgreSQL Database | PostgreSQL13, repmgr | nvme0n2 20GB: for PGDATA |

## Implement diagram

# Installation
## Prerequisites
* Check the ``repmgr`` compatibility maxtrix ``https://repmgr.org/docs/current/install-requirements.html#INSTALL-COMPATIBILITY-MATRIX``

* In case, we do not have subscription so follow this to configure local repository for Red Hat Enterprise Linux: ``https://www.sysnet-admin.com/2020/11/install-red-hat-packages-from-local-repository/``

* Configure host file for each nodes
```
cat <<'EOF' | sudo tee -a /etc/hosts
192.168.100.31      pgdb01
192.168.100.32      pgdb02
EOF
```

* Allow local firewall for PostgreSQL Failover service with port ``5432`` (Default port, but you can change it)
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.100.31 port port=5432 protocol=tcp accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.100.32 port port=5432 protocol=tcp accept'
firewall-cmd --reload
```

* Configure additional disk to store PostgreSQL data ``PGDATA`` on each nodes
```
lsblk
pvcreate /dev/nvme0n2
vgcreate vg_pgdata /dev/nvme0n2
lvcreate -l 100%VG vg_pgdata -n lv_pgdata
mkfs.xfs /dev/vg_pgdata/lv_pgdata
echo "$(blkid /dev/vg_pgdata/lv_pgdata | awk '{print $2}') /pgdata     xfs     defaults    0 0" >> /etc/fstab
mkdir /pgdata
mount -a
df -h
```

## Installing PostgreSQL (version 13)
* Install the repository RPM:
```
sudo dnf install -y \
    https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
```

* Disable the built-in PostgreSQL module:
```
sudo dnf -qy module disable postgresql
```

* Install PostgreSQL:
```
sudo dnf install -y postgresql13-server selinux-policy-minimum
```

* Set ``PGDATA=/pgdata`` and ``PGPORT=5432`` variable. You can change to yours
```
cat <<'EOF' | sudo tee -a ~/.bashrc
export PGDATA=/pgdata
export PGPORT=5432
EOF
source ~/.bashrc
echo $PGDATA $PGPORT
```

* Set ``PGDATA`` directory ``owner`` and ``SELinux``
```
sudo chown -R postgres:postgres $PGDATA
sudo chmod o-rwx $PGDATA
sudo semanage fcontext -a -t postgresql_db_t "$PGDATA(/.*)?"
sudo restorecon -Rv $PGDATA
```

* Update postgres daemon service to use the ``PGDATA`` directory
```
sudo sed -i "s@^Environment=PGDATA=.*@Environment=PGDATA=$PGDATA@" /usr/lib/systemd/system/postgresql*.service
sudo sed -i "s@^PGDATA=.*@PGDATA=$PGDATA@" /var/lib/pgsql/.bash_profile
sudo systemctl daemon-reload
```

* Optionally initialize the database and enable automatic start:
```
sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
```

* Enable, start and check status PostgreSQL service
```
sudo systemctl enable postgresql-13 --now
sudo systemctl status postgresql-13
```

* Reference: ``https://www.postgresql.org/download/linux/redhat/``

## Installing REPMGR (repmgr13 version 5.2)
```
sudo dnf install -y repmgr13
```

# Configuring PostgreSQL standard setting (follow CIS Benchmark) on Primary server
```
sudo echo "umask 077" >> /var/lib/pgsql/.bash_profile
sudo groupadd pg_wheel && gpasswd -a postgres pg_wheel

sudo mkdir -p /var/log/postgres && \
sudo chown postgres:postgres /var/log/postgres && \
sudo chmod 750 /var/log/postgres

sudo -u postgres psql <<'EOF'
-- Ensure the log destinations are set correctly --
alter system set log_destination = 'csvlog';
-- Ensure the logging collector is enabled --
alter system set logging_collector = 'on';
-- Ensure the log file destination directory is set correctly --
alter system set log_directory='/var/log/postgres';
-- Ensure the filename pattern for log files is set correctly --
alter system set log_filename='postgresql-%d%m%Y.log';
-- Ensure the log file permissions are set correctly --
alter system set log_file_mode = '0600';
-- Ensure 'log_truncate_on_rotation' is enabled --
alter system set log_truncate_on_rotation = 'on';
-- Ensure the maximum log file lifetime is set correctly --
alter system set log_rotation_age='1d';
-- Ensure the maximum log file size is set correctly --
alter system set log_rotation_size = '0';
-- Ensure the correct syslog facility is selected --
alter system set syslog_facility = 'LOCAL1';
-- Ensure the program name for PostgreSQL syslog messages is correct --
alter system set syslog_ident = 'postgres';
-- Ensure the correct messages are written to the server log --
alter system set log_min_messages = 'warning';
-- Ensure the correct SQL statements generating errors are recorded --
alter system set log_min_error_statement = 'error';
-- Ensure 'debug_print_parse' is disabled --
alter system set debug_print_parse='off';
-- Ensure 'debug_print_rewritten' is disabled --
alter system set debug_print_rewritten = 'off';
-- Ensure 'debug_print_plan' is disabled --
alter system set debug_print_plan = 'off';
-- Ensure 'debug_pretty_print' is enabled --
alter system set debug_pretty_print = 'on';
-- Ensure 'log_connections' is enabled --
alter system set log_connections = 'on';
-- Ensure 'log_disconnections' is enabled --
alter system set log_disconnections = 'on';
-- Ensure 'log_error_verbosity' is set correctly --
alter system set log_error_verbosity = 'verbose';
-- Ensure 'log_hostname' is set correctly --
alter system set log_hostname='off';
-- Ensure 'log_line_prefix' is set correctly --
alter system set log_line_prefix = '%t [%p]: [%l-1] db=%d,user=%u,app=%a,client=%h ';
-- Ensure 'log_statement' is set correctly --
alter system set log_statement='ddl';
-- Ensure 'log_timezone' is set correctly --
alter system set log_timezone = 'UTC';
EOF

sudo echo '%pg_wheel ALL= /bin/su - postgres' > /etc/sudoers.d/postgres
sudo chmod 600 /etc/sudoers.d/postgres

sudo -u postgres psql <<'EOF'
-- Set listen address to any address --
alter system set listen_addresses = '*';
-- Set password encryption with md5 --
alter system set password_encryption = 'md5';
EOF

sudo -u postgres psql -c "alter system set port = '$PGPORT';"
sudo semanage port -a -t postgresql_port_t -p tcp $PGPORT

sudo cp -p $PGDATA/pg_hba.conf{,_`date +"%d%m%Y"`}
cat <<'EOF' | sudo tee $PGDATA/pg_hba.conf
# TYPE         DATABASE        USER        ADDRESS         METHOD
# Only local be able to access Postgres with "peer"
local    all    all        peer

# Also allow the host unrestricted access to connect to itself
host    all     all    127.0.0.1/32    trust
host    all     all    ::1/128         trust
EOF

sudo systemctl restart postgresql-13
sudo -u postgres psql -p $PGPORT -p $PGPORT -c "alter role postgres with password 'password#1';"
```

# Configuring PostgreSQL replication on primary server
```
cat <<'EOF' | sudo tee $PGDATA/pg_replication.conf
## Replication Configuration
max_wal_senders = 10
max_replication_slots = 10
wal_level = 'replica'
hot_standby = on
archive_mode = on
archive_command = '/bin/true'
EOF
```

# Create the repmgr user and database on primary server
```
sudo -u postgres createuser -s repmgr
sudo -u postgres createdb repmgr -O repmgr
```

# Configuring authentication in pg_hba.conf on primary server
* Update replication connection permission on primary server
```
cat <<'EOF' | sudo tee -a $PGDATA/pg_hba.conf
## replication connection permission
local   replication   repmgr                              trust
host    replication   repmgr      127.0.0.1/32            trust
host    replication   repmgr      192.168.100.31/24       trust
host    replication   repmgr      192.168.100.32/24       trust

local   repmgr        repmgr                              trust
host    repmgr        repmgr      127.0.0.1/32            trust
host    repmgr        repmgr      192.168.100.31/24       trust
host    repmgr        repmgr      192.168.100.32/24       trust
EOF
sudo -u postgres psql -c "select pg_reload_conf();"
```

* On secondary server, Check the primary database is reachable from the standby using psql:
```
psql 'host=pgdb01 user=repmgr dbname=repmgr connect_timeout=2'
```

# repmgr configuration file
* Create a repmgr.conf file on the primary server. The file must contain at least the following parameters:
```
cat <<EOF | sudo tee /etc/repmgr/repmgr.conf
node_id=1
node_name='$(hostname)'
conninfo='host=$(hostname) user=repmgr dbname=repmgr connect_timeout=2'
data_directory='$PGDATA'
failover=automatic
promote_command='/usr/pgsql-13/bin/repmgr standby promote -f /etc/repmgr/repmgr.conf --log-to-file'
follow_command='/usr/pgsql-13/bin/repmgr standby follow -f /etc/repmgr/repmgr.conf --log-to-file --upstream-node-id=%n'
EOF
```

# Register the primary server
* To enable repmgr to support a replication cluster, the primary node must be registered with repmgr
```
ln -s /usr/pgsql-13/bin/repmgr /usr/local/bin/repmgr
su - postgres
repmgr -f /etc/repmgr/repmgr.conf primary register
repmgr -f /etc/repmgr/repmgr.conf cluster show
```

# Clone the standby server
* Create a repmgr.conf file on the standby server. It must contain at least the same parameters as the primary's repmgr.conf, but with the mandatory values node, node_name, conninfo (and possibly data_directory) adjusted accordingly
```
cat <<EOF | sudo tee /etc/repmgr/repmgr.conf
node_id=2
node_name='$(hostname)'
conninfo='host=$(hostname) user=repmgr dbname=repmgr connect_timeout=2'
data_directory='$PGDATA'
failover=automatic
promote_command='/usr/pgsql-13/bin/repmgr standby promote -f /etc/repmgr/repmgr.conf --log-to-file'
follow_command='/usr/pgsql-13/bin/repmgr standby follow -f /etc/repmgr/repmgr.conf --log-to-file --upstream-node-id=%n'
EOF
```
* Stop PostgreSQL service on secondary server (standby)
```
sudo system stop postgresql-13
```

* Use the --dry-run option to check the standby can be cloned
```
sudo ln -s /usr/pgsql-13/bin/repmgr /usr/local/bin/repmgr
sudo ln -s /usr/pgsql-13/bin/repmgrd /usr/local/bin/repmgrd

su - postgres
repmgr -h pgdb01 -U repmgr -d repmgr -f /etc/repmgr/repmgr.conf standby clone --dry-run
```

* If no problems are reported, the standby can then be cloned with:
```
repmgr -h pgdb01 -U repmgr -d repmgr -f /etc/repmgr/repmgr.conf standby clone
```

* Configure standard settings on standby server
```
sudo echo "umask 077" >> /var/lib/pgsql/.bash_profile
sudo groupadd pg_wheel && gpasswd -a postgres pg_wheel

sudo mkdir -p /var/log/postgres && \
sudo chown postgres:postgres /var/log/postgres && \
sudo chmod 750 /var/log/postgres

sudo echo '%pg_wheel ALL= /bin/su - postgres' > /etc/sudoers.d/postgres
sudo chmod 600 /etc/sudoers.d/postgres
```

* Start PostgreSQL service
```
sudo systemctl start postgresql-13
```

# Register the standby
* Register the standby server with:
```
repmgr -f /etc/repmgr/repmgr.conf standby register
repmgr -f /etc/repmgr/repmgr.conf cluster show
```

# Start repmgrd daemon process
* To enable the automatic failover, we now need to start the repmgrd daemon process on master slave and witness:
```
su - postgres
psql -c "alter system set shared_preload_libraries = 'repmgr';"
repmgrd -v -f /etc/repmgr/repmgr.conf
```