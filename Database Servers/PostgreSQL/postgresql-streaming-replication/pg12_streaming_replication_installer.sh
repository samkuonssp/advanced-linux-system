#!/bin/bash
# How to use this script: yum install -y curl && bash <(curl https://gitlab.com/samkuonssp/advanced-linux-system/-/raw/master/Database%20Servers/PostgreSQL/postgresql-streaming-replication/pg12_streaming_replication_installer.sh)

pg12_setup_env() {
echo -e "\nPlease provide your setup info...."
read -p "PostgreSQL Master Node's hostname, i.e dbpg01: " PG_MASTER_NODE_HOST
read -p "PostgreSQL Master Node's IP, i.e 192.168.43.21: " PG_MASTER_NODE_IP
read -p "PostgreSQL Slave Node's hostname, i.e dbpg02: " PG_SLAVE_NODE_HOST
read -p "PostgreSQL Slave Node's IP, i.e 192.168.43.22: " PG_SLAVE_NODE_IP
read -p "PostgreSQL Data path, i.e /pgdata: " PG_DATA
read -p "PostgreSQL Archive path, i.e /pgarchive: " PG_ARCHIVE
read -p "PostgreSQL Port,  i.e 5432: " PG_PORT
read -p "PostgreSQL Admin's password: " PG_PASSWORD
read -p "Replication User's password: " PG_REPLICATION_PASSWORD
if [[ ( -z "$PG_MASTER_NODE_HOST") || ( -z "$PG_MASTER_NODE_IP" ) || ( -z "$PG_SLAVE_NODE_HOST" ) || ( -z "$PG_SLAVE_NODE_IP" ) || ( -z "$PG_DATA" ) || ( -z "$PG_ARCHIVE" ) || ( -z "$PG_PORT" ) || ( -z "$PG_PASSWORD" ) || ( -z "$PG_REPLICATION_PASSWORD" ) ]]; then
echo -e "These are mandatories info requirement, cannot be blank. Please try again, thank!"
exit
else
cat >> /etc/hosts <<EOF
# PG Node Info
$PG_MASTER_NODE_IP    $PG_MASTER_NODE_HOST
$PG_SLAVE_NODE_IP     $PG_SLAVE_NODE_HOST
EOF
cat >> ~/.bashrc <<EOF
# PostgreSQL Directories Path
export PG_MASTER_NODE_HOST=$PG_MASTER_NODE_HOST
export PG_MASTER_NODE_IP=$PG_MASTER_NODE_IP
export PG_SLAVE_NODE_HOST=$PG_SLAVE_NODE_HOST
export PG_SLAVE_NODE_IP=$PG_SLAVE_NODE_IP
export PG_DATA=$PG_DATA
export PG_ARCHIVE=$PG_ARCHIVE
export PG_PORT=$PG_PORT
EOF
source ~/.bashrc
fi
}

pg12_installer() {
echo -e "\nInstall PostgreSQL yum repository (latest version)..............."
rpm -Uvh https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

echo -e "Install the PostgreSQL client/server packages...................."
yum install -y postgresql12 postgresql12-server postgresql12-contrib pgaudit14_12 policycoreutils-python

echo -e "\nChange PGDATA path in PostgreSQL daemon service..............."
sed -i "s@^Environment=PGDATA=.*@Environment=PGDATA=$PG_DATA@" /usr/lib/systemd/system/postgresql*.service
sed -i "s@^PGDATA=.*@PGDATA=$PG_DATA@" /var/lib/pgsql/.bash_profile
systemctl daemon-reload

echo -e "\nCreate PostgreSQL directories and permission....................."
mkdir -p $PG_DATA $PG_ARCHIVE
chown -R postgres:postgres {$PG_DATA,$PG_ARCHIVE}
chmod -R 700 {$PG_DATA,$PG_ARCHIVE}
chmod g+s {$PG_DATA,$PG_ARCHIVE}

echo -e "\nApply SELinux for PostgreSQL directories........................."
semanage fcontext -a -t postgresql_db_t "$PG_DATA(/.*)?"
semanage fcontext -a -t postgresql_db_t "$PG_ARCHIVE(/.*)?"
restorecon -Rv {$PG_DATA,$PG_ARCHIVE}

echo -e "\nFirst initialize the PostgreSQL database server after install....."
PGSETUP_INITDB_OPTIONS="-k" /usr/pgsql-12/bin/postgresql-12-setup initdb

echo -e "\nEnable auto startup and start PostgreSQL service.................."
systemctl enable postgresql-12 && systemctl start postgresql-12
}

pg12_configuration() {
echo -e "\nStart configuration................................................"
echo -e "\nBackup PostgreSQL main configuration..............................."
cp $PG_DATA/postgresql.conf $PG_DATA/postgresql.conf_`date +"%d%m%Y"`
cp $PG_DATA/pg_hba.conf $PG_DATA/pg_hba.conf_`date +"%d%m%Y"`
chown postgres:postgres $PG_DATA/{postgresql,pg_hba}.conf_`date +"%d%m%Y"`

echo -e "\nPostgreSQL group & permission......................................."
echo "umask 077" >> /var/lib/pgsql/.bash_profile
groupadd pg_wheel && gpasswd -a postgres pg_wheel

echo -e "\nLogging Monitoring And Auditing......................................."
mkdir -p /var/log/postgres && \
chown postgres:postgres /var/log/postgres && \
chmod 750 /var/log/postgres

echo -e "\nStandard PostgreSQL hardening settings................................"
cat > $PG_DATA/postgresql.conf <<EOF
## Database Settings
listen_addresses = 'localhost,$PG_MASTER_NODE_IP'
port = '$PG_PORT'
timezone = 'Asia/Phnom_Penh'
password_encryption = 'scram-sha-256'
shared_preload_libraries = 'pg_stat_statements, pgaudit'
pg_stat_statements.max = '5000'
pg_stat_statements.track = all
pgaudit.log = 'ddl,write'

## Archive Settings
archive_mode = on
archive_command = 'test ! -f $PG_ARCHIVE/%f && cp %p $PG_ARCHIVE/%f'
archive_cleanup_command = 'pg_archivecleanup $PG_ARCHIVE/ %r'

## Logging and Debuging
log_destination = 'csvlog'
logging_collector = 'on'
log_directory = '/var/log/postgres'
log_filename = 'postgresql-%d%m%Y.log'
log_file_mode = '0600'
log_truncate_on_rotation = 'on'
log_rotation_age = '1d'
log_rotation_size = '0'
syslog_facility = 'LOCAL1'
syslog_ident = 'postgres'
log_min_messages = 'warning'
log_min_error_statement = 'error'
debug_print_parse = 'off'
debug_print_rewritten = 'off'
debug_print_plan = 'off'
debug_pretty_print = 'on'
log_connections = 'on'
log_disconnections = 'on'
log_error_verbosity = 'verbose'
log_hostname = 'off'
log_line_prefix = '%t [%p]: [%l-1] db=%d,user=%u,app=%a,client=%h '
log_statement = 'ddl'
log_timezone = 'Asia/Phnom_Penh'

EOF

echo -e "\nUser Access and Authorization......................................."
echo '%pg_wheel ALL= /bin/su - postgres' > /etc/sudoers.d/postgres
chmod 600 /etc/sudoers.d/postgres
sudo -u postgres psql -c 'select pg_reload_conf();'

echo -e "\nIf you use new port, please make sure to allow it from SELinux........"
semanage port -a -t postgresql_port_t -p tcp $PG_PORT

echo -e "\nPostgreSQL access restriction........................................."
cat > $PG_DATA/pg_hba.conf <<EOF
# TYPE         DATABASE        USER        ADDRESS         METHOD
# Only local be able to access Postgres with "peer"
local    all    all        peer

# Also allow the host unrestricted access to connect to itself
host    all     all    127.0.0.1/32    trust
host    all     all    ::1/128         trust
EOF

echo -e "\nRestart PostgreSQL service to apply the changes........................"
systemctl restart postgresql-12

echo -e "\nSet the Postgres's password"
sudo -u postgres psql -p $PG_PORT -c "alter role postgres with password '$PG_PASSWORD';"
}

pg12_master_replication() {
echo -e "\nPostgreSQL replication configurtion......................................."
sudo -u postgres psql -p $PG_PORT -c "create user repuser replication login connection limit 5 encrypted password '$PG_REPLICATION_PASSWORD';"

cat >> $PG_DATA/postgresql.conf <<EOF
## Replication Settings
hot_standby = on
wal_level = replica
max_wal_senders = 10
max_replication_slots = 5
wal_keep_segments = 10
primary_conninfo = 'user=repuser host=$PG_SLAVE_NODE_IP port=$PG_PORT application_name=$PG_MASTER_NODE_HOST'
recovery_target_timeline = 'latest'
restore_command = 'cp $PG_ARCHIVE/%f %p'
promote_trigger_file = '/tmp/postgresql.trigger.$PG_PORT'
synchronous_commit = on
synchronous_standby_names = '$PG_SLAVE_NODE_HOST'
EOF

sudo -u postgres psql -p $PG_PORT -d postgres -U postgres <<'EOF'
select * from pg_replication_slots;
select * from pg_create_physical_replication_slot('st_slot_1');
select * from pg_replication_slots;
alter system set primary_slot_name = 'st_slot_1';
EOF

echo -e "\nPostgreSQL restriction for replication...................................."
cat >> $PG_DATA/pg_hba.conf <<EOF
host    replication    repuser   $PG_MASTER_NODE_IP/32   trust
host    replication    repuser   $PG_SLAVE_NODE_IP/32    trust
EOF

echo -e "\Restart PostgreSQL service to apply the changes"
systemctl restart postgresql-12
}

pg12_slave_replication() {
echo -e "\nPostgreSQL group & permission......................................."
echo "umask 077" >> /var/lib/pgsql/.bash_profile
groupadd pg_wheel && gpasswd -a postgres pg_wheel

echo -e "\nUser Access and Authorization......................................."
echo '%pg_wheel ALL= /bin/su - postgres' > /etc/sudoers.d/postgres
chmod 600 /etc/sudoers.d/postgres
sudo -u postgres psql -c 'select pg_reload_conf();'

echo -e "\nLogging Monitoring And Auditing......................................."
mkdir -p /var/log/postgres && \
chown postgres:postgres /var/log/postgres && \
chmod 750 /var/log/postgres

echo -e "\nIf you use new port, please make sure to allow it from SELinux........"
semanage port -a -t postgresql_port_t -p tcp $PG_PORT

echo -e "\nMake PostgreSQL service is not running and PGDATA is clean.................."
systemctl stop postgresql-12
rm -rf $PG_DATA/*

echo -e "\nBackup PGDATA from PostgreSQL Master node...................................."
sudo -u postgres  pg_basebackup -h $PG_MASTER_NODE_IP -p $PG_PORT -D $PG_DATA -U repuser -v -P

echo -e "\nChange configuration after backup from Master................................"
sed -i "s/^listen_addresses = *.*/listen_addresses = 'localhost,$PG_SLAVE_NODE_IP'/" $PG_DATA/postgresql.conf
sed -i "s/host=$PG_SLAVE_NODE_IP/host=$PG_MASTER_NODE_IP/" $PG_DATA/postgresql.conf
sed -i "s/application_name=$PG_MASTER_NODE_HOST/application_name=$PG_SLAVE_NODE_HOST/" $PG_DATA/postgresql.conf
sed -i "s/synchronous_commit = on/synchronous_commit = local/" $PG_DATA/postgresql.conf
sed -i "s/synchronous_standby_names = '$PG_SLAVE_NODE_HOST'/synchronous_standby_names = '$PG_MASTER_NODE_HOST'/" $PG_DATA/postgresql.conf
sed -i "s/^synchronous_standby_names/#&/" $PG_DATA/postgresql.conf

echo -e "\nYou must create **standby.signal** in $PG_DATA to make streaming replication working..."
touch $PG_DATA/standby.signal
chown postgres:postgres $PG_DATA/standby.signal

echo -e "\nStart and enable PostgreSQL service ........................................."
systemctl enable postgresql-12 && systemctl start postgresql-12
}

pg12_firewall() {
echo -e "\nLocal Firewall Settings..................................................."
firewall-cmd --permanent --new-ipset=NET_PG --type=hash:net
firewall-cmd --permanent --ipset=NET_PG --add-entry=$PG_MASTER_NODE_IP
firewall-cmd --permanent --ipset=NET_PG --add-entry=$PG_SLAVE_NODE_IP
firewall-cmd --permanent --service=postgresql --remove-port=5432/tcp
firewall-cmd --permanent --service=postgresql --add-port=$PG_PORT/tcp
firewall-cmd --permanent --add-rich-rule='rule family="ipv4" source ipset="NET_PG" service name="postgresql" accept'
firewall-cmd --reload
}

pg12_master() {
    pg12_setup_env && \
    pg12_installer && \
    pg12_firewall && \
    pg12_configuration && \
    pg12_master_replication
    echo -e "\nTunning your PostgreSQL with "https://pgtune.leopard.in.ua/#/""
}

pg12_slave() {
    pg12_setup_env && \
    pg12_installer && \
    pg12_firewall && \
    pg12_slave_replication
    echo -e "\nTunning your PostgreSQL with "https://pgtune.leopard.in.ua/#/""
}

## Deployment stages
COLUMNS=12
options=(
"Install PostgreSQL Master Node"
"Install PostgreSQL Slave Nodes"
"Select any to quite...!"
)
echo -e "\nThis installer tested on RHEL 7.7, but CEN 7.x should be working too...."
echo -e "\nPlease select your installation plan:"
select opt in "${options[@]}"; do
    case $REPLY in
        1) pg12_master; break;;
        2) pg12_slave; break;;
        3|*) break ;;
    esac
done

