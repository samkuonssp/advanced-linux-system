## PostgreSQL Clustering with Pgpool-II & Watchdog ##
Reference: https://www.pgpool.net/docs/latest/en/html/example-cluster.html

I. Setup Requirements
We assume that all the Pgpool-II servers and the PostgreSQL servers are in the same subnet.
We will use 3 servers with RHEL 8.x installed. We will install PostgreSQL 14 and Pgpool-II (4.3.0) on each server
1.1. Cluster System configuration
https://www.pgpool.net/docs/latest/en/html/cluster_40.gif

Note: The roles of Active, Standby, Primary, Standby are not fixed and may be changed by further operations.

1.2. Hostname and IP address
----------------------------------------------
| Hostname  | IP address     | VIP           |
----------------------------------------------
| server-01 | 192.168.100.21 |               |
| server-02 | 192.168.100.22 | 192.168.100.20|
| server-03 | 192.168.100.23 |               |
----------------------------------------------

1.3. PostgreSQL version and Configuration
-----------------------------------------------
| Item                | Value    | Detail     |
-----------------------------------------------
| PostgreSQL version  | 14.0     | -          |
| port                | 5432     | -          |
| $PGDATA             | /pgdata  | -          |
| Archive mode        | on       | /pgarchive |
| Replication Slots   | Enable   | -          |
| Start automatically | Enable   | -          |
-----------------------------------------------

1.4. Pgpool-II Version and Configuration
----------------------------------------------------------------
| Item                | Value                         | Detail
----------------------------------------------------------------
| Pgpool-II version   | 4.3.0                         | -
| Port                | 9999                          | Pgpool-II accepts connections
| Port                | 9898                          | PCP process accepts connections
| Port                | 9000                          | Watchdog accepts connections
| Port                | 9694                          | UDP port for receiving Watchdog's heartbeat signal
| Config file         | /etc/pgpool-II/pgpool.conf    | Pgpool-II config file
| Pgpool-II start user| postgres (Pgpool-II 4.1 or later)| Pgpool-II 4.0 or before, the default startup user is root
| Running mode        | streaming replication mode    | -
| Watchdog            | on                            | Life check method: heartbeat
| Start automatically | Enable                        | -
----------------------------------------------------------------

1.5. Various sample scripts included in rpm package
---------------------------------------------------------------------------------------
| Feature             | Script                                    | Detail
---------------------------------------------------------------------------------------
| Failover            | /etc/pgpool-II/failover.sh.sample         | Run by failover_command to perform failover
                      | /etc/pgpool-II/follow_primary.sh.sample   | Run by follow_primary_command to synchronize the Standby with the new Primary after failover.
| Online recovery     | /etc/pgpool-II/recovery_1st_stage.sample  | Run by recovery_1st_stage_command to recovery a Standby node
                      | /etc/pgpool-II/pgpool_remote_start.sample | Run after recovery_1st_stage_command to start the Standby node
| Watchdog            | /etc/pgpool-II/escalation.sh.sample       | Run by wd_escalation_command to switch the Active/Standby Pgpool-II safely
----------------------------------------------------------------------------------------
Note: The above scripts are included in the RPM package and can be customized as needed.

1.6. Users
-----------------------------------
| User Name   | Password  | Detail
-----------------------------------
| repl        | repl      | PostgreSQL replication user
| pgpool      | pgpool    | Pgpool-II health check (health_check_user) and replication delay check (sr_check_user) user
| postgres    | postgres  | User running online recovery
-----------------------------------

II. Packages Installation (for all servers)
2.1. PostgreSQL 14
- Install the PostgreSQL repository RPM. You can check the latest here: https://www.postgresql.org/download/linux/redhat/
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm

- Disable the built-in PostgreSQL module
sudo dnf -qy module disable postgresql

- Install PostgreSQL 14, pgaudit and SELinux tools
sudo dnf install -y \
    postgresql14-server \
	postgresql14-contrib \
    pgaudit16_14 \
    policycoreutils-python-utils

2.2. Pgpool-II
- Since Pgpool-II related packages are also included in PostgreSQL YUM repository, add the "exclude" settings to /etc/yum.repos.d/pgdg-redhat-all.repo so that Pgpool-II is not installed from PostgreSQL YUM repository.
sed -i '/^repo_gpgcheck/a exclude=pgpool*' /etc/yum.repos.d/pgdg-redhat-all.repo

- Install Pgpool-II repository RPM
sudo dnf install -y https://www.pgpool.net/yum/rpms/4.3/redhat/rhel-8-x86_64/pgpool-II-release-4.3-1.noarch.rpm

- Install Pgpool-II from Pgpool-II YUM repository
sudo dnf install -y pgpool-II-pg14-*

III. Cluster Configuration
3.1. Prerequisites
3.1.1. Set hosts file records
cat <<'EOF' | sudo tee -a /etc/hosts
192.168.100.21 server-01
192.168.100.22 server-02
192.168.100.23 server-03
EOF

3.1.2. Local firewall settings
- Create custom firewall service for cluster named "pgcluster"
sudo firewall-cmd --permanent --new-service=pgcluster
for srv in 22/tcp 5432/tcp 9999/tcp 9898/tcp 9000/tcp 9694/udp;
do
    sudo firewall-cmd --permanent --service=pgcluster --add-port=${srv};
done
firewall-cmd --reload

- Allow cluster servers to custom created firewall service named "pgcluster"
sudo firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.100.21/32 service name=pgcluster accept'
sudo firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.100.22/32 service name=pgcluster accept'
sudo firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.100.23/32 service name=pgcluster accept'
firewall-cmd --reload

3.2. PostgreSQL Configuration
3.2.1. Set PostgreSQL variables
cat <<'EOF' | sudo tee /etc/profile.d/pgvar.sh
export PGDATA=/pgdata
export PGARCHIVE=/pgarchive
EOF
source /etc/profile.d/pgvar.sh

3.2.2. Prepare "/pgdata" and "/pgarchive" directories to store DB cluster data
sudo mkdir $PGDATA $PGARCHIVE
sudo chown -R postgres:postgres $PGDATA $PGARCHIVE
sudo chmod o-rwx $PGDATA $PGARCHIVE
sudo semanage fcontext -a -t postgresql_db_t "$PGDATA(/.*)?"
sudo semanage fcontext -a -t postgresql_db_t "$PGARCHIVE(/.*)?"
sudo restorecon -Rv $PGDATA
sudo restorecon -Rv $PGARCHIVE

3.2.3. Update PostgreSQL daemon service to use the "/pgdata" directory
sudo sed -i "s@^Environment=PGDATA=.*@Environment=PGDATA=$PGDATA@" /usr/lib/systemd/system/postgresql*.service
sudo sed -i "s@^PGDATA=.*@PGDATA=$PGDATA@" /var/lib/pgsql/.bash_profile
sudo systemctl daemon-reload

3.2.4. Initialize the first database
sudo postgresql-14-setup initdb

3.2.5. Start and enable service PostgreSQL
sudo systemctl enable --now postgresql-14

3.2.6. Apply PostgreSQL database standard settings
echo "umask 077" >> /var/lib/pgsql/.bash_profile
groupadd pg_wheel && gpasswd -a postgres pg_wheel

mkdir -p /var/log/postgres && \
chown postgres:postgres /var/log/postgres && \
chmod 750 /var/log/postgres

sudo -u postgres psql <<'EOF'
-- Ensure the log destinations are set correctly --
alter system set log_destination = 'csvlog';
-- Ensure the logging collector is enabled --
alter system set logging_collector = 'on';
-- Ensure the log file destination directory is set correctly --
alter system set log_directory='/var/log/postgres';
-- Ensure the filename pattern for log files is set correctly --
alter system set log_filename='postgresql-%d%m%Y.log';
-- Ensure the log file permissions are set correctly --
alter system set log_file_mode = '0600';
-- Ensure 'log_truncate_on_rotation' is enabled --
alter system set log_truncate_on_rotation = 'on';
-- Ensure the maximum log file lifetime is set correctly --
alter system set log_rotation_age='1d';
-- Ensure the maximum log file size is set correctly --
alter system set log_rotation_size = '0';
-- Ensure the correct syslog facility is selected --
alter system set syslog_facility = 'LOCAL1';
-- Ensure the program name for PostgreSQL syslog messages is correct --
alter system set syslog_ident = 'postgres';
-- Ensure the correct messages are written to the server log --
alter system set log_min_messages = 'warning';
-- Ensure the correct SQL statements generating errors are recorded --
alter system set log_min_error_statement = 'error';
-- Ensure 'debug_print_parse' is disabled --
alter system set debug_print_parse='off';
-- Ensure 'debug_print_rewritten' is disabled --
alter system set debug_print_rewritten = 'off';
-- Ensure 'debug_print_plan' is disabled --
alter system set debug_print_plan = 'off';
-- Ensure 'debug_pretty_print' is enabled --
alter system set debug_pretty_print = 'on';
-- Ensure 'log_connections' is enabled --
alter system set log_connections = 'on';
-- Ensure 'log_disconnections' is enabled --
alter system set log_disconnections = 'on';
-- Ensure 'log_error_verbosity' is set correctly --
alter system set log_error_verbosity = 'verbose';
-- Ensure 'log_hostname' is set correctly --
alter system set log_hostname='off';
-- Ensure 'log_line_prefix' is set correctly --
alter system set log_line_prefix = '%t [%p]: [%l-1] db=%d,user=%u,app=%a,client=%h ';
-- Ensure 'log_statement' is set correctly --
alter system set log_statement='ddl';
-- Ensure 'log_timezone' is set correctly --
alter system set log_timezone = 'UTC';
-- Ensure the PostgreSQL Audit Extension (pgAudit) is enabled --
alter system set shared_preload_libraries = 'pgaudit';
EOF

echo "pgaudit.log='ddl,write'" | sudo tee -a $PGDATA/postgresql.auto.conf
echo '%pg_wheel ALL= /bin/su - postgres' | sudo tee /etc/sudoers.d/postgres
chmod 600 /etc/sudoers.d/postgres

sudo -u postgres psql <<'EOF'
-- Set listen address to any address --
alter system set listen_addresses = '*';
-- Set password encryption with scram-sha-256 --
alter system set password_encryption = 'scram-sha-256';
EOF

cp -p $PGDATA/pg_hba.conf{,_`date +"%d%m%Y"`}
cat <<'EOF' | sudo tee $PGDATA/pg_hba.conf
# TYPE      DATABASE        USER        ADDRESS         METHOD
# Only local be able to access Postgres with "peer"
local       all             all                         peer

# Also allow the host unrestricted access to connect to itself
host        all             all         127.0.0.1/32    trust
host        all             all         ::1/128         trust
EOF

- Restart PostgreSQL service to apply the change
sudo systemctl restart postgresql-14

- Do not forget to set/change your postgres role password
sudo -u postgres psql -c "alter role postgres with password 'changeme';"

3.2.7. Tuning your PostgreSQL database
Follow this link: https://pgtune.leopard.in.ua/#/

3.2.8. [On Server-01] Set up PostgreSQL streaming replication on the primary server
- In this example, we use WAL archiving
sudo -u postgres psql <<EOF
alter system set archive_mode = on;
alter system set archive_command = 'cp "%p" "$PGARCHIVE/%f"';
alter system set max_wal_senders = 10;
alter system set max_replication_slots = 10;
alter system set wal_level = replica;
alter system set hot_standby = on;
alter system set wal_log_hints = on;
EOF
sudo systemctl restart postgresql-14

- Because of the security reasons, we create a user repl solely used for replication purpose, and a user pgpool for streaming replication delay check and health check of Pgpool-II. Server 1 only
sudo -u postgres psql <<EOF
create role repl with replication login password 'repl';
create role pgpool with login password 'pgpool';
GRANT pg_monitor TO pgpool;
select pg_reload_conf();
EOF

- Assuming that all the Pgpool-II servers and the PostgreSQL servers are in the same subnet and edit pg_hba.conf to enable scram-sha-256 authentication method.
cat <<'EOF' | sudo tee -a $PGDATA/pg_hba.conf

# Allow connection for replication
host        all             all         192.168.100.20/32         scram-sha-256
host        all             all         192.168.100.21/32         scram-sha-256
host        all             all         192.168.100.22/32         scram-sha-256
host        all             all         192.168.100.23/32         scram-sha-256

host        replication     all         192.168.100.20/32         scram-sha-256
host        replication     all         192.168.100.21/32         scram-sha-256
host        replication     all         192.168.100.22/32         scram-sha-256
host        replication     all         192.168.100.23/32         scram-sha-256
EOF
sudo -u postgres psql -c 'select pg_reload_conf();'

3.3. Pgpool-II Configuration
3.3.1. Prerequisites
- [On Server-01] To use the automated failover and online recovery of Pgpool-II, the settings that allow passwordless SSH to all backend servers between Pgpool-II execution user (default root user) and postgres user and between postgres user and postgres user are necessary. Execute the following command on all servers to set up passwordless SSH. The generated key file name is id_rsa_pgpool.
sed -i 's/^PermitEmptyPasswords/#&/' /etc/ssh/sshd_config 
echo "PermitEmptyPasswords yes" | sudo tee -a /etc/ssh/sshd_config
sudo systemctl restart sshd

passwd postgres
--> enter new password for postgres
--> confirm your password again

su - postgres
mkdir ~/.ssh && cd ~/.ssh
ssh-keygen -b 4096 -t rsa -C Pgpool-II_PassLess -f id_rsa_pgpool
ssh-copy-id -i id_rsa_pgpool.pub postgres@server-01
ssh-copy-id -i id_rsa_pgpool.pub postgres@server-02
ssh-copy-id -i id_rsa_pgpool.pub postgres@server-03
scp -i id_rsa_pgpool -p id_rsa_pgpool postgres@server-02:~/.ssh/
scp -i id_rsa_pgpool -p id_rsa_pgpool postgres@server-03:~/.ssh/

- [On Server-01] replicate pg_hba.conf to /etc/pgpool-II/pool_hba.conf
su - postgres
scp -i ~/.ssh/id_rsa_pgpool -p $PGDATA/pg_hba.conf postgres@server-01:/etc/pgpool-II/pool_hba.conf
scp -i ~/.ssh/id_rsa_pgpool -p $PGDATA/pg_hba.conf postgres@server-02:/etc/pgpool-II/pool_hba.conf
scp -i ~/.ssh/id_rsa_pgpool -p $PGDATA/pg_hba.conf postgres@server-03:/etc/pgpool-II/pool_hba.conf

- [all servers] The default password file name for authentication is pool_passwd. To use scram-sha-256 authentication, the decryption key to decrypt the passwords is required. We create the .pgpoolkey file in Pgpool-II start user postgres's (Pgpool-II 4.1 or later) home directory. (Pgpool-II 4.0 or before, by default Pgpool-II is started as root)
su - postgres
echo 'pgpoolkey123' > ~/.pgpoolkey
chmod 600 ~/.pgpoolkey

- [all servers] Execute command pg_enc -m -k /path/to/.pgpoolkey -u username -p to register user name and AES encrypted password in file pool_passwd. If pool_passwd doesn't exist yet, it will be created in the same directory as pgpool.conf.
su - postgres
pg_enc -m -k ~/.pgpoolkey -u pgpool -p
--> db password: [pgpool user's password]

pg_enc -m -k ~/.pgpoolkey -u postgres -p
--> db password: [postgres user's password]

# cat /etc/pgpool-II/pool_passwd
pgpool:AESheq2ZMZjynddMWk5sKP/Rw==
postgres:AESHs/pWL5rtXy2IwuzroHfqg==

- [All Servers] To allow repl user without specifying password for streaming replication and online recovery, and execute pg_rewind using postgres, we create the .pgpass file in postgres user's home directory and change the permission to 600 on each PostgreSQL server.
su - postgres
cat <<'EOF' | tee /var/lib/pgsql/.pgpass
server-01:5432:replication:repl:repl
server-02:5432:replication:repl:repl
server-03:5432:replication:repl:repl
server-01:5432:postgres:postgres:changeme
server-02:5432:postgres:postgres:changeme
server-03:5432:postgres:postgres:changeme
EOF
chmod 600 /var/lib/pgsql/.pgpass
chown postgres:postgres /var/lib/pgsql/.pgpass
exit

- Create the log directory on all servers.
mkdir /var/log/pgpool_log/
chown postgres:postgres /var/log/pgpool_log/

- [All Servers] Create failover and follow_primary scripts from sample files
cp -p /etc/pgpool-II/failover.sh{.sample,}
cp -p /etc/pgpool-II/follow_primary.sh{.sample,}
sed -i "s@^ARCHIVEDIR=*.*@ARCHIVEDIR=$PGARCHIVE@" /etc/pgpool-II/follow_primary.sh

- Online recovery sample scriptsrecovery_1st_stage and pgpool_remote_start are installed in /etc/pgpool-II/. Copy these files to the data directory of the primary server (server1).
cp -p /etc/pgpool-II/recovery_1st_stage.sample $PGDATA/recovery_1st_stage
cp -p /etc/pgpool-II/pgpool_remote_start.sample $PGDATA/pgpool_remote_start
chown postgres:postgres $PGDATA/{recovery_1st_stage,pgpool_remote_start}
sed -i "s@^ARCHIVEDIR=*.*@ARCHIVEDIR=$PGARCHIVE@" $PGDATA/recovery_1st_stage

- In order to use the online recovery functionality, the functions of pgpool_recovery, pgpool_remote_start, pgpool_switch_xlog are required, so we need to install pgpool_recovery on template1 of PostgreSQL server server1.
sudo -u postgres psql template1 -c "CREATE EXTENSION pgpool_recovery"

- Allow postgres run ip/arping via sudo without a password.
cat <<'EOF' | sudo tee -a /etc/sudoers.d/postgres
postgres ALL=NOPASSWD: /sbin/ip
postgres ALL=NOPASSWD: /usr/sbin/arping
EOF

- [All Servers] The sample script escalation.sh is installed in /etc/pgpool-II/
cp -p /etc/pgpool-II/escalation.sh{.sample,}
sed -i 's/^PGPOOLS=*.*/PGPOOLS=(server-01 server-02 server-03)/' /etc/pgpool-II/escalation.sh
sed -i 's/^VIP=*.*/VIP=192.168.100.20/' /etc/pgpool-II/escalation.sh
sed -i 's/^DEVICE=*.*/DEVICE=eth0/' /etc/pgpool-II/escalation.sh

- [All Servers] then we use pg_md5 to create the encrypted password entry for pgpool user as below:
su - postgres
echo 'pgpool:'`pg_md5 pgpool` > /etc/pgpool-II/pcp.conf
echo 'localhost:9898:pgpool:pgpool' > ~/.pcppass
chmod 600 ~/.pcppass

3.3.2. Create pgpool_node_id
From Pgpool-II 4.2, now all configuration parameters are identical on all hosts. If watchdog feature is enabled, to distinguish which host is which, a pgpool_node_id file is required. You need to create a pgpool_node_id file and specify the pgpool (watchdog) node number (e.g. 0, 1, 2 ...) to identify pgpool (watchdog) host.

- Server-01:
echo "0" | tee /etc/pgpool-II/pgpool_node_id

- Server-02:
echo "1" | tee /etc/pgpool-II/pgpool_node_id

- Server-03:
echo "2" | tee /etc/pgpool-II/pgpool_node_id

3.3.3. Pgpool-II Configuration
cp /etc/pgpool-II/pgpool.conf{,_`date +"%d%m%Y"`}
cat <<'EOF' | tee /etc/pgpool-II/pgpool.conf
#--------------------------------------------------------------------------------
# BACKEND CLUSTERING MODE
#--------------------------------------------------------------------------------
backend_clustering_mode = 'streaming_replication'

#---------------------------------------------------------------------------------
# LOGS
#---------------------------------------------------------------------------------
log_destination = 'stderr'
logging_collector = on
log_directory = '/var/log/pgpool_log'
log_filename = 'pgpool-%Y-%m-%d_%H%M%S.log'
log_truncate_on_rotation = on
log_rotation_age = 1d
log_rotation_size = 10MB

#--------------------------------------------------------------------------------
# CONNECTIONS
#--------------------------------------------------------------------------------
# - Pgpool Connection Settings -
listen_addresses = '*'
port = 9999
socket_dir = '/var/run/postgresql'

# - pgpool Communication Manager Connection Settings -
pcp_listen_addresses = '*'
pcp_port = 9898
pcp_socket_dir = '/var/run/postgresql'

#--------------------------------------------------------------------------------
# HEALTH CHECK GLOBAL PARAMETERS
#--------------------------------------------------------------------------------
health_check_period = 5
health_check_timeout = 30
health_check_user = 'pgpool'
health_check_password = ''
health_check_max_retries = 3

# - Backend Connection Settings -
backend_hostname0 = 'server-01'
backend_port0 = 5432
backend_weight0 = 1
backend_data_directory0 = '/pgdata'
backend_flag0 = 'ALLOW_TO_FAILOVER'
backend_application_name0 = 'server-01'

backend_hostname1 = 'server-02'
backend_port1 = 5432
backend_weight1 = 1
backend_data_directory1 = '/pgdata'
backend_flag1 = 'ALLOW_TO_FAILOVER'
backend_application_name0 = 'server-02'

backend_hostname2 = 'server-03'
backend_port2 = 5432
backend_weight2 = 1
backend_data_directory2 = '/pgdata'
backend_flag2 = 'ALLOW_TO_FAILOVER'
backend_application_name0 = 'server-03'

# - Authentication -
enable_pool_hba = on
pool_passwd = 'pool_passwd'

#---------------------------------------------------------------------------------
# STREAMING REPLICATION MODE
#---------------------------------------------------------------------------------
sr_check_user = 'pgpool'
sr_check_password = ''

#---------------------------------------------------------------------------------
# FAILOVER AND FAILBACK
#---------------------------------------------------------------------------------
failover_command = '/etc/pgpool-II/failover.sh %d %h %p %D %m %H %M %P %r %R %N %S'
follow_primary_command = '/etc/pgpool-II/follow_primary.sh %d %h %p %D %m %H %M %P %r %R'

#---------------------------------------------------------------------------------
# ONLINE RECOVERY
#---------------------------------------------------------------------------------
recovery_user = 'postgres'
recovery_password = ''
recovery_1st_stage_command = 'recovery_1st_stage'

#---------------------------------------------------------------------------------
# WATCHDOG
#---------------------------------------------------------------------------------
# - Enabling -
use_watchdog = on

# - Virtual IP control Setting -
delegate_IP = '192.168.100.20'
if_up_cmd = '/usr/bin/sudo /sbin/ip addr add $_IP_$/24 dev eth0 label eth0:0'
if_down_cmd = '/usr/bin/sudo /sbin/ip addr del $_IP_$/24 dev eth0'
arping_cmd = '/usr/bin/sudo /usr/sbin/arping -U $_IP_$ -w 1 -I eth0'

# - Watchdog communication Settings -
hostname0 = 'server-01'
wd_port0 = 9000
pgpool_port0 = 9999

hostname1 = 'server-02'
wd_port1 = 9000
pgpool_port1 = 9999

hostname2 = 'server-03'
wd_port2 = 9000
pgpool_port2 = 9999

# - Lifecheck Setting -
wd_lifecheck_method = 'heartbeat'
wd_interval = 10

# -- heartbeat mode --
heartbeat_hostname0 = 'server-01'
heartbeat_port0 = 9694
heartbeat_device0 = ''

heartbeat_hostname1 = 'server-02'
heartbeat_port1 = 9694
heartbeat_device1 = ''

heartbeat_hostname2 = 'server-03'
heartbeat_port2 = 9694
heartbeat_device2 = ''

wd_heartbeat_keepalive = 2
wd_heartbeat_deadtime = 30
wd_escalation_command = '/etc/pgpool-II/escalation.sh'

EOF

- [On Server-01] The configuration of pgpool.conf on server1 is completed. Copy the pgpool.conf to other Pgpool-II nodes (server2 and server3).
su - postgres
scp -i ~/.ssh/id_rsa_pgpool -p /etc/pgpool-II/pgpool.conf postgres@server-02:/etc/pgpool-II/pgpool.conf
scp -i ~/.ssh/id_rsa_pgpool -p /etc/pgpool-II/pgpool.conf postgres@server-03:/etc/pgpool-II/pgpool.conf

3.3.4. /etc/sysconfig/pgpool Configuration
sed -i 's/^OPTS=*.*/OPTS=" -D -n"/' /etc/sysconfig/pgpool

3.3.5. [All Servers] Set Pgpool-II to start automatically on all servers.
sudo systemctl enable --now pgpool

3.3.6. How to use
3.3.6.1. Set up PostgreSQL standby server
