# PGPool-II + Watchdog Setup Example
** Reference: https://www.pgpool.net/docs/latest/en/html/example-cluster.html

# Requirements
We assume that all the Pgpool-II servers and the PostgreSQL servers are in the same subnet.

# Cluster System Configuration
* We will use 3 servers with RHEL 8.x installed. We will install PostgreSQL 14 and Pgpool-II (4.3.0) on each server
Figure-1. Cluster System configuration
https://www.pgpool.net/docs/latest/en/html/cluster_40.gif

**Note**: The roles of Active, Standby, Primary, Standby are not fixed and may be changed by further operations.

* Table-1. Hostname and IP address
Hostname  | IP address     | VIP
---------------------------------------------
pgpool-01 | 192.168.100.21 | 
pgpool-02 | 192.168.100.22 | 192.168.100.20
pgpool-03 | 192.168.100.23 |

* Table-2. PostgreSQL version and Configuration
Item                | Value                 | Detail
------------------------------------------------------------
PostgreSQL version  | 14.0                  | -
port                | 5432                  | -
$PGDATA             | /pgdata               | -
Archive mode        | on                    | /pgarchive
Replication Slots   | Enable                | -
Start automatically | Enable                | -
------------------------------------------------------------


* Table-3. Pgpool-II Version and Configuration
Item                | Value                         | Detail
-------------------------------------------------------------
Pgpool-II version   | 4.3.0                         | -
Port                | 9999                          | Pgpool-II accepts connections
Port                | 9898                          | PCP process accepts connections
Port                | 9000                          | Watchdog accepts connections
Port                | 9694                          | UDP port for receiving Watchdog's heartbeat signal
Config file         | /etc/pgpool-II/pgpool.conf    | Pgpool-II config file
Pgpool-II start user| postgres (Pgpool-II 4.1 or later)| Pgpool-II 4.0 or before, the default startup user is root
Running mode        | streaming replication mode    | -
Watchdog            | on                            | Life check method: heartbeat
Start automatically | Enable                        | -
-------------------------------------------------------------

* Table 4. Various sample scripts included in rpm package
Feature             | Script                                    | Detail
---------------------------------------------------------------------------------------
Failover            | /etc/pgpool-II/failover.sh.sample	        | Run by failover_command to perform failover
                    | /etc/pgpool-II/follow_primary.sh.sample   | Run by follow_primary_command to synchronize the Standby with the new Primary after failover.
Online recovery	    | /etc/pgpool-II/recovery_1st_stage.sample	| Run by recovery_1st_stage_command to recovery a Standby node
                    | /etc/pgpool-II/pgpool_remote_start.sample	| Run after recovery_1st_stage_command to start the Standby node
Watchdog            | /etc/pgpool-II/escalation.sh.sample	    | Run by wd_escalation_command to switch the Active/Standby Pgpool-II safely
----------------------------------------------------------------------------------------
**Note**: The above scripts are included in the RPM package and can be customized as needed.

# Installation
* On all servers, Install PostgreSQL from PostgreSQL YUM repository. Reference: https://www.postgresql.org/download/linux/redhat/
sudo dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
sudo dnf -qy module disable postgresql
sudo dnf install -y postgresql14-server pgaudit16_14 policycoreutils-python-utils

* Since Pgpool-II related packages are also included in PostgreSQL YUM repository, add the "exclude" settings to /etc/yum.repos.d/pgdg-redhat-all.repo so that Pgpool-II is not installed from PostgreSQL YUM repository.
sed -i '/^repo_gpgcheck/a exclude=pgpool*' /etc/yum.repos.d/pgdg-redhat-all.repo

* Install Pgpool-II from Pgpool-II YUM repository.
sudo dnf install -y https://www.pgpool.net/yum/rpms/4.3/redhat/rhel-8-x86_64/pgpool-II-release-4.3-1.noarch.rpm
sudo dnf install -y pgpool-II-pg14-*

# Before Starting
Before you start the configuration process, please check the following prerequisites.
## Local Firewall Settings


## Setup PostgreSQL
* Set PostgreSQL variables
cat <<'EOF' | sudo tee /etc/profile.d/pgvar.sh
export PGDATA=/pgdata
export PGARCHIVE=/pgarchive
export PGPORT=5432
EOF
sudo source /etc/profile.d/pgvar.sh

* Prepare pgdata and pgarchive. Optional: install "policycoreutils-python-utils" if has no "semanage" command
sudo mkdir /pg{data,archive}
sudo chown -R postgres:postgres $PGDATA $PGARCHIVE
sudo chmod o-rwx $PGDATA $PGARCHIVE
sudo semanage fcontext -a -t postgresql_db_t "$PGDATA(/.*)?"
sudo semanage fcontext -a -t postgresql_db_t "$PGARCHIVE(/.*)?"
sudo restorecon -Rv $PGDATA
sudo restorecon -Rv $PGARCHIVE

* Update PostgreSQL daemon service to use the "/pgdata" directory
sudo sed -i "s@^Environment=PGDATA=.*@Environment=PGDATA=$PGDATA@" /usr/lib/systemd/system/postgresql*.service
sudo sed -i "s@^PGDATA=.*@PGDATA=$PGDATA@" /var/lib/pgsql/.bash_profile
sudo systemctl daemon-reload

* Initialize the first database
sudo postgresql-14-setup initdb

* Start and enable service PostgreSQL
sudo systemctl enable --now postgresql-14

* Setting PostgreSQL database
echo "umask 077" >> /var/lib/pgsql/.bash_profile
groupadd pg_wheel && gpasswd -a postgres pg_wheel

mkdir -p /var/log/postgres && \
chown postgres:postgres /var/log/postgres && \
chmod 750 /var/log/postgres

sudo -u postgres psql <<'EOF'
-- Ensure the log destinations are set correctly --
alter system set log_destination = 'csvlog';
-- Ensure the logging collector is enabled --
alter system set logging_collector = 'on';
-- Ensure the log file destination directory is set correctly --
alter system set log_directory='/var/log/postgres';
-- Ensure the filename pattern for log files is set correctly --
alter system set log_filename='postgresql-%d%m%Y.log';
-- Ensure the log file permissions are set correctly --
alter system set log_file_mode = '0600';
-- Ensure 'log_truncate_on_rotation' is enabled --
alter system set log_truncate_on_rotation = 'on';
-- Ensure the maximum log file lifetime is set correctly --
alter system set log_rotation_age='1d';
-- Ensure the maximum log file size is set correctly --
alter system set log_rotation_size = '0';
-- Ensure the correct syslog facility is selected --
alter system set syslog_facility = 'LOCAL1';
-- Ensure the program name for PostgreSQL syslog messages is correct --
alter system set syslog_ident = 'postgres';
-- Ensure the correct messages are written to the server log --
alter system set log_min_messages = 'warning';
-- Ensure the correct SQL statements generating errors are recorded --
alter system set log_min_error_statement = 'error';
-- Ensure 'debug_print_parse' is disabled --
alter system set debug_print_parse='off';
-- Ensure 'debug_print_rewritten' is disabled --
alter system set debug_print_rewritten = 'off';
-- Ensure 'debug_print_plan' is disabled --
alter system set debug_print_plan = 'off';
-- Ensure 'debug_pretty_print' is enabled --
alter system set debug_pretty_print = 'on';
-- Ensure 'log_connections' is enabled --
alter system set log_connections = 'on';
-- Ensure 'log_disconnections' is enabled --
alter system set log_disconnections = 'on';
-- Ensure 'log_error_verbosity' is set correctly --
alter system set log_error_verbosity = 'verbose';
-- Ensure 'log_hostname' is set correctly --
alter system set log_hostname='off';
-- Ensure 'log_line_prefix' is set correctly --
alter system set log_line_prefix = '%t [%p]: [%l-1] db=%d,user=%u,app=%a,client=%h ';
-- Ensure 'log_statement' is set correctly --
alter system set log_statement='ddl';
-- Ensure 'log_timezone' is set correctly --
alter system set log_timezone = 'UTC';
-- Ensure the PostgreSQL Audit Extension (pgAudit) is enabled --
alter system set shared_preload_libraries = 'pgaudit';
EOF

echo "pgaudit.log='ddl,write'" >> $PGDATA/postgresql.auto.conf
echo '%pg_wheel ALL= /bin/su - postgres' > /etc/sudoers.d/postgres
chmod 600 /etc/sudoers.d/postgres

sudo -u postgres psql <<'EOF'
-- Set listen address to any address --
alter system set listen_addresses = '*';
-- Set password encryption with scram-sha-256 --
alter system set password_encryption = 'scram-sha-256';
EOF

sudo -u postgres psql -c "alter system set port = '$PGPORT';"
semanage port -a -t postgresql_port_t -p tcp $PGPORT

cp -p $PGDATA/pg_hba.conf{,_`date +"%d%m%Y"`}
cat > $PGDATA/pg_hba.conf <<'EOF'
# TYPE         DATABASE        USER        ADDRESS         METHOD
# Only local be able to access Postgres with "peer"
local    all    all        peer

# Also allow the host unrestricted access to connect to itself
host    all     all    127.0.0.1/32    trust
host    all     all    ::1/128         trust
EOF

# Restart PostgreSQL service to apply the change
sudo systemctl restart postgresql-14

# Do not forget to set/change your postgres role password
sudo -u postgres psql -p $PGPORT -c "alter role postgres with password 'changeme4db';"

## Tuning your PostgreSQL
https://pgtune.leopard.in.ua/#/

## Set up PostgreSQL streaming replication on the primary server. In this example, we use WAL archiving.
sudo -u postgres psql -p $PGPORT <<EOF
alter system set archive_mode = on;
alter system set archive_command = 'cp "%p" "$PGARCHIVE/%f"';
alter system set max_wal_senders = 10;
alter system set max_replication_slots = 10;
alter system set wal_level = replica;
alter system set hot_standby = on;
alter system set wal_log_hints = on;
EOF
sudo systemctl restart postgresql-14

* Because of the security reasons, we create a user repl solely used for replication purpose, and a user pgpool for streaming replication delay check and health check of Pgpool-II. Server 1 only
* Table-5. Users
User Name   | Password  | Detail
-----------------------------------
repl        | repl      | PostgreSQL replication user
pgpool      | pgpool	| Pgpool-II health check (health_check_user) and replication delay check (sr_check_user) user
postgres	| postgres	| User running online recovery
-----------------------------------

sudo -u postgres psql -p $PGPORT <<EOF
create role repl with replication login password 'repl';
create role pgpool with login password 'pgpool';
GRANT pg_monitor TO pgpool;
select pg_reload_conf();
EOF

* Assuming that all the Pgpool-II servers and the PostgreSQL servers are in the same subnet and edit pg_hba.conf to enable scram-sha-256 authentication method.
cat <<'EOF' | sudo tee -a $PGDATA/pg_hba.conf

# Allow connection for replication
host    replication     all    samenet         scram-sha-256
EOF
sudo -u postgres psql -p $PGPORT -c 'select pg_reload_conf();'

* To use the automated failover and online recovery of Pgpool-II, the settings that allow passwordless SSH to all backend servers between Pgpool-II execution user (default root user) and postgres user and between postgres user and postgres user are necessary. Execute the following command on all servers to set up passwordless SSH. The generated key file name is id_rsa_pgpool.



* We set Pgpool-II to start automatically on all servers.
systemctl enable pgpool.service