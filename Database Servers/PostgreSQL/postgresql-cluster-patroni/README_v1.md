# PostgreSQL Clustering with Patroni, etcd, HAproxy, KeepAlivedD and Pgbouncer
[[_TOC_]]
## Before you begin
* Env for all nodes
```
TOKEN=pgcluster
CLUSTER_STATE=new
NAME_1=DBPG01
NAME_2=DBPG02
NAME_3=DBPG03
HOST_1=192.168.43.31
HOST_2=192.168.43.32
HOST_3=192.168.43.33
CLUSTER_IP=192.168.43.30
CLUSTER=${NAME_1}=http://${HOST_1}:2380,${NAME_2}=http://${HOST_2}:2380,${NAME_3}=http://${HOST_3}:2380
echo "export TOKEN=${TOKEN} CLUSTER_STATE=${CLUSTER_STATE} NAME_1=${NAME_1} NAME_2=${NAME_2} NAME_3=${NAME_3} HOST_1=${HOST_1} HOST_2=${HOST_2} HOST_3=${HOST_3} CLUSTER_IP=${CLUSTER_IP} CLUSTER=${CLUSTER}" >> ~/.bashrc
```

* Firewall settings
```
firewall-cmd --permanent --new-service=PGCLUSTER
for PORT in 22/tcp 2379/tcp 2380/tcp 8008/tcp 5432/tcp
do
    firewall-cmd --permanent --service=PGCLUSTER --add-port=$PORT
done
firewall-cmd --permanent --new-ipset=PGHOSTS --type=hash:net
for HOST in ${HOST_1} ${HOST_2} ${HOST_3}
do
    firewall-cmd --permanent --ipset=PGHOSTS --add-entry=$HOST
done
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source ipset=PGHOSTS service name=PGCLUSTER accept'
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source ipset=PGHOSTS destination address=224.0.0.18/32 protocol value=vrrp accept'
firewall-cmd --reload
```

## Required packages installation
* Install these below required packages on all nodes.
```
# Install PostgreSQL
sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm && \
sudo yum install -y \
    postgresql12 \
    postgresql12-server \
    postgresql12-contrib \
    pgaudit14_12 \
    policycoreutils-python && \
systemctl disable postgresql-12

# Install Patroni
sudo yum install -y \
    gcc \
    python3 \
    python3-pip \
    python3-psycopg2 \
    http://mirror.centos.org/centos/7/os/x86_64/Packages/python3-devel-3.6.8-10.el7.x86_64.rpm

sudo pip3 install --upgrade pip && \
sudo pip3 install --upgrade setuptools && \
sudo pip3 install patroni[etcd]
sudo pip3 install psycopg2-binary

# Install etcd
sudo yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/etcd-3.3.11-2.el7.centos.x86_64.rpm

# Install HAProxy
sudo yum install -y haproxy

# Install KeepAliveD
sudo yum install -y keepalived
```

## Clustering configuration
* Set variable for host 1
```
THIS_NAME=${NAME_1}
THIS_IP=${HOST_1}
```

* Set variable for host 2
```
THIS_NAME=${NAME_2}
THIS_IP=${HOST_2}
```

* Set variable for host 3
```
THIS_NAME=${NAME_3}
THIS_IP=${HOST_3}
```

### Configuring ETCD
* Set this ETCD configuration for each hosts
```
cp /etc/etcd/etcd.conf /etc/etcd/etcd.conf_`date +"%d%m%Y"`
cat <<EOF | sudo tee /etc/etcd/etcd.conf
# [Member]
ETCD_NAME="${THIS_NAME}"
ETCD_DATA_DIR="/var/lib/etcd/default.etcd"
ETCD_LISTEN_PEER_URLS="http://${THIS_IP}:2380"
ETCD_LISTEN_CLIENT_URLS="http://${THIS_IP}:2379,http://127.0.0.1:2379"

# [Cluster]
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://${THIS_IP}:2380"
ETCD_INITIAL_CLUSTER="${CLUSTER}"
ETCD_INITIAL_CLUSTER_STATE="${CLUSTER_STATE}"
ETCD_INITIAL_CLUSTER_TOKEN="${TOKEN}"
ETCD_ADVERTISE_CLIENT_URLS="http://${THIS_IP}:2379"
EOF
```

* Start and enable ETCD service
```
systemctl start etcd && systemctl enable etcd
```

* Verify ETCD member list
```
etcdctl member list
```

### Configuring Patroni
* Create Patroni systemd service on all nodes
```
cat <<EOF | sudo tee /usr/lib/systemd/system/patroni.service
[Unit]
Description=Runners to orchestrate a high-availability PostgreSQL
After=syslog.target network.target

[Service]
Type=simple
User=postgres
Group=postgres
ExecStart=`which patroni` /etc/patroni.yml
KillMode=process
TimeoutSec=30
Restart=no

[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
```

* Create PostgreSQL data directories on all nodes
```
PGDATA=/pgdata
PGARCHIVE=/pgarchive
echo "export PGDATA=${PGDATA} PGARCHIVE=${PGARCHIVE}" >> ~/.bashrc
mkdir -p $PGDATA $PGARCHIVE
chown -R postgres:postgres {$PGDATA,$PGARCHIVE}
chmod -R 700 {$PGDATA,$PGARCHIVE}
semanage fcontext -a -t postgresql_db_t "$PGDATA(/.*)?"
semanage fcontext -a -t postgresql_db_t "$PGARCHIVE(/.*)?"
restorecon -Rv {$PGDATA,$PGARCHIVE}
```

* Create PostgreSQL log directory on each hosts
```
mkdir -p /var/log/postgres && \
chown postgres:postgres /var/log/postgres && \
chmod 750 /var/log/postgres
```

* Create Patroni configuration for each hosts
```
cat <<EOF | sudo tee /etc/patroni.yml
scope: pgcluster
namespace: /db/
name: ${THIS_NAME}

restapi:
    listen: ${THIS_IP}:8008
    connect_address: ${THIS_IP}:8008

etcd:
    hosts: 
    - ${HOST_1}:2379
    - ${HOST_2}:2379
    - ${HOST_3}:2379

bootstrap:
    dcs:
        ttl: 30
        loop_wait: 10
        retry_timeout: 10
        maximum_lag_on_failover: 1048576
        postgresql:
            use_pg_rewind: true

    initdb:
    - encoding: UTF8
    - data-checksums

    pg_hba:
    - host replication replicator 127.0.0.1/32 md5
    - host replication replicator ${HOST_1}/0 md5
    - host replication replicator ${HOST_2}/0 md5
    - host replication replicator ${HOST_3}/0 md5
    - host all all 0.0.0.0/0 md5

    users:
        admin:
            password: admin
            options:
                - createrole
                - createdb

postgresql:
    listen: ${THIS_IP}:5432
    bin_dir: /usr/pgsql-12/bin
    connect_address: ${THIS_IP}:5432
    data_dir: ${PGDATA}
    pgpass: /tmp/pgpass
    unix_socket_directories: ${PGDATA}
    authentication:
        replication:
            username: replicator
            password: rep-pass
        superuser:
            username: postgres
            password: secretpassword
    parameters:
        unix_socket_directories: '.'
        timezone: 'Asia/Phnom_Penh'
        password_encryption: 'md5'
        shared_preload_libraries: 'pg_stat_statements, pgaudit'
        pg_stat_statements.max: '5000'
        pg_stat_statements.track: all
        pgaudit.log: 'ddl,write'

        ## Archive Settings
        archive_mode: on
        archive_command: 'test ! -f $PGARCHIVE/%f && cp %p $PGARCHIVE/%f'
        archive_cleanup_command: 'pg_archivecleanup $PGARCHIVE/ %r'

        ## Logging and Debuging
        log_destination: 'csvlog'
        logging_collector: 'on'
        log_directory: '/var/log/postgres'
        log_filename: 'postgresql-%d%m%Y.log'
        log_file_mode: '0600'
        log_truncate_on_rotation: 'on'
        log_rotation_age: '1d'
        log_rotation_size: '0'
        syslog_facility: 'LOCAL1'
        syslog_ident: 'postgres'
        log_min_messages: 'warning'
        log_min_error_statement: 'error'
        debug_print_parse: 'off'
        debug_print_rewritten: 'off'
        debug_print_plan: 'off'
        debug_pretty_print: 'on'
        log_connections: 'on'
        log_disconnections: 'on'
        log_error_verbosity: 'verbose'
        log_hostname: 'off'
        log_line_prefix: '%t [%p]: [%l-1] db=%d,user=%u,app=%a,client=%h '
        log_statement: 'ddl'
        log_timezone: 'Asia/Phnom_Penh'

tags:
    nofailover: false
    noloadbalance: false
    clonefrom: false
    nosync: false
EOF
```

* Start and enable patroni service
```
systemctl start patroni && systemctl enable patroni
```

* List all patroni member
```
patronictl -c /etc/patroni.yml list
```

### Configuring HAproxy
* Configure HAProxy settings for postgres cluster on each hosts
```
cp /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg_`date +"%d%m%Y"`
cat <<EOF | sudo tee /etc/haproxy/haproxy.cfg
# Global & Defaults Config
global
    log 127.0.0.1 local0
    chroot /var/lib/haproxy
    pidfile /var/run/haproxy.pid
    stats socket /var/run/haproxy.sock mode 0600 level admin
    user haproxy
    group haproxy
    daemon
    maxconn 2048

defaults
    mode tcp
    log global
    option dontlognull
    option redispatch
    retries 2
    timeout client 30m
    timeout connect 4s
    timeout server 30m
    timeout check 5s

# HAProxy Stats
listen stats
    bind :9000
    mode http
    stats enable
    stats hide-version
    stats realm Haproxy\ Statistics
    stats uri /haproxy-stats
    stats auth admin:secretpassword

# Connections to PG Master (Read/Write) port 5001
listen Master
    bind *:5001
    option httpchk
    http-check expect status 200
    default-server inter 3s fall 3 rise 2 on-marked-down shutdown-sessions
    server ${NAME_1} ${HOST_1}:5432 maxconn 2048 check port 8008
    server ${NAME_2} ${HOST_2}:5432 maxconn 2048 check port 8008
    server ${NAME_3} ${HOST_3}:5432 maxconn 2048 check port 8008

# Connections to PG Slave (Read only) port 5002
listen Slaves
    bind *:5002
    option httpchk
    http-check expect status 503
    default-server inter 3s fall 3 rise 2 on-marked-down shutdown-sessions
    server ${NAME_1} ${HOST_1}:5432 maxconn 2048 check port 8008
    server ${NAME_2} ${HOST_2}:5432 maxconn 2048 check port 8008
    server ${NAME_3} ${HOST_3}:5432 maxconn 2048 check port 8008
EOF
```
* Verify HAProxy configuration
```
haproxy -c -V -f /etc/haproxy/haproxy.cfg
```

* Allow SELinux for HAProxy to bind any port
```
setsebool -P haproxy_connect_any 1
```

* Start and enable HAproxy service
```
systemctl start haproxy && systemctl enable haproxy
```

### Configuring KeepAliveD
* Set keepalived variable for host 1
```
STATE=MASTER
PRIORITY=103
```

* Set keepalived variable for host 2
```
STATE=BACKUP
PRIORITY=102
```

* Set keepalived variable for host 3
```
STATE=BACKUP
PRIORITY=101
```

* Set KeepaliveD configuration for each hosts
```
cp /etc/keepalived/keepalived.conf /etc/keepalived/keepalived.conf_`date +"%d%m%Y"`
cat <<EOF | sudo tee /etc/keepalived/keepalived.conf
vrrp_script chk_haproxy {
    script "killall -0 haproxy"
    interval 2
    weight 2
}

vrrp_instance pgvip {
    interface eth0
    state ${STATE}
    virtual_router_id ${CLUSTER_IP##*.}
    priority ${PRIORITY}
    virtual_ipaddress {
        ${CLUSTER_IP}
    }
    track_script {
        chk_haproxy
    }
}
EOF
```

* Start and enable KeepaliveD service
```
systemctl start keepalived && systemctl enable keepalived
```

## Allow firewall on all nodes for user access to:
* Postgres master (read/write)
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.1 port port=5001 protocol=tcp accept'
firewall-cmd --reload
```

* Postgres slave (read only)
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.1 port port=5002 protocol=tcp accept'
firewall-cmd --reload
```

* HAProxy stats
```
firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source address=192.168.43.1 port port=9000 protocol=tcp accept'
firewall-cmd --reload
```

