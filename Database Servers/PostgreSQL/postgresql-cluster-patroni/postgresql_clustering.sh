#!/bin/bash
## Inventory
if [ -f inventory ]
then
  export $(cat inventory | sed 's/#.*//g' | xargs)
fi

# Install required packages and its dependencies
install_packages() {
  # Install PostgreSQL
  # Install Patroni
  # Install etcd
  # Install HAproxy
  # Install KeepAliveD
}

# Configure etcd

# Configure Patroni

# Configure HAProxy

# Configure KeepAliveD