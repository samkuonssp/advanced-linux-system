# PostgreSQL Cluster with Patroni + ETCD + HAProxy + KeepAliveD (Optional)

[[_TOC_]]

## Setup Requirements
We assume that all the `PostgreSQL, Patroni, ETCD, HAProxy and KeepAliveD` servers are in the same subnet. We will use 3 servers with Red Hat Linux Enterprise 8.x (RHEL 8.x) installed.

### Cluster System Configuration

**Note**: The roles of Active, Standby, Primary, Standby are not fixed and may be changed by further operations.

### Hostname & IP Addresses
| Hostname  | IP address     | Cluster VIP   |
| --------- | -------------- | ------------- |
| server-01 | 192.168.100.21 |               |
| server-02 | 192.168.100.22 | 192.168.100.20|
| server-03 | 192.168.100.23 |               |

**Note**: if you already have your own load balancer, you could skip `KeepAliveD` and `Cluster VIP` here.

### PostgreSQL version & Configuration
| Item                | Value    | Detail     |
| ------------------- | -------- | ---------- |
| PostgreSQL version  | 14.0     | -          |
| port                | 5432     | -          |
| $PGDATA             | /pgdata  | -          |
| Archive mode        | on       | /pgarchive |
| Start automatically | Enable   | -          |

### Users
| User Name   | Password  | Detail                     |
| ----------- | --------- | -------------------------- |
| replicator  | pass4repl | PostgreSQL replication user|
| postgres    | pass4pgdb | DB Superuser               |
| admin       | pass4adm  | Additional user which needed to be created after initialization new cluster |

### Services & Ports requirement
| Service    | Port/Protocol | Description |
| ---------- | ------------- | ------------- |
| ETCD       | 2379/TCP      | Clent requests|
|            | 2380/TCP      | Peer communications |
| Patroni    | 8008/TCP      | Patroni's Rest API |
| HAProxy    | 5001/TCP      | HAProxy port for DB Master (Writable) |
|            | 5002/TCP      | HAProxy port for DB Slave(s) (Read Only) |
| KeepAliveD | VRRP          | To provide Cluster VIP or floating IP |
| PostgreSQL | 5432/TCP      | Default port for database service |

## Packages Installation
### Install PostgreSQL
- Install the PostgreSQL repository RPM. You can check the latest here: https://www.postgresql.org/download/linux/redhat/
```bash
sudo dnf install -y \
    yum-utils \
    https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
```

- Disable old PostgreSQL version repo and the built-in PostgreSQL module
```bash
sudo yum-config-manager --disable $(dnf repolist | egrep 'pgdg' | egrep -v 'pgdg-common|pgdg14' | awk '{print $1}')
sudo dnf -qy module disable postgresql
```

- Install PostgreSQL 14, pgaudit and SELinux tools
```bash
sudo dnf install -y \
    postgresql14-server \
    postgresql14-contrib \
    pgaudit16_14 \
    policycoreutils-python-utils
```

- Disable PostgreSQL service auto startup, since its service will handle by `patroni`
```bash
sudo systemctl disable --now postgresql-14
```

### Install Patroni
- Use the package manager from your distro
```bash
sudo dnf install -y \
    python3 \
    python3-pip \
    python3-psycopg2
```

- Install psycopg2 from the binary package
```bash
sudo pip3 install --upgrade pip setuptools
sudo pip3 install patroni[etcd] psycopg2-binary
```

### Install ETCD
- Download latest `ETCD` packages. Find the latest release here: https://github.com/etcd-io/etcd/releases/
```bash
ETCD_VER=v3.5.2

# choose either URL
GOOGLE_URL=https://storage.googleapis.com/etcd
GITHUB_URL=https://github.com/etcd-io/etcd/releases/download
DOWNLOAD_URL=${GOOGLE_URL}

rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test

curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd-download-test --strip-components=1
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

sudo mv /tmp/etcd-download-test/{etcd,etcdctl,etcdutl} /usr/local/bin/

etcd --version
etcdctl --version
etcdutl --version
```

### Install HAProxy
```bash
sudo dnf install -y haproxy
```

### Install KeepAliveD (Optional)
```bash
sudo dnf install -y keepalived
```

## Cluster Configuration
### Prerequistes
#### Cluster Setup Environement Variables. 
- Apply all nodes
```bash
NODE_1=server-01
NODE_2=server-02
NODE_3=server-03
NODE_IP1=192.168.100.21
NODE_IP2=192.168.100.22
NODE_IP3=192.168.100.23
CLUSTER_VIP=192.168.100.20
CLUSTER_TOKEN=pgcluster
CLUSTER_STATE=new
```

```bash
cat <<EOF | sudo tee /etc/profile.d/pgcluster_env.sh
export NODE_1=${NODE_1}
export NODE_IP1=${NODE_IP1}
export NODE_2=${NODE_2}
export NODE_IP2=${NODE_IP2}
export NODE_3=${NODE_3}
export NODE_IP3=${NODE_IP3}
export CLUSTER_VIP=${CLUSTER_VIP}
export CLUSTER_TOKEN=${CLUSTER_TOKEN}
export CLUSTER_STATE=${CLUSTER_STATE}
export CLUSTER_NODES=${NODE_1}=http://${NODE_IP1}:2380,${NODE_2}=http://${NODE_IP2}:2380,${NODE_3}=http://${NODE_IP3}:2380
EOF
source /etc/profile.d/pgcluster_env.sh
```

- Set variable for NODE-01
```bash
cat <<EOF | sudo tee -a /etc/profile.d/pgcluster_env.sh
THIS_NODE=${NODE_1}
THIS_NODE_IP=${NODE_IP1}
EOF
```

- Set variable for NODE-02
```bash
cat <<EOF | sudo tee -a /etc/profile.d/pgcluster_env.sh
THIS_NODE=${NODE_2}
THIS_NODE_IP=${NODE_IP2}
EOF
```

- Set variable for NODE-03
```bash
cat <<EOF | sudo tee -a /etc/profile.d/pgcluster_env.sh
THIS_NODE=${NODE_3}
THIS_NODE_IP=${NODE_IP3}
EOF
```

#### Set hosts file record
```bash
cat <<EOF | sudo tee -a /etc/hosts
${NODE_IP1} ${NODE_1}
${NODE_IP2} ${NODE_2}
${NODE_IP3} ${NODE_3}
EOF
```

#### Local Firewall Settings
- Create custom firewall service for cluster named `pgcluster` and ipset (group IP) named `pgnodes`
```bash
sudo firewall-cmd --permanent --new-service=pgcluster
for srv in 2379/tcp 2380/tcp 8008/tcp 5432/tcp;
do
    sudo firewall-cmd --permanent --service=pgcluster --add-port=${srv};
done

sudo firewall-cmd --permanent --new-ipset=pgnodes --type=hash:net
for node in ${NODE_IP1} ${NODE_IP2} ${NODE_IP3}
do
    firewall-cmd --permanent --ipset=pgnodes --add-entry=${node}
done
firewall-cmd --reload
```

- Allow cluster servers to custom created firewall service named `pgcluster`
```bash
sudo firewall-cmd --permanent --add-rich-rule='rule family=ipv4 source ipset=pgnodes service name=pgcluster accept'
sudo firewall-cmd --permanent --add-rich-rule='rule protocol value="vrrp" accept'
firewall-cmd --reload
```

### Configure ETCD
- Set `ETCD` configuration for each node
```bash
cat <<EOF | sudo tee /etc/etcd/etcd.conf
# [Member]
ETCD_NAME="${THIS_NODE}"
ETCD_DATA_DIR="/var/lib/etcd/default.etcd"
ETCD_LISTEN_PEER_URLS="http://${THIS_NODE_IP}:2380"
ETCD_LISTEN_CLIENT_URLS="http://${THIS_NODE_IP}:2379,http://127.0.0.1:2379"

# [Cluster]
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://${THIS_NODE_IP}:2380"
ETCD_INITIAL_CLUSTER="${CLUSTER_NODES}"
ETCD_INITIAL_CLUSTER_STATE="${CLUSTER_STATE}"
ETCD_INITIAL_CLUSTER_TOKEN="${CLUSTER_TOKEN}"
ETCD_ADVERTISE_CLIENT_URLS="http://${THIS_NODE_IP}:2379"
EOF
```

- Configure `systemd` to run `ETCD` service
```bash
sudo groupadd --system etcd
sudo useradd -s /sbin/nologin --system -g etcd etcd
sudo mkdir -p /var/lib/etcd/
sudo mkdir /etc/etcd
sudo chown -R etcd:etcd /var/lib/etcd/
sudo chmod -R 700 /var/lib/etcd/

cat <<EOF | sudo tee /etc/systemd/system/etcd.service
[Unit]
Description=Etcd Server
After=network.target
After=network-online.target
Wants=network-online.target

[Service]
Type=notify
WorkingDirectory=/var/lib/etcd/
EnvironmentFile=-/etc/etcd/etcd.conf
User=etcd
# set GOMAXPROCS to number of processors
ExecStart=/bin/bash -c "GOMAXPROCS=$(nproc) /usr/bin/etcd"
Restart=on-failure
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF
```

- Enable `ETCD` auto startup and start its service
```bash
sudo systemctl daemon-reload
sudo systemctl enable --now etcd
```

- Verify `ETCD` member and status
```bash
etcdctl member list -w table
etcdctl endpoint health --cluster -w table
etcdctl endpoint status --cluster -w table

etcdctl endpoint health
etcdctl put /message "Hello World"
etcdctl put foo "Hello World!"

etcdctl get /message
etcdctl get foo
```

### Configure Patroni
- Create PostgreSQL data directories on all nodes
```bash
PGBIN=/usr/pgsql-14/bin
PGDATA=/pgdata
PGARCHIVE=/pgarchive
mkdir -p $PGDATA $PGARCHIVE
chown -R postgres:postgres {$PGDATA,$PGARCHIVE}
chmod -R 700 {$PGDATA,$PGARCHIVE}
semanage fcontext -a -t postgresql_db_t "$PGDATA(/.*)?"
semanage fcontext -a -t postgresql_db_t "$PGARCHIVE(/.*)?"
restorecon -Rv {$PGDATA,$PGARCHIVE}
```

- Create PostgreSQL log directory on each node
```bash
mkdir -p /var/log/postgres
chown postgres:postgres /var/log/postgres
chmod 750 /var/log/postgres
```

- Create Patroni configuration for each node
```
cat <<EOF | sudo tee /etc/patroni.yml
scope: pgcluster
namespace: /db/
name: ${THIS_NODE}

restapi:
    listen: ${THIS_NODE_IP}:8008
    connect_address: ${THIS_NODE_IP}:8008

etcd:
    hosts: 
    - ${NODE_IP1}:2379
    - ${NODE_IP2}:2379
    - ${NODE_IP3}:2379

bootstrap:
    dcs:
        ttl: 30
        loop_wait: 10
        retry_timeout: 10
        maximum_lag_on_failover: 1048576
        postgresql:
            use_pg_rewind: true

    initdb:
    - encoding: UTF8
    - data-checksums

    pg_hba:
    - host replication replicator 127.0.0.1/32 scram-sha-256
    - host replication replicator ${NODE_IP1}/0 scram-sha-256
    - host replication replicator ${NODE_IP2}/0 scram-sha-256
    - host replication replicator ${NODE_IP3}/0 scram-sha-256
    - host all all 0.0.0.0/0 scram-sha-256

    users:
        admin:
            password: pass4adm
            options:
                - createrole
                - createdb

postgresql:
    listen: ${THIS_NODE_IP}:5432
    bin_dir: ${PGBIN}
    connect_address: ${THIS_NODE_IP}:5432
    data_dir: ${PGDATA}
    pgpass: /tmp/pgpass
    unix_socket_directories: ${PGDATA}
    authentication:
        replication:
            username: replicator
            password: pass4repl
        superuser:
            username: postgres
            password: pass4pgdb
    parameters:
        unix_socket_directories: '.'
        timezone: 'Asia/Phnom_Penh'
        password_encryption: 'scram-sha-256'
        shared_preload_libraries: 'pg_stat_statements, pgaudit'
        pg_stat_statements.max: '5000'
        pg_stat_statements.track: all
        pgaudit.log: 'ddl,write'

        ## Archive Settings
        archive_mode: on
        archive_command: 'test ! -f $PGARCHIVE/%f && cp %p $PGARCHIVE/%f'
        archive_cleanup_command: 'pg_archivecleanup $PGARCHIVE/ %r'

        ## Logging and Debuging
        log_destination: 'csvlog'
        logging_collector: 'on'
        log_directory: '/var/log/postgres'
        log_filename: 'postgresql-%d%m%Y.log'
        log_file_mode: '0600'
        log_truncate_on_rotation: 'on'
        log_rotation_age: '1d'
        log_rotation_size: '0'
        syslog_facility: 'LOCAL1'
        syslog_ident: 'postgres'
        log_min_messages: 'warning'
        log_min_error_statement: 'error'
        debug_print_parse: 'off'
        debug_print_rewritten: 'off'
        debug_print_plan: 'off'
        debug_pretty_print: 'on'
        log_connections: 'on'
        log_disconnections: 'on'
        log_error_verbosity: 'verbose'
        log_hostname: 'off'
        log_line_prefix: '%t [%p]: [%l-1] db=%d,user=%u,app=%a,client=%h '
        log_statement: 'ddl'
        log_timezone: 'Asia/Phnom_Penh'

tags:
    nofailover: false
    noloadbalance: false
    clonefrom: false
    nosync: false
EOF
```

- Create Patroni systemd service on all nodes
```bash
cat <<EOF | sudo tee /usr/lib/systemd/system/patroni.service
[Unit]
Description=Runners to orchestrate a high-availability PostgreSQL
After=syslog.target network.target

[Service]
Type=simple
User=postgres
Group=postgres
ExecStart=`which patroni` /etc/patroni.yml
KillMode=process
TimeoutSec=30
Restart=no

[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
```

- Enable `Patroni` auto startup and start its service
```
systemctl enable --now patroni
```

* List all patroni member
```
patronictl -c /etc/patroni.yml list
```

### Configure HAProxy
### Configure KeepAliveD (Optional)
