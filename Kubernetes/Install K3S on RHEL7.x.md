# Requirement
yum install -y \
    http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm \
    https://rpm.rancher.io/k3s-selinux-0.1.1-rc1.el7.noarch.rpm

* Firewall on master
firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --permanent --add-port=8472/udp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --reload

* Fireawll on workers
firewall-cmd --permanent --add-port=8472/udp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --reload

# Install K3S
* On master
curl -sfL https://get.k3s.io | sh -
cat /var/lib/rancher/k3s/server/node-token

* On workers
K3S_TOKEN=$(ssh -o StrictHostKeyChecking=no 192.168.43.41 "awk -F: '{print $1}' /var/lib/rancher/k3s/server/node-token")
curl -sfL https://get.k3s.io | K3S_URL=https://192.168.43.41:6443 K3S_TOKEN=$K3S_TOKEN sh -

# Deploying the Kubernetes Dashboard
GITHUB_URL=https://github.com/kubernetes/dashboard/releases
VERSION_KUBE_DASHBOARD=$(curl -w '%{url_effective}' -I -L -s -S ${GITHUB_URL}/latest -o /dev/null | sed -e 's|.*/||')
k3s kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/${VERSION_KUBE_DASHBOARD}/aio/deploy/recommended.yaml

# Dashboard RBAC Configuration
cat > dashboard.admin-user.yml <<'EOF'
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
EOF

cat > dashboard.admin-user-role.yml <<'EOF'
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
EOF

* Deploy the admin-user configuration
k3s kubectl create -f dashboard.admin-user.yml -f dashboard.admin-user-role.yml

* Obtain the Bearer token
k3s kubectl -n kubernetes-dashboard describe secret admin-user-token | grep ^token