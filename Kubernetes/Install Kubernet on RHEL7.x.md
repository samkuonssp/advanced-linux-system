[[_TOC_]]
# Prerequisites
## Server Info
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| rh7k8smtr01 | 192.168.43.41 | RHEL 7 | K8S Master Node 01 | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility |
| rh7k8swkr01 | 192.168.43.44 | RHEL 7 | K8S Worker Node 01 | Kubelet, Kube-proxy, Pod |
| k8swkr02 | 192.168.43.45 | RHEL 7 | K8S Worker Node 02 | Kubelet, Kube-proxy, Pod |

* ``API Server`` – It provides kubernetes API using Jason / Yaml over http, states of API objects are stored in etcd
* ``Scheduler`` – It is a program on master node which performs the scheduling tasks like launching containers in worker nodes based on resource availability
* ``Controller Manager`` – Main Job of Controller manager is to monitor replication controllers and create pods to maintain desired state.
* ``etcd`` – It is a Key value pair data base. It stores configuration data of cluster and cluster state.
* ``Kubectl utility`` – It is a command line utility which connects to API Server on port 6443. It is used by administrators to create pods, services etc.
* ``Kubelet`` – It is an agent which runs on every worker node, it connects to docker  and takes care of creating, starting, deleting containers.
* ``Kube-Proxy`` – It routes the traffic to appropriate containers based on ip address and port number of the incoming request. In other words we can say it is used for port translation.
* ``Pod`` – Pod can be defined as a multi-tier or group of containers that are deployed on a single worker node or docker host.

## Verify the MAC address and product_uuid are unique for every node
* Get MAC by ``ip link`` or ``ifconfig -a`` command
* The product_uuid can be checked by using the command
```
sudo cat /sys/class/dmi/id/product_uuid
```

## Configure Host file for each nodes
```
cat >> /etc/hosts <<'EOF'
192.168.43.41   rh7k8smtr01
192.168.43.44   rh7k8swkr01
192.168.43.41   k8smaster
EOF
```

## Configure local firewall
* For master node or control-plane node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 6443*	| Kubernetes API server	| All |
| TCP	| Inbound	| 2379-2380	| etcd server client API	| kube-apiserver, etcd |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 10251	| kube-scheduler | Self |
| TCP	| Inbound	| 10252	| kube-controller-manager	| Self |

```
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

* For worker node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 30000-32767	| NodePort Services† | All |

```
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

## Letting iptables see bridged traffic
```
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter
sysctl -p /etc/sysctl.d/k8s.conf
```
## Disable SWAP on all nodes
```
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a
```

# Install Docker on each node
* Remove existing Docker
```
sudo yum -y remove \
    docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine \
    docker-ce \
    docker-ce-cli \
    && rm -rf /etc/yum.repos.d/docker-ce*.repo
```
* Install requirement packages
```
sudo yum -y install \
    yum-utils \
    device-mapper-persistent-data \
    lvm2 \
    http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```
* Install Docker CE
```
sudo yum-config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum -y install \
    docker-ce \
    docker-ce-cli \
    containerd.io
systemctl enable docker --now
```

# Install Kubernetes
* Create Kubernetes Repository
```
cat > /etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```
* Install ``kubeadm, kubelet, kubectl`` on each node
```
sudo yum -y install \
    kubeadm \
    kubelet \
    kubectl
systemctl enable kubelet --now
```
* Pull image for ``Kubernetes Cluster``
```
kubeadm config images pull
```

* Create kubeadm-config
```
cat > kubeadm-config.yml <<EOF
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
kubernetesVersion: $(kubeadm version -o short)  #<-- Use the word stable for newest version
controlPlaneEndpoint: k8smaster:6443  #<-- Use the node alias not the IP
networking:
  podSubnet: 192.169.0.0/16  #<-- Match the IP range from the Calico config file
EOF
```

* Initialize Kubernetes Master with ``kubeadm init``
```
kubeadm init --config=kubeadm-config.yml | tee kubeadm-config-init.out

#kubeadm init \
    --kubernetes-version $(kubeadm version -o short) \
    --pod-network-cidr=192.169.0.0/16 \
    --apiserver-advertise-address=k8smgr01
```

* To start using your cluster, you need to run the following as a regular user:
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

* Deploy pod network with ``Calio``
``
curl -Lo calico.yml https://docs.projectcalico.org/manifests/calico.yaml
vim calico.yml
-> Uncomment and change CIRD network as podSubnet
- name: CALICO_IPV4POOL_CIDR
  value: "192.169.0.0/16"

kubectl apply -f calico.yml
``

* Get config and status of cluster and pods
```
kubeadm config print init-defaults
kubectl get nodes
kubectl get pods --all-namespaces
```
* Look at details of the nodes
```
kubectl get nodes
kubectl describe node
kubectl describe node <node hostname>
```

* Get Kubernetes join token command
```
kubeadm token create --print-join-command
```

# Join worker node(s) to cluster
```
kubeadm join 192.168.43.41:6443 --token hb2sm7.kusa7qyosb3a4j65     --discovery-token-ca-cert-hash sha256:64e62573ecbb0a30bc50daab8c0cd43b9ef93174599f2822e11bceb53e82dca0
```

# Optionals
* Deploy the pod network with ``Flannel``
```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

* Deploy the pod network with ``Istio``. Reference: https://istio.io/docs/setup/getting-started/
  1. Download Istio or by this link: https://github.com/istio/istio/releases/
  ```
  curl -L https://istio.io/downloadIstio | sh -
  cd istio-*/
  export PATH=$PWD/bin:$PATH
  ```
  2. Install Istio
  ```
  istioctl install --set profile=demo
  kubectl label namespace default istio-injection=enabled
  ```
* Create an Istio Gateway:
```
kubectl apply -f - <<EOF
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: istio
  name: ingress
spec:
  rules:
  - host: httpbin.example.com
    http:
      paths:
      - path: /status/*
        backend:
          serviceName: httpbin
          servicePort: 8000
EOF
```

* Deploy the pod network with ``Calico``
  1. Reference: https://docs.projectcalico.org/getting-started/kubernetes/self-managed-onprem/onpremises
  2. Download the Calico networking manifest for the Kubernetes API datastore.
  ```
  curl https://docs.projectcalico.org/manifests/calico.yaml -O
  ```
  3. Apply the manifest using the following command.
  ```
  kubectl apply -f calico.yaml
  ```

* Deploy the pod network with ``WeavNet``
```
export kubever=$(kubectl version | base64 | tr -d '\n')
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"
```

* Verify status of cluster and pods again, make cluster is ready, and pods are all running
```
kubectl get nodes
kubectl get pods --all-namespaces
```

## Deployment Details
* To generate the YAML file of the newly created objects, do:
```
kubectl get deployments,rs,pods -o yaml
```

* Sometimes, a JSON output can make it more clear:
```
kubectl get deployments,rs,pods -o json
```

* To scal
```
kubectl scale deploy/dev-web --replicas=4
```

# References:
* https://kubernetes.io/docs/tasks/tools/install-kubectl/