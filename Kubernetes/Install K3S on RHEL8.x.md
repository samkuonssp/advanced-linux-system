[[_TOC_]]

# Requirement
* Set host file
```
cat >> /etc/hosts <<'EOF'
192.168.43.41 k3smgr01
192.168.43.42 k3swkr01
192.168.43.43 k3swkr02
EOF
```

* Currently firewal is settings properly, so suggest to stop firewalld service on each node
```
systemctl stop firewalld && systemctl disable firewalld
```

* Firewall on master
```
firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --permanent --add-port=8472/udp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --add-port=22/tcp
firewall-cmd --reload
```

* Fireawll on workers
```
firewall-cmd --permanent --add-port=8472/udp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --reload
```

* Set dependency repository
```
cat > /etc/yum.repos.d/k3sdeps.repo <<'EOF'
[k3sdeps-baseos]
name=K3S BaseOS Deps on RHEL8.x
baseurl=http://mirror.centos.org/centos/$releasever/BaseOS/$basearch/os/
gpgcheck=0
enabled=1
includepkgs=policycoreutils-python-utils python3-policycoreutils checkpolicy audit-libs-python3 policycoreutils python3-libsemanage python3-setools python3-audit libsemanage audit-libs audit libcgroup

[k3sdeps-appstream]
name=K3S AppStream Deps on RHEL8.x
baseurl=http://mirror.centos.org/centos/$releasever/AppStream/$basearch/os/
gpgcheck=0
enabled=1
includepkgs=container-selinux
EOF
```

* Install ``container-selinux`` package
```
dnf -y install container-selinux \
    https://rpm.rancher.io/k3s-selinux-0.1.1-rc1.el7.noarch.rpm
```

# Install Docker CE runtime
* Install requirement packages
```
dnf -y install lvm2 \
    device-mapper-persistent-data \
    container-selinux \
    https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
```
* Install Docker CE
```
dnf config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo

dnf -y install \
    docker-ce \
    docker-ce-cli

systemctl enable docker --now
```

# Install K3S
* On master
```
curl -sfL https://get.k3s.io | sh -s - --docker
cat /var/lib/rancher/k3s/server/node-token
```

* On workers
```
K3S_URL="https://192.168.43.41:6443"
K3S_TOKEN=$(ssh -o StrictHostKeyChecking=no 192.168.43.41 "cat /var/lib/rancher/k3s/server/node-token")

curl -sfL https://get.k3s.io | K3S_URL=${K3S_URL} K3S_TOKEN=${K3S_TOKEN} sh -s - --docker
```

# Uninstall K3S
* From K3S master
```
/usr/local/bin/k3s-uninstall.sh
```

* From K3S worker(s)
```
/usr/local/bin/k3s-agent-uninstall.sh
```

# Deployment Apps
## Deploy k3s-demo
* Create namespace
```
kubectl create namespace k3s-demo-dev
```

* Prepare the template
```
cat > k3s-demo.yml <<'EOF'
apiVersion: apps/v1
kind: Deployment
metadata:
  name: k3s-demo-deployment
  namespace: k3s-demo-dev
spec:
  replicas: 1
  selector:
    matchLabels:
      app: k3s-demo
  template:
    metadata:
      labels:
        app: k3s-demo
    spec:
      containers:
      - name: k3s-demo
        image: nginx:latest
---
apiVersion: v1
kind: Service
metadata:
  name: k3s-demo-service
  namespace: k3s-demo-dev
spec:
  ports:
  - port: 80
    targetPort: 80
    name: http
  selector:
    app: k3s-demo
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: k3s-demo-ingress
  namespace: k3s-demo-dev
  annotations:
    kubernetes.io/ingress.class: "traefik"

spec:
  rules:
  - host: k3s-demo.lab.local
    http:
      paths:
      - path: /
        backend:
          serviceName: k3s-demo-service
          servicePort: 80
EOF
```

* Get deployment status
```
k3s kubectl get ingress,svc,pods -n k3s-demo-dev
```

# Basic commands
```
kubectl config get-clusters
kubectl cluster-info
kubectl get  nodes
kubectl get namespaces
kubectl get endpoints -n kube-system
kubectl get pods -n kube-system
kubectl get ingress,svc,pods -n retail-project-dev
```