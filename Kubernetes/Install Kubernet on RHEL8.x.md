[[_TOC_]]

# Prerequisites
## Server Info
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| Dockermgr01 | 192.168.43.41 | RHEL 8.x | Docker Master Node 01 | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility |
| Dockerwkr01 | 192.168.43.43 | RHEL 8.x | Docker Worker Node 01 | Kubelet, Kube-proxy, Pod |
| Dockerwkr02 | 192.168.43.44 | RHEL 8.x | Docker Worker Node 02 | Kubelet, Kube-proxy, Pod |

* ``API Server`` – It provides kubernetes API using Jason / Yaml over http, states of API objects are stored in etcd
* ``Scheduler`` – It is a program on master node which performs the scheduling tasks like launching containers in worker nodes based on resource availability
* ``Controller Manager`` – Main Job of Controller manager is to monitor replication controllers and create pods to maintain desired state.
* ``etcd`` – It is a Key value pair data base. It stores configuration data of cluster and cluster state.
* ``Kubectl utility`` – It is a command line utility which connects to API Server on port 6443. It is used by administrators to create pods, services etc.
* ``Kubelet`` – It is an agent which runs on every worker node, it connects to docker  and takes care of creating, starting, deleting containers.
* ``Kube-Proxy`` – It routes the traffic to appropriate containers based on ip address and port number of the incoming request. In other words we can say it is used for port translation.
* ``Pod`` – Pod can be defined as a multi-tier or group of containers that are deployed on a single worker node or docker host.

## Verify the MAC address and product_uuid are unique for every node
* Get MAC by ``ip link`` or ``ifconfig -a`` command
* The product_uuid can be checked by using the command
```
cat /sys/class/dmi/id/product_uuid
```

## Configure Host file for each nodes
```
cat >> /etc/hosts <<'EOF'
192.168.43.41   Dockermgr01
192.168.43.43   Dockerwkr01
192.168.43.44   Dockerwkr02
EOF
```

## Configure local firewall
* For master node or control-plane node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 6443*	| Kubernetes API server	| All |
| TCP	| Inbound	| 2379-2380	| etcd server client API	| kube-apiserver, etcd |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 10251	| kube-scheduler | Self |
| TCP	| Inbound	| 10252	| kube-controller-manager	| Self |

```
firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --permanent --add-port=2379-2380/tcp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --add-port=10251/tcp
firewall-cmd --permanent --add-port=10252/tcp
firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd --reload
```

* For worker node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 30000-32767	| NodePort Services† | All |

```
firewall-cmd --permanent --add-port=10251/tcp
firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd --reload
```

## Letting iptables see bridged traffic
```
cat <<EOF | tee /etc/sysctl.d/Docker.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter
sysctl --system
```
## Disable SWAP on all nodes
```
sed -i '/swap/d' /etc/fstab
swapoff -a
```

# Install Docker on each node
* Remove existing Docker
```
dnf -y remove \
            docker \
            docker-client \
            docker-client-latest \
            docker-common \
            docker-latest \
            docker-latest-logrotate \
            docker-logrotate \
            docker-engine \
            docker-ce \
            docker-ce-cli \
            && rm -rf /etc/dnf.repos.d/docker-ce*.repo
```
* Create dependency repository
```
cat > /etc/dnf.repos.d/dockerdeps.repo <<'EOF'
[dockerdeps-baseos]
name=Docker BaseOS Deps on RHEL8.x
baseurl=http://mirror.centos.org/centos/$releasever/BaseOS/$basearch/os/
gpgcheck=0
enabled=1
includepkgs=policycoreutils-python-utils python3-policycoreutils checkpolicy audit-libs-python3 policycoreutils python3-libsemanage python3-setools python3-audit libsemanage audit-libs audit libcgroup

[dockerdeps-appstream]
name=Docker AppStream Deps on RHEL8.x
baseurl=http://mirror.centos.org/centos/$releasever/AppStream/$basearch/os/
gpgcheck=0
enabled=1
includepkgs=container-selinux
EOF
```

* Install requirement packages
```
dnf -y install lvm2 \
            device-mapper-persistent-data \
            container-selinux \
            https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
```
* Install Docker CE
```
dnf config-manager \
            --add-repo https://download.docker.com/linux/centos/docker-ce.repo

dnf -y install \
            docker-ce \
            docker-ce-cli

systemctl enable docker --now
```

# Install Kubernetes
* Create Kubernetes Repository
```
cat > /etc/dnf.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/dnf/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/dnf/doc/dnf-key.gpg https://packages.cloud.google.com/dnf/doc/rpm-package-key.gpg
EOF
```
* Install ``kubeadm, kubelet, kubectl`` on each node
```
dnf -y install \
            kubeadm \
            kubelet \
            kubectl
systemctl enable kubelet --now
```
* Pull image for ``Kubernetes Cluster``
```
kubeadm config images pull
```

* Initialize Kubernetes Master with ``kubeadm init``
```
kubeadm init --kubernetes-version $(kubeadm version -o short)
```

* To start using your cluster, you need to run the following as a regular user:
```
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
```

* Get status of cluster and pods
```
kubectl get nodes
kubectl get pods --all-namespaces
```
* Deploy the pod network with ``Flannel``
```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

* Deploy the pod network with ``Istio``. Reference: https://istio.io/docs/setup/getting-started/
  1. Download Istio or by this link: https://github.com/istio/istio/releases/
  ```
  curl -L https://istio.io/downloadIstio | sh -
  cd istio-*/
  export PATH=$PWD/bin:$PATH
  ```
  2. Install Istio
  ```
  istioctl install --set profile=demo
  kubectl label namespace default istio-injection=enabled
  ```
* Create an Istio Gateway:
```
kubectl apply -f - <<EOF
apiVersion: networking.Docker.io/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: istio
  name: ingress
spec:
  rules:
  - host: httpbin.example.com
    http:
      paths:
      - path: /status/*
        backend:
          serviceName: httpbin
          servicePort: 8000
EOF
```

* Deploy the pod network with ``Calico``
  1. Reference: https://docs.projectcalico.org/getting-started/kubernetes/self-managed-onprem/onpremises
  2. Download the Calico networking manifest for the Kubernetes API datastore.
  ```
  curl https://docs.projectcalico.org/manifests/calico.yaml -O
  ```
  3. Apply the manifest using the following command.
  ```
  kubectl apply -f calico.yaml
  ```

* Deploy the pod network with ``WeavNet``
```
export kubever=$(kubectl version | base64 | tr -d '\n')
kubectl apply -f "https://cloud.weave.works/Docker/net?Docker-version=$kubever"
```

* Verify status of cluster and pods again, make cluster is ready, and pods are all running
```
kubectl get nodes
kubectl get pods --all-namespaces
```

* Get Kubernetes join token command
```
kubeadm token create --print-join-command
```

# Join worker node(s) to cluster
```
kubeadm join 192.168.43.31:6443 --token hb2sm7.kusa7qyosb3a4j65     --discovery-token-ca-cert-hash sha256:64e62573ecbb0a30bc50daab8c0cd43b9ef93174599f2822e11bceb53e82dca0
```