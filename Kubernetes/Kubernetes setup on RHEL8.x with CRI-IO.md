```Kubernetes Setup on RHEL 8.x with containerd```
[[_TOC_]]

# Prerequisites
## Kubernetes ccomponents
* ``API Server`` – It provides kubernetes API using Jason / Yaml over http, states of API objects are stored in etcd
* ``Scheduler`` – It is a program on master node which performs the scheduling tasks like launching containers in worker nodes based on resource availability
* ``Controller Manager`` – Main Job of Controller manager is to monitor replication controllers and create pods to maintain desired state.
* ``etcd`` – It is a Key value pair data base. It stores configuration data of cluster and cluster state.
* ``Kubectl utility`` – It is a command line utility which connects to API Server on port 6443. It is used by administrators to create pods, services etc.
* ``Kubelet`` – It is an agent which runs on every worker node, it connects to docker  and takes care of creating, starting, deleting containers.
* ``Kube-Proxy`` – It routes the traffic to appropriate containers based on ip address and port number of the incoming request. In other words we can say it is used for port translation.
* ``Pod`` – Pod can be defined as a multi-tier or group of containers that are deployed on a single worker node or docker host.

## LAB Setup Info
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| rh8k8smtr | 192.168.100.50 | RHEL 8 | K8S Master Node | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility |
| rh8k8swkr01 | 192.168.100.51 | RHEL 8.x | K8S Worker Node 01 | Kubelet, Kube-proxy, Pod, KeepAliveD, HAProxy |
| rh8k8swkr02 | 192.168.100.52 | RHEL 8.x | K8S Worker Node 02 | Kubelet, Kube-proxy, Pod, KeepAliveD, HAProxy |

## Configure hosts file for every nodes
```
cat >> /etc/hosts <<'EOF'
192.168.100.50   rh8k8smtr
192.168.100.51   rh8k8swkr01
192.168.100.52   rh8k8swkr02
EOF
```
## Disable SWAP on all nodes
```
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a
```

## Verify the MAC address and product_uuid are unique for every nodes
* Get the MAC address of the network intefaces using
```
ip link
ifconfig -a
```
* Check product_uuid by command
```
sudo cat /sys/class/dmi/id/product_uuid
```

## Setup required sysctl params, these persist across reboots.
```
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

sudo sysctl --system
```

## Check required ports and apply for local firewall
* For master node or control-plane node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 6443*	| Kubernetes API server	| All |
| TCP	| Inbound	| 2379-2380	| etcd server client API	| kube-apiserver, etcd |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 10251	| kube-scheduler | Self |
| TCP	| Inbound	| 10252	| kube-controller-manager	| Self |

```
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

* For worker node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 30000-32767	| NodePort Services† | All |

```
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

# Install Container Rumetime (containerd)
* Set up the repository
```
sudo dnf install \
    device-mapper-persistent-data \
    lvm2

dnf config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

* Install containerd
```
dnf install containerd.io
```

* Configure containerd
```
sudo mkdir -p /etc/containerd
sudo containerd config default > /etc/containerd/config.toml
```

* Start and enable containerd
```
sudo systemctl enable containerd --now
```

# Installing Kubernetes (kubeadm)
* Create Kubernetes Repository
```
cat <<'EOF' | sudo tee /etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

```
* Install ``kubeadm, kubelet, kubectl`` on each node
```
sudo dnf -y install \
    kubeadm \
    kubelet \
    kubectl
sudo systemctl enable kubelet --now
```

# Initial Kubernetes Master
* Pull image for ``Kubernetes Cluster``
```
kubeadm config images pull
```

* Initial Kubernetes cluster with kubeadm
```
sudo echo "192.168.100.50    k8smaster" >> /etc/hosts
sudo kubeadm init \
    --kubernetes-version $(kubeadm version -o short) \
    --pod-network-cidr=192.169.0.0/16 \
    --control-plane-endpoint=k8smaster
```

* To start using your cluster, you need to run the following as a regular user
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

* Deploy the POD network with ``Calico`` as CNI
```
curl -Lo calico.yml https://docs.projectcalico.org/manifests/calico.yaml
vim calico.yml
-> Uncomment and change CIRD network as podSubnet
- name: CALICO_IPV4POOL_CIDR
  value: "192.169.0.0/16"

kubectl apply -f calico.yml
```

* Get join token
```
kubeadm token create --print-join-command
```

# Join Kubernetes worker nodes
* Add host file
```
sudo echo "192.168.100.50    k8smaster" >> /etc/hosts
```

* Join worker node(s)
```
kubeadm join k8smaster:6443 --token vnuipg.lytpc10hx754pqmc     --discovery-token-ca-cert-hash sha256:5fc11851fadac0a7a8c8e48d21f5735c67adfc1dea265faa3bd6c7c0a7de0d0f
```

# Deploy HAProxy Ingress
* Install helm, HAProxy Ingress requires version 3
```
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

* Add the HAProxy Ingress’ Helm repository. This will instruct Helm to find all available packages:
```
helm repo add haproxy-ingress https://haproxy-ingress.github.io/charts
```

* Check if kubeconfig points to the right cluster:
```
kubectl cluster-info
```

* Install HAProxy Ingress using haproxy-ingress as the release name:
```
helm install haproxy-ingress haproxy-ingress/haproxy-ingress \
    --create-namespace --namespace=ingress-controller \
    --set controller.hostNetwork=true
```

* Output messages
```
HAProxy Ingress has been installed!

HAProxy is exposed as a `LoadBalancer` type service.
It may take a few minutes for the LoadBalancer IP to be available.
You can watch the status by running:

    kubectl --namespace ingress-controller get services haproxy-ingress -o wide -w

An example Ingress that makes use of the controller:

  apiVersion: networking.k8s.io/v1beta1
  kind: Ingress
  metadata:
    annotations:
      kubernetes.io/ingress.class: haproxy
    name: example
    namespace: default
  spec:
    rules:
      - host: www.example.com
        http:
          paths:
            - backend:
                serviceName: exampleService
                servicePort: 8080
              path: /
```
