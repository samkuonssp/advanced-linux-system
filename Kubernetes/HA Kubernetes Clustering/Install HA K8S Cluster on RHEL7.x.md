[[_TOC_]]
# Demonstration Servers
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| k8smaster-vip | 192.168.100.40 | N/A | Cluster VIP | N/A |
| k8smaster-01 | 192.168.100.41 | RHEL7.x | K8S Master Node 01 | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility, HAproxy, KeepAliveD |
| k8smaster-02 | 192.168.100.42 | RHEL7.x | K8S Master Node 02 | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility, HAproxy, KeepAliveD |
| k8smaster-03 | 192.168.100.43 | RHEL7.x | K8S Master Node 03 | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility, HAproxy, KeepAliveD |
| k8sworker-01 | 192.168.100.51 | RHEL7.x | K8S Master Node 03 | Kubelet, Kube-proxy, Pod |
| k8sworker-02 | 192.168.100.52 | RHEL7.x | K8S Master Node 03 | Kubelet, Kube-proxy, Pod |

# Prerequisites
## Set ``hostname`` and ``/etc/hosts`` file
* Set ``hostname`` for each nodes
```
sudo hostnamectl set-hostname k8s<master/worker>-<xx>
sudo exec bash
```

* Set ``/etc/hosts`` file for each nodes
```
cat <<'EOF' | sudo tee -a /etc/hosts
k8smaster-vip   192.168.100.40
k8smaster-01    192.168.100.41
k8smaster-02    192.168.100.42
k8smaster-03    192.168.100.43
k8sworker-01    192.168.100.51
k8sworker-01    192.168.100.52
EOF
```

## Configure Local Firewall
* For master node(s) or control-plane node(s)
```
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

* For worker node(s)
```
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

## Letting iptables see bridged traffic
```
cat <<EOF | sudo tee /etc/sysctl.d/iptables-bridge.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter
sysctl --system
```

## Disable SWAP on all nodes
```
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a
```

## Install Docker Runtime on each node(s)
* Remove existing old Dokcer packages
```
sudo yum remove \
        docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-engine && \
sudo rm -rf /etc/yum.repos.d/docker-ce*.repo
```

* Install additional requirement packages
```
sudo yum install \
        device-mapper-persistent-data \
        yum-utils \
        lvm2
```

* Add Docker CE official repository and required additional repository
```
# Fix RHEL 7.x release version
echo '7' > /etc/yum/vars/releasever

# Docker CE official repository
sudo yum-config-manager \
        --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Additional repository from CentOS Extra
cat <<'EOF' | sudo tee /etc/yum.repos.d/docker-ce-deps.repo
[docker-ce-deps]
name=Docker Deps on RHEL7.x
baseurl=http://mirror.centos.org/centos/$releasever/extras/$basearch/
gpgcheck=0
enabled=1
includepkgs=container-selinux
EOF
```

* Install Docker Engine CE (online). Note: we must have internet connection!!!
```
sudo yum install \
        docker-ce \
        docker-ce-cli \
        containerd.io
```

* Enable and start ``Docker Engine`` service
```
systemctl enable docker --now
```
# Kubernetes Master Node(s)
## Install HAProxy and KeepAliveD packages
```
sudo yum install \
        haproxy \
        keepalived
```

## Install Kubernetes packages
* Create Kubernetes Repository
```
cat <<'EOF' | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```

* Install ``kubeadm, kubelet, kubectl``
```
sudo yum install \
        kubeadm \
        kubelet \
        kubectl
systemctl enable kubelet --now
```

