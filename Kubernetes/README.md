```Build Kubernetes for Production Environment```

[[_TOC_]]

# Prerequisites
## Kubernetes Overview
* ``API Server`` – It provides kubernetes API using Jason / Yaml over http, states of API objects are stored in etcd
* ``Scheduler`` – It is a program on master node which performs the scheduling tasks like launching containers in worker nodes based on resource availability
* ``Controller Manager`` – Main Job of Controller manager is to monitor replication controllers and create pods to maintain desired state.
* ``etcd`` – It is a Key value pair data base. It stores configuration data of cluster and cluster state.
* ``Kubectl utility`` – It is a command line utility which connects to API Server on port 6443. It is used by administrators to create pods, services etc.
* ``Kubelet`` – It is an agent which runs on every worker node, it connects to docker  and takes care of creating, starting, deleting containers.
* ``Kube-Proxy`` – It routes the traffic to appropriate containers based on ip address and port number of the incoming request. In other words we can say it is used for port translation.
* ``Pod`` – Pod can be defined as a multi-tier or group of containers that are deployed on a single worker node or docker host.

At a high level, you have four main deployment configurations:

* ``Single-node``
With a single-node deployment, all the components run on the same server. This is great for testing, learning, and developing around Kubernetes.

* ``Single head node, multiple workers``
Adding more workers, a single head node and multiple workers typically will consist of a single node etcd instance running on the head node with the API, the scheduler, and the controller-manager.

* ``Multiple head nodes with HA, multiple workers``
Multiple head nodes in an HA configuration and multiple workers add more durability to the cluster. The API server will be fronted by a load balancer, the scheduler and the controller-manager will elect a leader (which is configured via flags). The etcd setup can still be single node.

* ``HA etcd, HA head nodes, multiple workers``
The most advanced and resilient setup would be an HA etcd cluster, with HA head nodes and multiple workers. Also, etcd would run as a true cluster, which would provide HA and would run on nodes separate from the Kubernetes head nodes.

## LAB Setup Info
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| k8smtr01 | 192.168.43.31 | RHEL 8 | K8S Master Node 01 | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility |
| k8swkr01 | 192.168.43.33 | RHEL 8 | K8S Worker Node 01 | Kubelet, Kube-proxy, Pod |
| k8swkr02 | 192.168.43.34 | RHEL 8 | K8S Worker Node 02 | Kubelet, Kube-proxy, Pod |

## Configure hosts file for every nodes
```
cat >> /etc/hosts <<'EOF'
192.168.43.31   k8smtr01
192.168.43.33   k8swkr01
192.168.43.34   k8swkr02
EOF
```
## Disable SWAP on all nodes
```
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a
```

## Verify the MAC address and product_uuid are unique for every nodes
* Get the MAC address of the network intefaces using
```
ip link
ifconfig -a
```
* Check product_uuid by command
```
sudo cat /sys/class/dmi/id/product_uuid
```

## Letting iptables see bridged traffic
* As requirement for Linux Node's iptables to correctly see bridged traffic, we should ensure ``net.bridge.bridge-nf-call-iptables = 1``
```
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo modprobe br_netfilter
sudo sysctl --system
```

## Check required ports and apply for local firewall
* For master node or control-plane node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 6443*	| Kubernetes API server	| All |
| TCP	| Inbound	| 2379-2380	| etcd server client API	| kube-apiserver, etcd |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 10251	| kube-scheduler | Self |
| TCP	| Inbound	| 10252	| kube-controller-manager	| Self |

```
sudo firewall-cmd --permanent --add-port=6443/tcp
sudo firewall-cmd --permanent --add-port=2379-2380/tcp
sudo firewall-cmd --permanent --add-port=10250/tcp
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10252/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

* For worker node(s)

| Protocol | Direction | Port Range | Purpose	| Used By |
| ----- | ----- | ----- | ----- | ----- |
| TCP	| Inbound	| 10250	| Kubelet API	| Self, Control plane |
| TCP	| Inbound	| 30000-32767	| NodePort Services† | All |

```
sudo firewall-cmd --permanent --add-port=10251/tcp
sudo firewall-cmd --permanent --add-port=10255/tcp
sudo firewall-cmd --reload
```

## Installing Docker for runtime
* Remove existing Docker runtime
```
sudo yum remove -y \
        docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-engine \
        docker-ce \
        docker-ce-cli \
        && rm -rf /etc/yum.repos.d/docker-ce*.repo
```
* Install requirement packages
```
sudo yum install -y \
        device-mapper-persistent-data \
        lvm2 \
        yum-utils \
        http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```
* Install Docker CE
```
sudo yum-config-manager \
        --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y \
        docker-ce \
        docker-ce-cli \
        containerd.io
```
* Set up the Docker daemon
```
mkdir /etc/docker
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF
```
* Start and enable Docker service
```
systemctl enable --now docker
```

## Installing kubeadm, kubelet and kubectl
* Create Kubernetes repository
```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
```

* Set SELinux in permissive mode
```
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```
* Install ``Kubelet, Kubeadm & Kubectl`` packages
```
sudo yum install -y \
        kubelet \
        kubeadm \
        kubectl \
        --disableexcludes=kubernetes
```
* Start and enable ``Kubelet`` service
```
systemctl enable --now kubelet
```

# Initial cluster for control plan node(s)
* Initialize Kubernetes Master with ``kubeadm init``
```
kubeadm init --kubernetes-version $(kubeadm version -o short)
```

* To start using your cluster, you need to run the following as a regular user:
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

* Deploy network with Weavnet
```
export kubever=$(kubectl version | base64 | tr -d '\n')
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"
```

* Check nodes status
```
kubectl get nodes
kubectl get pods --all-namespaces
```

* Get join token
```
kubeadm token create --print-join-command
```

# Jion worker node(s) to cluster
kubeadm join 192.168.43.31:6443 --token 4ld0sq.6tr6lx2bs9x1fc0d \
    --discovery-token-ca-cert-hash sha256:19f315d2651394da5fa1f73aaefb42ea3fb9e9feca