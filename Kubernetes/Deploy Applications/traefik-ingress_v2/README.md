# Step 1) prepare CRD, RBAC and a ServiceAccount for Traefik 2
traefik_v2-ingress-req.yaml

# Step 2) Install Traefik 2 as a deployment
traefik_v2-ingress-dep.yaml

# Step 3) Understand what’s happening
# Step 4) Configure the Traefik 2 Service
traefik_v2-ingress-svc.yaml

# Reference: https://codar.nl/howto-run-traefik2-on-kubernetes-with-network-policies/