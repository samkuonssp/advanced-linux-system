# Deploy NGINX
## Simple deployment
* Create a new deplyment for nginx
```
kubectl create deployment nginx --image=nginx
kubectl get deployments
```

* Make the NGINX container accessible via the internet
```
kubectl create service nodeport nginx --tcp=80:80
kubectl get svc
-> output example:
[root@k8smgr01 ~]# kubectl get svc
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP        12h
web-nginx    NodePort    10.97.30.236   <none>        80:32699/TCP   2m4s
```

* Verify that the NGINX deployment is successful by using curl on the slave node
```
curl http://k8swkr01:32699
```

## YAML deployment
cat > nginx-deployment.yaml <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
EOF

## Deploy new services
cat > nginx-one.yaml <<'EOF'
apiVersion: apps/v12
# Determines YAML versioned schema
kind: Deployment
# Describes the resource defined in this file
metadata:
  name: nginx-one
  labels:
    system: secondary
# Required string which defines object within namespace
  namespace: accounting
# Existing namespace resource will be deployed into
spec:
  selector:
    matchLabels:
      system: secondary
# Declaration of the label for the deployment to manage
  replicas: 2
# How many Pods of following containers to deploy
  template:
    metadata:
      labels:
        system: secondary
# Some string meaningful to users, not cluster. Keys
# must be unique for each object. Allows for mapping
# to customer needs.
    spec:
      containers:
# Array of objects describing containerized application with a Pod.
# Referenced with shorthand spec.template.spec.containers
      - image: nginx:1.16.1
# The Docker image to deploy
        imagePullPolicy: Always
        name: nginx
# Unique name for each container, use local or Docker repo image
        ports:
        - containerPort: 80
          protocol: TCP
# Optional resources this container may need to function.
      nodeSelector:
        system: secondOne
# One method of node affinity.
EOF


