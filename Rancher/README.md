[[_TOC_]]

## Disable SWAP on all nodes
```
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a
```

## Installing Docker for runtime
* Remove existing Docker runtime
```
sudo yum remove -y \
        docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-engine \
        docker-ce \
        docker-ce-cli \
        && rm -rf /etc/yum.repos.d/docker-ce*.repo
```
* Install requirement packages
```
sudo yum install -y \
        device-mapper-persistent-data \
        lvm2 \
        yum-utils \
        http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```
* Install Docker CE
```
sudo yum-config-manager \
        --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y \
        docker-ce \
        docker-ce-cli \
        containerd.io
```
* Start and enable Docker service
```
systemctl enable --now docker
```

firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --permanent --add-port=88/tcp
firewall-cmd --permanent --add-port=444/tcp
firewall-cmd --reload

vim /etc/ssh/sshd_config
AllowTcpForwarding yes
systemctl restart sshd

sudo docker run -d --restart=unless-stopped -p 88:80 -p 444:443 rancher/rancher

# Reference
* https://medium.com/@kwonghung.yip/setup-local-kubernetes-multi-node-cluster-with-rancher-server-fdb7a0669b5c


