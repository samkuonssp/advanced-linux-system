# LAB info
| Hostname | IP | OS | Purpose | Component Installed |
| ------ | ------ | ----- | ----- | ----- |
| rkemgr01 | 192.168.43.31 | RHEL 7.x | RKE Master Node 01 | API Server, Scheduler, Controller Manager, etcd, Kubectl Utility |
| rkewkr01 | 192.168.43.32 | RHEL 7.x | RKE Worker Node 01 | Kubelet, Kube-proxy, Pod |
| rkewkr02 | 192.168.43.33 | RHEL 7.x | RKE Worker Node 02 | Kubelet, Kube-proxy, Pod |

# Requirements
## Letting iptables see bridged traffic
```
cat <<EOF | tee /etc/sysctl.d/Docker.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
modprobe br_netfilter
sysctl --system
```
## Disable SWAP on all nodes
```
sed -i '/swap/d' /etc/fstab
swapoff -a
```

## Install Docker CE
* Create dependency repository
```
cat > /etc/yum.repos.d/dockerrequired.repo <<'EOF'
[dockerrequired]
name=Docker required packages on RHEL7.x
baseurl=http://mirror.centos.org/centos/$releasever/extras/$basearch/
gpgcheck=0
enabled=1
includepkgs=container-selinux
EOF
```

* Install requirement packages
```
yum -y install \
    yum-utils \
    device-mapper-persistent-data \
    container-selinux \
    lvm2
```

* Enable Docker CE Repo
```
yum-config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

* Install Docker CE
```
yum -y install \
    docker-ce \
    docker-ce-cli \
    containerd.io
systemctl enable docker --now
```

#### Using upstream Docker
## Software
## Ports
* On Master
```
firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --reload
```

## SSH Server Configuration
sed -i 's/^AllowTcpForwarding/#&/' /etc/ssh/sshd_config
echo "AllowTcpForwarding yes" >> /etc/ssh/sshd_config
systemctl restart sshd

## Installing Kubectl
* Create Kubernetes Repository
```
cat > /etc/yum.repos.d/kubernetes.repo <<EOF
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum -y install kubectl
```

## Installing RKE
yum -y install wget && \
curl -s https://api.github.com/repos/rancher/rke/releases/latest | grep download_url | grep linux-amd64 | cut -d '"' -f 4 | wget -i -
chmod +x rke_linux-amd64
mv rke_linux-amd64 /usr/bin/rke
rke --version